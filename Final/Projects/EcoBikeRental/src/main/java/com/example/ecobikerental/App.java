package com.example.ecobikerental;

import com.example.ecobikerental.dao.bike.BikeDAO;
import com.example.ecobikerental.enums.bike.State;
import com.example.ecobikerental.handlers.auth.LoginHandler;
import com.example.ecobikerental.utils.Configs;
import javafx.animation.FadeTransition;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.io.IOException;
import java.net.URL;
import java.util.Objects;

public class App extends Application {
    @Override
    public void start(Stage stage) throws IOException {
        try {
            // initialize the scene
            URL source = getClass().getResource(Configs.CHOOSE_STATION_PATH);
            AnchorPane root = (AnchorPane) FXMLLoader.load(Objects.requireNonNull(getClass().getResource(Configs.SPLASH_PATH)));
            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.show();

            // Load splash screen with fade in effect
            FadeTransition fadeIn = new FadeTransition(Duration.seconds(2), root);
            fadeIn.setFromValue(0);
            fadeIn.setToValue(1);
            fadeIn.setCycleCount(1);

            // Finish splash with fade out effect
            FadeTransition fadeOut = new FadeTransition(Duration.seconds(2), root);
            fadeOut.setFromValue(1);
            fadeOut.setToValue(0);
            fadeOut.setCycleCount(1);

            // After fade in, start fade out
            fadeIn.play();
            fadeIn.setOnFinished((e) -> {
                fadeOut.play();
            });

            // After fade out, load actual content
            fadeOut.setOnFinished((e) -> {
                try {
                    LoginHandler loginhandler = new LoginHandler(stage, Configs.LOGIN_PATH);
                    loginhandler.setScreenTitle("Login");
                    loginhandler.show();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        launch();
    }
}
