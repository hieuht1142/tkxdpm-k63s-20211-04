package com.example.ecobikerental.controllers;

import com.example.ecobikerental.dao.bike.IBikeDAO;
import com.example.ecobikerental.dao.category.ICategoryDAO;
import com.example.ecobikerental.dao.station.IStationDAO;
import com.example.ecobikerental.exceptions.ExistedLicencePlateException;
import com.example.ecobikerental.exceptions.InvalidBikeInfoException;
import com.example.ecobikerental.dao.bike.BikeDAO;
import com.example.ecobikerental.dao.category.CategoryDAO;
import com.example.ecobikerental.dao.station.StationDAO;
import com.example.ecobikerental.entities.Bike;
import com.example.ecobikerental.entities.Category;
import com.example.ecobikerental.entities.Station;
import com.example.ecobikerental.exceptions.UsedPositionException;
import com.example.ecobikerental.utils.Configs;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.ComboBox;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AddBikeController extends BaseController {

    private ICategoryDAO categoryDAO;

    private IStationDAO stationDAO;

    private IBikeDAO bikeDAO;

    public AddBikeController() {
        this.categoryDAO = new CategoryDAO();
        this.stationDAO = new StationDAO();
        this.bikeDAO = new BikeDAO();
    }

    public AddBikeController(CategoryDAO categoryDAO, StationDAO stationDAO, BikeDAO bikeDAO) {
        this.categoryDAO = categoryDAO;
        this.stationDAO = stationDAO;
        this.bikeDAO = bikeDAO;
    }

    public List<Category> initializeCategories() {
        return categoryDAO.findAll();
    }

    public List<Station> initializeStations() {
        return stationDAO.findAll();
    }

    public void addBike(Map<String, Object> bikeInfos) throws IOException {
        validateBikeInfos(bikeInfos);

        String licencePlate = (String) bikeInfos.get("licencePlate");
        if (bikeDAO.checkExistLicencePlate(licencePlate) > 0) {
            throw new ExistedLicencePlateException();
        }

        Integer stationId = ((Station) bikeInfos.get("station")).getId();
        Integer position = (Integer) bikeInfos.get("position");
        if (bikeDAO.checkUsedPosition(stationId, position) > 0) {
            throw new UsedPositionException();
        }

        File imageFile = (File) bikeInfos.get("imageFile");
        String savedImageName = saveBikeImage(imageFile);
        bikeInfos.put("image", savedImageName);

        Bike newBike = new Bike(bikeInfos);
        bikeDAO.save(newBike);
    }

    private String saveBikeImage(File imageFile) throws IOException {
        if (imageFile == null) {
            throw new InvalidBikeInfoException("Please choose bike image!");
        }

        UUID uuid = UUID.randomUUID();
        String savedFileName = uuid.toString() + "-" + imageFile.getName();

        File savedImage = new File(System.getProperty("user.dir") + Configs.BIKE_IMAGES_DIR, savedFileName);
        FileInputStream ifstream = new FileInputStream(imageFile);
        FileOutputStream ofstream = new FileOutputStream(savedImage);
        ofstream.write(ifstream.readAllBytes());

        ifstream.close();
        ofstream.close();
        return "assets/images/bikes/" + savedFileName;
    }

    public boolean validateBikeInfos(Map<String, Object> bikeInfos) {
        if (bikeInfos == null || bikeInfos.size() == 0) {
            throw new InvalidBikeInfoException("Please enter bike infos");
        }

        validateBikeName((String) bikeInfos.get("name"));
        validateCategory((Category) bikeInfos.get("category"));

        Float weight = validateWeight((String) bikeInfos.get("weight"));
        bikeInfos.replace("weight", weight);

        validateLicensePlate((String) bikeInfos.get("licencePlate"));

        validateManufacturingDate((LocalDate) bikeInfos.get("manufacturingDate"));

        Long price = validatePrice((String) bikeInfos.get("price"));
        bikeInfos.replace("price", price);

        validateStation((Station) bikeInfos.get("station"));

        Integer position = validatePosition((String) bikeInfos.get("position"), (Station) bikeInfos.get("station"));
        bikeInfos.replace("position", position);

        Integer bateryPercentage = validateBateryPercentage((String) bikeInfos.get("bateryPercentage"));
        bikeInfos.replace("bateryPercentage", bateryPercentage);

        Integer loadCycles = validateLoadCycles((String) bikeInfos.get("loadCycles"));
        bikeInfos.replace("loadCycles",  loadCycles);

        Integer remainTime = validateTimeRemaining((String) bikeInfos.get("remainTime"));
        bikeInfos.replace("remainTime", remainTime);

        return true;
    }

    public boolean validateBikeName(String bikeName) {
        if (bikeName == null || bikeName.isBlank()) {
            throw new InvalidBikeInfoException("Please enter bike name!");
        }

        Pattern bikeNamePattern = Pattern.compile("[a-zA-Z\\s]+");
        Matcher bikeNameMatcher = bikeNamePattern.matcher(bikeName);
        if (!bikeNameMatcher.matches()) {
            throw new InvalidBikeInfoException("Bike name only contain a-z, A-Z and whitespaces!");
        }
        return true;
    }

    public boolean validateCategory(Category category) {
        if (category == null) {
            throw new InvalidBikeInfoException("Please choose bike category");
        }
        return true;
    }

    public Float validateWeight(String weight) {
        if (weight == null || weight.isBlank()) {
            return null;
        }

        try {
            Float value =  Float.parseFloat(weight);
            if (value <= 0) {
                throw new InvalidBikeInfoException("Weight must be a positive number!");
            }
            return value;
        } catch (NumberFormatException ex) {
            throw new InvalidBikeInfoException("Invalid bike weight format");
        }
    }

    public boolean validateLicensePlate(String licensePlate) {
        if (licensePlate == null || licensePlate.isBlank()) {
            throw new InvalidBikeInfoException("Please enter license plate!");
        }

        Pattern licensePlatePattern = Pattern.compile("[0-9]{2}[A-Z]-[0-9]{5}");
        Matcher liscensePlateMatcher = licensePlatePattern.matcher(licensePlate);
        if (!liscensePlateMatcher.matches()) {
            throw new InvalidBikeInfoException("Invalid license plate format!");
        }
        return true;
    }

    public boolean validateManufacturingDate(LocalDate manufacturingDate) {
        if (manufacturingDate != null && manufacturingDate.isAfter(LocalDate.now())) {
            throw new InvalidBikeInfoException("Manufacturing date must be before now!");
        }
        return true;
    }

    public Long validatePrice(String price) {
        if (price == null || price.isBlank()) {
            throw new InvalidBikeInfoException("Please enter bike price!");
        }

        try {
            Long value = Long.parseLong(price);
            if (value <= 0) {
                throw new InvalidBikeInfoException("Price must be positive!");
            } else if (value % 1000 != 0) {
                throw new InvalidBikeInfoException("Price must be  divisible for 1000");
            }
            return value;
        } catch (NumberFormatException ex) {
            throw new InvalidBikeInfoException("Invalid price format!");
        }
    }

    public boolean validateStation(Station station) {
        if (station == null) {
            throw new InvalidBikeInfoException("Please choose station!");
        }
        return true;
    }

    public Integer validatePosition(String position, Station station) {
        if (position == null || position.isBlank()) {
            throw new InvalidBikeInfoException("Please enter position!");
        }

        try {
            Integer pos = Integer.parseInt(position);
            if (pos <= 0) {
                throw new InvalidBikeInfoException("Position must be positive");
            } else if (pos > station.getTotalPositions()) {
                throw new InvalidBikeInfoException(String.format("Station %s not contains position %d", station.getName(), pos));
            }
            return pos;
        } catch (NumberFormatException ex) {
            throw new InvalidBikeInfoException("Invalid position format");
        }
    }

    public Integer validateBateryPercentage(String bateryPercentage) {
        if (bateryPercentage == null || bateryPercentage.isBlank()) {
            return null;
        }

        try {
            Integer value = Integer.parseInt(bateryPercentage);
            if (value < 0) {
                throw new InvalidBikeInfoException("Battery percentage must be not negative!");
            } else if (value > 100) {
                throw new InvalidBikeInfoException("Battery percentage must be less than or equal 100");
            }
            return value;
        } catch (NumberFormatException ex) {
            throw new InvalidBikeInfoException("Invalid battery percentage format");
        }
    }

    public Integer validateLoadCycles(String loadCycles) {
        if (loadCycles == null || loadCycles.isBlank()) {
            return null;
        }

        try {
            Integer value = Integer.parseInt(loadCycles);
            if (value < 0) {
                throw new InvalidBikeInfoException("Load cycles must not negative!");
            }
            return value;
        } catch (NumberFormatException ex) {
            throw new InvalidBikeInfoException("Invalid load cycles format!");
        }
    }

    public Integer validateTimeRemaining(String timeRemaining) {
        if (timeRemaining == null || timeRemaining.isBlank()) {
            return null;
        }

        try {
            Integer value = Integer.parseInt(timeRemaining);
            if (value < 0) {
                throw new InvalidBikeInfoException("Time remaining must be not negative!");
            }
            return value;
        } catch (NumberFormatException ex) {
            throw new InvalidBikeInfoException("Invalid time remaining format!");
        }
    }
}
