package com.example.ecobikerental.controllers;

import com.example.ecobikerental.dao.bike.BikeDAO;
import com.example.ecobikerental.dao.bike.IBikeDAO;
import com.example.ecobikerental.entities.Bike;
import com.example.ecobikerental.entities.Paginator;

import static com.example.ecobikerental.utils.Configs.PER_PAGE;

public class BikeController extends BaseController {
    private IBikeDAO bikeDAO;

    public BikeController() {
        this.bikeDAO = new BikeDAO();
    }

    public Paginator getAllBikes(int skip) {
        return bikeDAO.findAll(skip, PER_PAGE);
    }

    public Bike searchBike(String bikeCode) {
        return bikeDAO.find(bikeCode);
    }

    public Paginator searchBike(String keyword, String category, int skip) {
        return bikeDAO.find(keyword, category, skip, PER_PAGE);
    }

    public Paginator searchByKeyword(String keyword, int skip) {
        return bikeDAO.findByKeyword(keyword, skip, PER_PAGE);
    }

    public Paginator searchByCategory(String category, int skip) {
        return bikeDAO.findByCategory(category, skip, PER_PAGE);
    }
}
