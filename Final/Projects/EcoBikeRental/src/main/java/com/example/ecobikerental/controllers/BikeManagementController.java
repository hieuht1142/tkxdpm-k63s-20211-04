package com.example.ecobikerental.controllers;

import com.example.ecobikerental.dao.bike.BikeDAO;
import com.example.ecobikerental.dao.bike.IBikeDAO;
import com.example.ecobikerental.dao.bikedetail.BikeDetailsDAO;
import com.example.ecobikerental.dao.bikedetail.IBikeDetailDAO;
import com.example.ecobikerental.dto.BikeDetailsDto;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TableView;

import java.util.List;

public class BikeManagementController extends BaseController {

    private IBikeDetailDAO bikeDetailsDAO;

    private IBikeDAO bikeDAO;

    public BikeManagementController() {
        bikeDetailsDAO = new BikeDetailsDAO();
        bikeDAO = new BikeDAO();
    }

    public BikeManagementController(BikeDetailsDAO bikeDetailsDAO) {
        this.bikeDetailsDAO = bikeDetailsDAO;
    }

    public int countPage(int pageSize) {
        int numOfBikes = bikeDAO.countAll();
        return numOfBikes % pageSize == 0 ? numOfBikes/pageSize : numOfBikes/pageSize + 1;
    }

    public List<BikeDetailsDto> getBikeTablePageData(int offset, int pageSize) {
        return bikeDetailsDAO.findAllWithDetails(offset, pageSize);
    }
}
