package com.example.ecobikerental.controllers;

import com.example.ecobikerental.dao.category.CategoryDAO;
import com.example.ecobikerental.dao.category.ICategoryDAO;
import com.example.ecobikerental.entities.Category;

import java.util.ArrayList;
import java.util.List;

public class CategoryController {

    private ICategoryDAO categoryDAO;

    public CategoryController() {
        this.categoryDAO = new CategoryDAO();
    }

    public List<Category> getAllCategories() {
        return categoryDAO.findAll();
    }

    public List<String> getAllCategoryNames() {
        List<String> names = new ArrayList<>();
        List<Category> categories = getAllCategories();

        categories.forEach((category) -> {
            names.add(category.getName());
        });

        return names;
    }
}
