package com.example.ecobikerental.controllers;

import com.example.ecobikerental.dao.bike.BikeDAO;
import com.example.ecobikerental.dao.bike.IBikeDAO;
import com.example.ecobikerental.dao.rental.IRentalDAO;
import com.example.ecobikerental.dao.rental.RentalDAO;
import com.example.ecobikerental.enums.bike.State;
import com.example.ecobikerental.enums.invoice.Type;
import com.example.ecobikerental.exceptions.BikeNotAvailableException;
import com.example.ecobikerental.exceptions.InvalidRentingInfoException;
import com.example.ecobikerental.entities.Bike;
import com.example.ecobikerental.entities.Invoice;
import com.example.ecobikerental.entities.Rental;
import com.example.ecobikerental.utils.Configs;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.example.ecobikerental.enums.rental.RentingType.*;

public class RentBikeController extends BaseController {
    private IRentalDAO rentalDAO;
    private IBikeDAO bikeDAO;
    private Rental rental;
    private static RentBikeController instance = new RentBikeController();

    public static RentBikeController getInstance() {
        return instance;
    }

    public RentBikeController() {
        this.rentalDAO = new RentalDAO();
        this.bikeDAO = new BikeDAO();
    }

    public boolean rentBike(Bike bike) throws SQLException {
        if (!bike.checkBikeAvailability()) {
            throw new BikeNotAvailableException("The bike had already been rented. Please choose another one!");
        }

        return true;
    }

    public Rental processRentingInfo(Bike bike, HashMap<String, String> rentingInfo) {
        validateRentingInfo(rentingInfo);

        long deposit = bike.getDeposit();
        createRental(bike);
        setRentingInfo(deposit, rentingInfo.get("rentingType"));

        return rental;
    }

    private void validateRentingInfo(HashMap<String, String> rentingInfo) {
        String rentingType = rentingInfo.get("rentingType");
        String name = rentingInfo.get("name");
        String phoneNumber = rentingInfo.get("phoneNumber");

        if (!validateName(name)) {
            throw new InvalidRentingInfoException("Name is invalid!");
        }

        if (!validatePhoneNumber(phoneNumber)) {
            throw new InvalidRentingInfoException("Phone number is invalid!");
        }

        if (!validateRentingType(rentingType)) {
            throw new InvalidRentingInfoException("Please choose renting type!");
        }
    }

    public boolean validateName(String name) {
        try {
            if (name.length() < 2 || name.length() > 30) {
                return false;
            }

            Pattern pattern = Pattern.compile(Configs.REGEX_NAME);
            Matcher matcher = pattern.matcher(name);

            return matcher.find();
        } catch (NullPointerException e) {
            return false;
        }
    }

    public boolean validatePhoneNumber(String phoneNumber) {
        try {
            Pattern pattern = Pattern.compile(Configs.REGEX_PHONE);
            Matcher matcher = pattern.matcher(phoneNumber);

            return matcher.find();
        } catch (NullPointerException e) {
            return false;
        }
    }

    public boolean validateRentingType(String rentingType) {
        return rentingType != null && !rentingType.equals("") && ALL.contains(rentingType);
    }

    private void createRental(Bike bike) {
        rental = new Rental(bike);
    }

    private void setRentingInfo(long deposit, String rentingType) {
        rental.setDeposit(deposit);
        rental.setStartTime(Timestamp.valueOf(LocalDateTime.now()));
        AuthController authController = new AuthController();
        rental.setRenter(authController.getAuthUser());

        if (rentingType.equals(BY_MINUTES_TEXT)) {
            rental.setRentingType(BY_MINUTES);
        } else if (rentingType.equals(BY_DAY_TEXT)) {
            rental.setRentingType(BY_DAY);
        }
    }

    public Invoice createInvoice(Rental rental) {
        Invoice invoice = new Invoice(rental);
        invoice.setTotalAmount(rental.getDeposit());
        invoice.setBike(rental.getBike());
        invoice.setType(Type.DEPOSIT_INVOICE);

        return invoice;
    }

    public int saveRental(Rental rental) {
        return rentalDAO.create(rental);
    }

    public void updateBikeRented(Bike bike) {
        bikeDAO.update(bike.getId(), State.RENTED);
    }
}
