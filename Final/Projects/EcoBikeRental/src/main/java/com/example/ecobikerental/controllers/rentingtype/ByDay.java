package com.example.ecobikerental.controllers.rentingtype;

import com.example.ecobikerental.entities.Rental;
import com.example.ecobikerental.enums.rental.RentingType;

public class ByDay implements RentingTypeInterface{
    @Override
    public long calculateRentingFee(Rental rental) {
        long fee = 0;
        long minus = (rental.getEndTime().getTime() - rental.getStartTime().getTime())/(1000 * 60);
        long hour = minus/60;
        fee = 200000;
        if(hour < 12){
            fee = fee - (12-hour)*10000;
        } else if(hour >= 24){
            fee += Math.ceil((minus - 1440)/15.0)*2000;
        }
        return fee;
    }
}
