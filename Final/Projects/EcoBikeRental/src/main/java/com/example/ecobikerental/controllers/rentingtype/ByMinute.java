package com.example.ecobikerental.controllers.rentingtype;

import com.example.ecobikerental.entities.Rental;
import com.example.ecobikerental.enums.rental.RentingType;

public class ByMinute implements RentingTypeInterface{
    @Override
    public long calculateRentingFee(Rental rental) {
        long fee = 0;
        long minus = (rental.getEndTime().getTime() - rental.getStartTime().getTime())/(1000 * 60);
        if(minus < 10){
            fee = 0;
        } else if(minus <= 30){
            fee = 10000;
        } else {
            fee = (long) (10000 + Math.ceil((minus - 30)/15.0)*3000);
        }
        return fee;
    }
}
