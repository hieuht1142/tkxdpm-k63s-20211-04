package com.example.ecobikerental.controllers.rentingtype;

import com.example.ecobikerental.entities.Rental;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;

public class RentingTypeFactory {
    private HashMap registerRentingType = new HashMap();
    private static RentingTypeFactory instance = new RentingTypeFactory();

    public static RentingTypeFactory getInstance() {
        return instance;
    }

    public void registerRentingType(int rentingType, RentingTypeInterface rentingTypeObject) {
        registerRentingType.put(rentingType, rentingTypeObject);
    }

    public long calculateRentingFee(int rentingType, Rental rental) {
        RentingTypeInterface rentingTypeObject = (RentingTypeInterface) registerRentingType.get(rentingType);
        if (rentingTypeObject != null) {
            return rentingTypeObject.calculateRentingFee(rental);
        }

        return 0;
    }
}
