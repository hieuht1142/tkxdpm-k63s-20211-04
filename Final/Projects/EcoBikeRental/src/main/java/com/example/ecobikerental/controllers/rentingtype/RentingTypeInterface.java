package com.example.ecobikerental.controllers.rentingtype;

import com.example.ecobikerental.entities.Rental;

public interface RentingTypeInterface {
    public long calculateRentingFee(Rental rental);
}
