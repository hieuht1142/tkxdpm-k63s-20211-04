package com.example.ecobikerental.dao.bikedetail;

import com.example.ecobikerental.dao.BaseDAO;
import com.example.ecobikerental.dao.mapper.BikeDetailsMapper;
import com.example.ecobikerental.dto.BikeDetailsDto;

import java.util.List;

public class BikeDetailsDAO extends BaseDAO<BikeDetailsDto> implements IBikeDetailDAO {

    @Override
    public List<BikeDetailsDto> findAllWithDetails() {
        String command = "SELECT b.name, licence_plate, price, position, state, c.name as category_name, s.name as station_name " +
                "FROM bikes b JOIN categories c ON b.category_id = c.id " +
                "JOIN stations s ON b.station_id = s.id ";

        return query(command, new BikeDetailsMapper());
    }

    @Override
    public List<BikeDetailsDto> findAllWithDetails(int offset, int pageSize) {
        String command = "SELECT b.name, licence_plate, price, position, state, c.name as category_name, s.name as station_name " +
                "FROM bikes b JOIN categories c ON b.category_id = c.id " +
                "JOIN stations s ON b.station_id = s.id " +
                "LIMIT ?, ?";

        return query(command, new BikeDetailsMapper(), offset, pageSize);
    }
}
