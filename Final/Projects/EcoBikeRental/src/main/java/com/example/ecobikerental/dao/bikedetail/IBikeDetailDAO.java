package com.example.ecobikerental.dao.bikedetail;

import com.example.ecobikerental.dao.IBaseDAO;
import com.example.ecobikerental.dto.BikeDetailsDto;

import java.util.List;

public interface IBikeDetailDAO extends IBaseDAO<BikeDetailsDto> {
    public List<BikeDetailsDto> findAllWithDetails();
    public List<BikeDetailsDto> findAllWithDetails(int offset, int pageSize);
}
