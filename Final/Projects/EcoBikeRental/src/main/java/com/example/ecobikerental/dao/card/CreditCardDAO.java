package com.example.ecobikerental.dao.card;

import com.example.ecobikerental.dao.BaseDAO;
import com.example.ecobikerental.dao.mapper.CreditCardMapper;
import com.example.ecobikerental.entities.paymentcard.CreditCard;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

public class CreditCardDAO extends BaseDAO<CreditCard> implements ICreditCardDAO {

    @Override
    public int create(CreditCard creditCard) {
        String sql = "INSERT INTO credit_cards (number, holder_name, security_code, created_date, modified_date, " +
                "expiration_date, user_id) VALUES (?, ?, ?, ?, ?, ?, ?)";
        Timestamp now = new Timestamp(new Date().getTime());

        return (int) super.insert(sql, creditCard.getCardCode(), creditCard.getOwner(), creditCard.getCvvCode(), now, now,
                creditCard.getDateExpired(), creditCard.getUser().getId());
    }

    @Override
    public CreditCard find(int userId) {
        String sql = "SELECT * FROM credit_cards WHERE user_id = ?";
        List<CreditCard> cards = super.query(sql, new CreditCardMapper(), userId);

        return cards.size() > 0 ? cards.get(0) : null;
    }

    @Override
    public CreditCard find(String number) {
        String sql = "SELECT * FROM credit_cards WHERE number LIKE ?";
        List<CreditCard> cards = super.query(sql, new CreditCardMapper(), number);

        return cards.size() > 0 ? cards.get(0) : null;
    }
}
