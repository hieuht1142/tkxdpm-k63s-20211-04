package com.example.ecobikerental.dao.card;

import com.example.ecobikerental.dao.IBaseDAO;
import com.example.ecobikerental.entities.paymentcard.CreditCard;

public interface ICreditCardDAO extends IBaseDAO<CreditCard> {
    public int create(CreditCard creditCard);
    public CreditCard find(int userId);
    public CreditCard find(String number);
}
