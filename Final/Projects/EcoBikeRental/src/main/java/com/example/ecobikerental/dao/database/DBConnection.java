package com.example.ecobikerental.dao.database;

import java.sql.Connection;

public abstract class DBConnection {
    public abstract Connection setUpConnection();
}
