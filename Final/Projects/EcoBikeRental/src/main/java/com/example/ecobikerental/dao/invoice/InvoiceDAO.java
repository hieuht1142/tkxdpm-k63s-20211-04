package com.example.ecobikerental.dao.invoice;

import com.example.ecobikerental.dao.BaseDAO;
import com.example.ecobikerental.entities.Invoice;

import java.sql.Timestamp;
import java.util.Date;

public class InvoiceDAO extends BaseDAO<Invoice> implements IInvoiceDAO {

    @Override
    public int create(Invoice invoice) {
        String sql = "INSERT into invoices (invoice_type, total_amount, rental_id, created_date) " +
                "VALUES (?, ?, ?, ?)";
        Timestamp createdDate = new Timestamp(new Date().getTime());

        return (int) super.insert(sql, invoice.getType(), invoice.getTotalAmount(), invoice.getRental().getId(), createdDate);
    }
}
