package com.example.ecobikerental.dao.mapper;

import com.example.ecobikerental.dao.bike.BikeDAO;
import com.example.ecobikerental.dao.user.UserDAO;
import com.example.ecobikerental.entities.Bike;
import com.example.ecobikerental.entities.Rental;
import com.example.ecobikerental.entities.User;

import java.sql.ResultSet;
import java.sql.SQLException;

public class RentalMapper implements IBaseMapper<Rental>{
    private BikeDAO bikeDAO;
    private UserDAO userDAO;

    public RentalMapper(){
        bikeDAO = new BikeDAO();
        userDAO = new UserDAO();
    }

    @Override
    public Rental mapRow(ResultSet rs) {
        Rental rental = new Rental();
        try {
            rental.setId(rs.getInt("id"));
            rental.setRentingType(rs.getInt("renting_type"));
            rental.setStartTime(rs.getTimestamp("start_time"));
            rental.setEndTime(rs.getTimestamp("end_time"));
            rental.setRentingFee(rs.getLong("renting_fee"));
            rental.setState(rs.getInt("state"));
            rental.setDeposit(rs.getLong("deposit"));
            rental.setStatus(rs.getInt("status"));
            rental.setCreatedDate(rs.getTimestamp("created_date"));
            rental.setCreatedBy(rs.getString("created_by"));
            rental.setModifiedDate(rs.getTimestamp("modified_date"));
            rental.setModifiedBy(rs.getString("modified_by"));
            int bikeId = rs.getInt("bike_id");
            if(bikeId != 0){
                Bike bike = bikeDAO.find(bikeId);
                rental.setBike(bike);
            }
            int userId = rs.getInt("user_id");
            if(userId != 0){
                User user = userDAO.find(userId);
                rental.setRenter(user);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return rental;
    }
}
