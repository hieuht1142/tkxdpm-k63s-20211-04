package com.example.ecobikerental.dao.rental;

import com.example.ecobikerental.dao.IBaseDAO;
import com.example.ecobikerental.entities.Rental;

import java.util.List;

public interface IRentalDAO extends IBaseDAO<Rental> {
    public Rental find(int id);
    public void update(Rental rental);
    public List<Rental> findByUserId(int id);
    public int create(Rental rental);
}
