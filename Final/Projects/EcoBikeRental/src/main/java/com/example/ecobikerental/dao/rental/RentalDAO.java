package com.example.ecobikerental.dao.rental;

import com.example.ecobikerental.dao.BaseDAO;
import com.example.ecobikerental.dao.mapper.RentalMapper;
import com.example.ecobikerental.entities.Invoice;
import com.example.ecobikerental.entities.Rental;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

public class RentalDAO extends BaseDAO<Rental> implements IRentalDAO {

    @Override
    public Rental find(int id) {
        String sql = "SELECT * from rentals WHERE id = ?";
        List<Rental> rentals = super.query(sql, new RentalMapper(), id);

        return rentals.size() > 0 ? rentals.get(0) : null;
    }

    @Override
    public void update(Rental rental) {
        String sql = "UPDATE rentals set end_time = ?, renting_fee = ?, state = ?, modified_date = ? WHERE id = ?";
        Timestamp now = new Timestamp(new Date().getTime());
        super.updateOrDelete(sql, rental.getEndTime(), rental.getRentingFee(), rental.getState(), now, rental.getId());
    }

    @Override
    public List<Rental> findByUserId(int id) {
        String sql = "SELECT * from rentals WHERE user_id = ?";
        return super.query(sql, new RentalMapper(), id);
    }

    @Override
    public int create(Rental rental) {
        String sql = "INSERT into rentals (renting_type, start_time, bike_id, user_id, created_date, deposit) " +
                "VALUES (?, ?, ?, ?, ?, ?)";
        Timestamp createdDate = new Timestamp(new Date().getTime());

        return (int) super.insert(sql, rental.getRentingType(), rental.getStartTime(), rental.getBike().getId(),
                rental.getRenter().getId(), createdDate, rental.getDeposit());
    }
}
