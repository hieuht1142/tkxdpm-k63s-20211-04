package com.example.ecobikerental.dao.user;

import com.example.ecobikerental.dao.IBaseDAO;
import com.example.ecobikerental.entities.User;

import java.util.List;

public interface IUserDAO extends IBaseDAO<User> {
    User find(int id);
    List<User> find(String username, String password, int status);
}
