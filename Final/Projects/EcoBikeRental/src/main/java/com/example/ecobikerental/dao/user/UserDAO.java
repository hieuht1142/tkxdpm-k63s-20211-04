package com.example.ecobikerental.dao.user;

import com.example.ecobikerental.dao.BaseDAO;
import com.example.ecobikerental.dao.mapper.UserMapper;
import com.example.ecobikerental.entities.User;

import java.util.List;

public class UserDAO extends BaseDAO<User> implements IUserDAO {

    @Override
    public User find(int id) {
        String sql = "SELECT * FROM users WHERE id = ?";
        List<User> users = super.query(sql, new UserMapper(), id);

        return users.size() > 0 ? users.get(0) : null;
    }

    @Override
    public List<User> find(String username, String password, int status) {
        String sql = "SELECT * FROM users WHERE username LIKE ? AND password LIKE ? AND status = ?";

        return query(sql, new UserMapper(), username, password, status);
    }
}
