package com.example.ecobikerental.entities;

import com.example.ecobikerental.exceptions.BikeNotAvailableException;
import com.example.ecobikerental.enums.bike.State;

import java.security.SecureRandom;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.Map;

public class Bike extends BaseEntity {
    private String name;
    private String bikeCode;
    private Float weight;
    private String licencePlate;
    private Date manufacturingDate;
    private String producer;
    private Long price;
    private Integer position;
    private Integer state;
    private Integer batteryPercentage;
    private Integer loadCycles;
    private Integer timeRemaining;
    private Category category;
    private Station station;
    private String image;
    private Long deposit;

    public Bike() {}

    public Bike(Map<String, Object> bikeInfos) {
        this.name = (String) bikeInfos.get("name");
        this.image = (String) bikeInfos.get("image");
        this.weight = (Float) bikeInfos.get("weight");
        this.licencePlate = (String) bikeInfos.get("licencePlate");
        LocalDate tmpDate = (LocalDate) bikeInfos.get("manufacturingDate");
        if (tmpDate != null) {
            this.manufacturingDate = Date.from(tmpDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
        } else {
            this.manufacturingDate = null;
        }
        this.producer = (String) bikeInfos.get("producer");
        this.price = (Long) bikeInfos.get("price");
        this.position = (Integer) bikeInfos.get("position");
        this.state = State.AVAILABLE;
        this.batteryPercentage = (Integer) bikeInfos.get("batteryPercentage");
        this.loadCycles = (Integer) bikeInfos.get("loadCycles");
        this.timeRemaining = (Integer) bikeInfos.get("remainTime");
        this.category = (Category) bikeInfos.get("category");
        this.station = (Station) bikeInfos.get("station");
        this.bikeCode = generateBikeCode();
    }

    private String generateBikeCode() {
        final String DELIMITER = " ";
        String[] tokens = category.getName().split(DELIMITER);

        String prefix = "";
        for (String token: tokens) {
            prefix += token.charAt(0);
        }
        prefix += "-";

        SecureRandom sran = new SecureRandom();
        return prefix + sran.nextInt(1000000);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBikeCode() {
        return bikeCode;
    }

    public void setBikeCode(String bikeCode) {
        this.bikeCode = bikeCode;
    }

    public Float getWeight() {
        return weight;
    }

    public void setWeight(float weight) {
        this.weight = weight;
    }

    public String getLicencePlate() {
        return licencePlate;
    }

    public void setLicencePlate(String licencePlate) {
        this.licencePlate = licencePlate;
    }

    public Date getManufacturingDate() {
        return manufacturingDate;
    }

    public void setManufacturingDate(Date manufacturingDate) {
        this.manufacturingDate = manufacturingDate;
    }

    public String getProducer() {
        return producer;
    }

    public void setProducer(String producer) {
        this.producer = producer;
    }

    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    public Integer getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public Integer getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public Integer getBatteryPercentage() {
        return batteryPercentage;
    }

    public void setBatteryPercentage(int batteryPercentage) {
        this.batteryPercentage = batteryPercentage;
    }

    public Integer getLoadCycles() {
        return loadCycles;
    }

    public void setLoadCycles(int loadCycles) {
        this.loadCycles = loadCycles;
    }

    public Integer getTimeRemaining() {
        return timeRemaining;
    }

    public void setTimeRemaining(int timeRemaining) {
        this.timeRemaining = timeRemaining;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Station getStation() {
        return station;
    }

    public void setStation(Station station) {
        this.station = station;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getStationName() {
        return station.getName();
    }

    public String getCategoryName() {
        return category.getName();
    }

    public Long getDeposit() {
        return deposit;
    }

    public void setDeposit(long deposit) {
        this.deposit = deposit;
    }

    public boolean checkBikeAvailability() throws SQLException {
        return getState() != State.RENTED;
    }
}
