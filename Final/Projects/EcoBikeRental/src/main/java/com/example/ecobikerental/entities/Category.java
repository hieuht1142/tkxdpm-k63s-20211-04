package com.example.ecobikerental.entities;

public class Category extends BaseEntity{
    private String name;
    private long deposit;
    private float priceRate;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getDeposit() {
        return deposit;
    }

    public void setDeposit(long deposit) {
        this.deposit = deposit;
    }

    public float getPriceRate() {
        return priceRate;
    }

    public void setPriceRate(float priceRate) {
        this.priceRate = priceRate;
    }

    public String toString() {
        return name;
    }
}
