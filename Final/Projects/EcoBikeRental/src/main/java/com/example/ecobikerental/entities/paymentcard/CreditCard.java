package com.example.ecobikerental.entities.paymentcard;

import com.example.ecobikerental.enums.payment.Card;

import java.sql.Timestamp;
import java.util.Date;

public class CreditCard extends PaymentCard {
    private String cardCode;
    private String owner;
    private String cvvCode;
    private String dateExpired;

    static {
        PaymentCardFactory.getInstance().registerCard(Card.CREDIT_CARD, new CreditCard());
    }

    @Override
    public CreditCard createCard() {
        return new CreditCard();
    }

    public CreditCard() {}

    public CreditCard(String cardCode, String owner, String cvvCode, String dateExpired) {
        this.cardCode = cardCode;
        this.owner = owner;
        this.cvvCode = cvvCode;
        this.dateExpired = dateExpired;
        this.createdDate = new Timestamp(new Date().getTime());
    }

    public String getCardCode() {
        return cardCode;
    }

    public void setCardCode(String cardCode) {
        this.cardCode = cardCode;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getCvvCode() {
        return cvvCode;
    }

    public void setCvvCode(String cvvCode) {
        this.cvvCode = cvvCode;
    }

    public String getDateExpired() {
        return dateExpired;
    }

    public void setDateExpired(String dateExpired) {
        this.dateExpired = dateExpired;
    }

    @Override
    public String toString() {
        return "{" +
                super.toString() +
                " cardCode='" + cardCode + "'" +
                ", owner='" + owner + "'" +
                ", cvvCode='" + cvvCode + "'" +
                ", dateExpired='" + dateExpired + "'" +
                ", user='" + user + "'" +
                "}";
    }
}
