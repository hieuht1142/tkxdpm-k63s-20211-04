package com.example.ecobikerental.entities.paymentcard;

import com.example.ecobikerental.entities.BaseEntity;
import com.example.ecobikerental.entities.User;

public abstract class PaymentCard extends BaseEntity {

    protected User user;

    public abstract PaymentCard createCard();

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
