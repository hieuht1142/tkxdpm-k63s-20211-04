package com.example.ecobikerental.entities.paymentcard;

import java.util.HashMap;

public class PaymentCardFactory {
    private HashMap registeredCards = new HashMap();
    private static PaymentCardFactory instance = new PaymentCardFactory();

    public static PaymentCardFactory getInstance() {
        return instance;
    }

    public void registerCard(String cardType, PaymentCard card) {
        registeredCards.put(cardType, card);
    }

    public PaymentCard createCard(String cardType) {
        return ((PaymentCard) registeredCards.get(cardType)).createCard();
    }
}
