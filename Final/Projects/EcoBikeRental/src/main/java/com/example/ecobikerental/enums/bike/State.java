package com.example.ecobikerental.enums.bike;

public final class State {
    public static final int AVAILABLE = 1;
    public static final int RENTED = 0;
}
