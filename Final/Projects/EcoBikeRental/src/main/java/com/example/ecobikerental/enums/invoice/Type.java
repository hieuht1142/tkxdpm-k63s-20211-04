package com.example.ecobikerental.enums.invoice;

public final class Type {
    public static final int RENTING_INVOICE = 1;
    public static final int DEPOSIT_INVOICE = 0;
    public static final int REFUND_INVOICE = 2;

    public static String[] CONTENT = {"Đặt cọc phí thuê xe tại EcoBike", "Thanh toán phí thuê xe tại EcoBike", "EcoBike hoàn tiền cọc thuê xe"};
}
