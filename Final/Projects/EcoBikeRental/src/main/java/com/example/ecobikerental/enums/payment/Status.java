package com.example.ecobikerental.enums.payment;

public final class Status {
    public static final int SUCCESSFUL = 1;
    public static final int FAILED = 0;
}
