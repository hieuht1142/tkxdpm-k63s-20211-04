package com.example.ecobikerental.enums.rental;

public final class State {
    public static final int IN_PROGRESS = 1;
    public static final int FINISHED = 0;

    public static final String[] state = {"Hoàn thành", "Đang thuê"};
}
