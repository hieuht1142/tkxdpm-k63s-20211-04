package com.example.ecobikerental.exceptions;

/**
 * BikeNotAvailableException is thrown when the bike is nto available for being rented.
 *
 * @author Vu Minh Hieu
 */
public class BikeNotAvailableException extends GeneralException {

    public BikeNotAvailableException() {
    }

    public BikeNotAvailableException(String message) {
        super(message);
    }

}