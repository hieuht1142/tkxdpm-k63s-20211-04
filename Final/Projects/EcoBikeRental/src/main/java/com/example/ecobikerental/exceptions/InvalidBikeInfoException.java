package com.example.ecobikerental.exceptions;

public class InvalidBikeInfoException extends AddBikeException {

    public InvalidBikeInfoException(String message) {
        super(message);
    }
}
