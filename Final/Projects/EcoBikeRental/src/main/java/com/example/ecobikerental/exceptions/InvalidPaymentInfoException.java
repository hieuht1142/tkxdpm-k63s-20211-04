package com.example.ecobikerental.exceptions;

import com.example.ecobikerental.exceptions.PaymentException;

public class InvalidPaymentInfoException extends PaymentException {
    public InvalidPaymentInfoException(String message) {
        super(message);
    }
}
