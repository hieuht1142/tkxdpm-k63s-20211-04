package com.example.ecobikerental.exceptions;;

public class PaymentException extends RuntimeException {
	public PaymentException(String message) {
		super(message);
	}
}
