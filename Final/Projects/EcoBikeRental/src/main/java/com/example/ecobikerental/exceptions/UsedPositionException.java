package com.example.ecobikerental.exceptions;

public class UsedPositionException extends AddBikeException {

    public UsedPositionException() {
        super("Possion has been used!");
    }
}
