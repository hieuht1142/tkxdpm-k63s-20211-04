package com.example.ecobikerental.handlers;

import com.example.ecobikerental.controllers.AuthController;
import com.example.ecobikerental.controllers.BaseController;
import com.example.ecobikerental.handlers.auth.LoginHandler;
import com.example.ecobikerental.utils.Configs;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import com.example.ecobikerental.handlers.user.HomeHandler;
import javafx.scene.Scene;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.Hashtable;
import java.util.ResourceBundle;

public class BaseHandler extends FXMLHandler implements Initializable {
    @FXML
    protected ImageView logoutBtn;

    private Scene scene;
    private BaseHandler prev;
    protected final Stage stage;
    protected HomeHandler homeHandler;
    protected Hashtable<String, String> messages;
    private BaseController bController;

    public BaseHandler(String screenPath) throws IOException {
        super(screenPath);
        this.stage = new Stage();
    }

    public BaseHandler(Stage stage, String screenPath) throws IOException {
        super(screenPath);
        this.stage = stage;
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        setLogoutBtnListener();
    }

    public void setPreviousScreen(BaseHandler prev) {
        this.prev = prev;
    }

    public BaseHandler getPreviousScreen() {
        return this.prev;
    }

    public void show() {
        if (this.scene == null) {
            this.scene = new Scene(this.content);
        }
        this.stage.setScene(this.scene);
        this.stage.show();
    }

    public void setScreenTitle(String string) {
        this.stage.setTitle(string);
    }

    public void setBController(BaseController bController) {
        this.bController = bController;
    }

    public BaseController getBController() {
        return this.bController;
    }

    public void forward(Hashtable messages) {
        this.messages = messages;
    }

    public void setHomeHandler(HomeHandler homeHandler) {
        this.homeHandler = homeHandler;
    }

    public void goBackToPreviousScreen() throws IOException {
        getPreviousScreen().show();
    }

    public void requestToLogout() throws IOException {
        AuthController authController = new AuthController();
        authController.logout();

        LoginHandler loginHandler = new LoginHandler(this.stage, Configs.LOGIN_PATH);
        loginHandler.setScreenTitle("Login");
        loginHandler.show();
    }

    private void setLogoutBtnListener() {
        if (logoutBtn != null) {
            logoutBtn.setOnMouseClicked(e -> {
                try {
                    requestToLogout();
                } catch (IOException exp) {
                    exp.printStackTrace();
                }
            });
        }
    }
}
