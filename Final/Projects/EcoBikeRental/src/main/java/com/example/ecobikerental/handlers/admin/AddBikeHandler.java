package com.example.ecobikerental.handlers.admin;

import com.example.ecobikerental.exceptions.AddBikeException;
import com.example.ecobikerental.exceptions.InvalidBikeInfoException;
import com.example.ecobikerental.controllers.AddBikeController;
import com.example.ecobikerental.entities.Category;
import com.example.ecobikerental.entities.Station;
import com.example.ecobikerental.handlers.BaseHandler;
import com.example.ecobikerental.utils.Toast;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

public class AddBikeHandler extends AdminBaseHandler {

    @FXML
    private AnchorPane bikePane;

    @FXML
    private TextField bikeNameInput;

    @FXML
    private ComboBox<Category> categoryInput;

    @FXML
    private TextField weightInput;

    @FXML
    private TextField licensePlateInput;

    @FXML
    private DatePicker manufacturingDateInput;

    @FXML
    private TextField priceInput;

    @FXML
    private ComboBox<Station> stationInput;

    @FXML
    private TextField positionInput;

    @FXML
    private TextField producerInput;

    @FXML
    private TextField bateryPercentageInput;

    @FXML
    private TextField loadCycleInput;

    @FXML
    private TextField remainTimeInput;

    @FXML
    private Button chooseFileButton;

    @FXML
    private ImageView bikeImage;

    @FXML
    private Button addButton;

    private AddBikeController addBikeController;

    private File imageFile;

    public AddBikeHandler(Stage stage, String screenPath) throws IOException {
        super(stage, screenPath);
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        super.initialize(url, resourceBundle);
        setChangeObserverForAllNumberFields(weightInput, priceInput, positionInput,
                loadCycleInput, bateryPercentageInput, remainTimeInput);

        addBikeController = new AddBikeController();
        ObservableList<Category> categoryItems = FXCollections.observableList(addBikeController.initializeCategories());
        categoryInput.setItems(categoryItems);
        ObservableList<Station> stationItems = FXCollections.observableList(addBikeController.initializeStations());
        stationInput.setItems(stationItems);

        chooseFileButton.setOnAction(event -> {
            onClickChooseFileButton();
        });
        addButton.setOnAction(event -> {
            confirmToAddBike();
        });
    }

    private void setChangeObserverForNumberField(TextField numberField) {
        numberField.textProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null && !newValue.matches("\\d*")) {
                numberField.setText(newValue.replaceAll("[^\\d]", ""));
            }
        });
    }

    private void setChangeObserverForAllNumberFields(TextField... numberFields) {
        for (TextField numberField : numberFields) {
            setChangeObserverForNumberField(numberField);
        }
    }

    public void onClickChooseFileButton() {
        Stage stage = (Stage) bikePane.getScene().getWindow();
        FileChooser imageChooser = new FileChooser();
        imageChooser.setTitle("Choose bike image");
        FileChooser.ExtensionFilter imageFilter = new FileChooser.ExtensionFilter("Image files", "*.jpg", "*.png", "*.jpeg");
        imageChooser.getExtensionFilters().add(imageFilter);
        imageFile = imageChooser.showOpenDialog(stage);

        if (imageFile != null) {
            Image image = new Image(imageFile.toURI().toString(), 283, 218, false, true);
            bikeImage.setImage(image);
        }
    }

    public void confirmToAddBike() {
        Map<String, Object> bikeInfos = collectBikeInfos();
        try {
            addBikeController.addBike(bikeInfos);
            Toast.makeText(stage, "Add bike successful!", "success");
            resetInput();
        } catch (AddBikeException ex) {
            Toast.makeText(stage, ex.getMessage(), "error");
        } catch (IOException e) {
            Toast.makeText(stage, "An error occur when saving bike image!", "error");
        }
    }

    private Map<String, Object> collectBikeInfos() {
        Map<String, Object> bikeInfos = new HashMap<>();

        bikeInfos.put("name", bikeNameInput.getText());
        bikeInfos.put("category", categoryInput.getSelectionModel().getSelectedItem());
        bikeInfos.put("weight", weightInput.getText());
        bikeInfos.put("licencePlate", licensePlateInput.getText());
        bikeInfos.put("manufacturingDate", manufacturingDateInput.getValue());
        bikeInfos.put("price", priceInput.getText());
        bikeInfos.put("station", stationInput.getSelectionModel().getSelectedItem());
        bikeInfos.put("position", positionInput.getText());
        bikeInfos.put("producer", producerInput.getText());
        bikeInfos.put("bateryPercentage", bateryPercentageInput.getText());
        bikeInfos.put("loadCycles", loadCycleInput.getText());
        bikeInfos.put("remainTime", remainTimeInput.getText());
        bikeInfos.put("imageFile", imageFile);

        return bikeInfos;
    }

    private void resetInput() {
        bikeNameInput.setText(null);
        categoryInput.getSelectionModel().clearSelection();
        weightInput.setText(null);
        licensePlateInput.setText(null);
        manufacturingDateInput.setValue(null);
        priceInput.setText(null);
        stationInput.getSelectionModel().clearSelection();
        positionInput.setText(null);
        producerInput.setText(null);
        bateryPercentageInput.setText(null);
        loadCycleInput.setText(null);
        bateryPercentageInput.setText(null);
        loadCycleInput.setText(null);
        remainTimeInput.setText(null);
        bikeImage.setImage(null);
        imageFile = null;
    }
}
