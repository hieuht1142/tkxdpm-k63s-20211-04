package com.example.ecobikerental.handlers.user;

import com.example.ecobikerental.controllers.PaymentController;
import com.example.ecobikerental.controllers.RentalController;
import com.example.ecobikerental.controllers.ReturnBikeController;
import com.example.ecobikerental.entities.Bike;
import com.example.ecobikerental.entities.Invoice;
import com.example.ecobikerental.entities.Rental;
import com.example.ecobikerental.entities.User;
import com.example.ecobikerental.enums.invoice.Type;
import com.example.ecobikerental.handlers.BaseHandler;
import com.example.ecobikerental.utils.Configs;
import com.jfoenix.controls.JFXButton;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;

public class InvoiceHandler extends UserBaseHandler implements Initializable, PaymentResultHandler {
    @FXML
    private Text deposit;
    @FXML
    private Text total;
    @FXML
    private Text refund;
    @FXML
    private Text renter;
    @FXML
    private Text station;
    @FXML
    private Text bikeCode;
    @FXML
    private Text bikeName;
    @FXML
    private Text category;
    @FXML
    private Text fromTime;
    @FXML
    private Text toTime;
    @FXML
    private Text totalTime;
    @FXML
    private Text type;
    @FXML
    private JFXButton btnCancel;
    @FXML
    private JFXButton btnConfirm;

    private Invoice invoice;

    public InvoiceHandler(Stage stage, String screenPath, Invoice invoice) throws IOException {
        super(stage, screenPath);
        this.invoice = invoice;
        setInvoice(invoice);

        btnConfirm.setOnMouseClicked(event -> {
            try {
                PaymentHandler paymentHandler = new PaymentHandler(this.stage, Configs.PAYMENT_PATH, invoice, Type.CONTENT[invoice.getType()]);
                paymentHandler.setBController(new PaymentController());
                paymentHandler.setPreviousScreen(this);
                paymentHandler.setHomeHandler(this.homeHandler);
                paymentHandler.init();
                paymentHandler.setScreenTitle("Payment Form");
                paymentHandler.show();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        btnCancel.setOnMouseClicked(event -> {
            try {
                RentingHistoryHandler rentingHistoryHandler = new RentingHistoryHandler(this.stage, Configs.VIEW_RENTING_HISTORY_PATH, new RentalController());
                rentingHistoryHandler.setScreenTitle("Renting History");
                rentingHistoryHandler.show();
            } catch (IOException exp) {
                exp.printStackTrace();
            }
        });
    }


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        super.initialize(url, resourceBundle);
    }

    public void setInvoice(Invoice invoice){
        this.invoice = invoice;
        long depositFee = invoice.getRental().getDeposit();
        long rentingFee = invoice.getRental().getRentingFee();
        deposit.setText(String.valueOf(depositFee));
        total.setText(String.valueOf(rentingFee));
        if(depositFee < rentingFee){
            type.setText("Amount:");
            refund.setText(String.valueOf(rentingFee - depositFee));
            this.invoice.setType(Type.RENTING_INVOICE);
            this.invoice.setTotalAmount(rentingFee - depositFee);
        } else {
            refund.setText(String.valueOf(depositFee - rentingFee));
            type.setText("Refund:");
            this.invoice.setType(Type.REFUND_INVOICE);
            this.invoice.setTotalAmount(depositFee - rentingFee);
        }
        Rental rental = invoice.getRental();
        Bike bike = rental.getBike();
        User user = rental.getRenter();
        renter.setText(user.getName());
        station.setText(bike.getStation().getName());
        bikeCode.setText(bike.getBikeCode());
        bikeName.setText(bike.getName());
        category.setText(bike.getCategory().getName());
        fromTime.setText(rental.getStartTime().toString());
        toTime.setText(rental.getEndTime().toString());

        long minus = (rental.getEndTime().getTime() - rental.getStartTime().getTime())/(1000 * 60);

        totalTime.setText(String.valueOf(minus));
    }

    @Override
    public void paymentSuccess(Invoice invoice) {
        ReturnBikeController controller = (ReturnBikeController)getBController();
        controller.paymentSuccess(invoice);
    }
}
