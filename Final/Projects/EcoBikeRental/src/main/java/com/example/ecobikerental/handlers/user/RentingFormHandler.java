package com.example.ecobikerental.handlers.user;

import com.example.ecobikerental.controllers.AuthController;
import com.example.ecobikerental.entities.User;
import com.example.ecobikerental.exceptions.InvalidRentingInfoException;
import com.example.ecobikerental.controllers.RentBikeController;
import com.example.ecobikerental.entities.Bike;
import com.example.ecobikerental.entities.Invoice;
import com.example.ecobikerental.entities.Rental;
import com.example.ecobikerental.enums.rental.RentingType;
import com.example.ecobikerental.handlers.BaseHandler;
import com.example.ecobikerental.utils.Configs;
import com.example.ecobikerental.utils.Toast;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.ResourceBundle;

public class RentingFormHandler extends UserBaseHandler implements Initializable {
    @FXML
    private JFXComboBox<String> comboBox;
    @FXML
    private JFXButton resetBtn;
    @FXML
    private JFXButton submitBtn;
    @FXML
    private JFXButton backBtn;
    @FXML
    private ImageView image;
    @FXML
    private Text name;
    @FXML
    private Text bikeCode;
    @FXML
    private Text station;
    @FXML
    private Text deposit;
    @FXML
    private TextField nameTextField;
    @FXML
    private TextField phoneTextField;

    private Bike bike;

    public RentingFormHandler(Stage stage, String screenPath, Bike bike) throws IOException {
        super(stage, screenPath);
        this.bike = bike;
        setImage(image, bike.getImage());
        name.setText(bike.getName());
        bikeCode.setText(bike.getBikeCode());
        station.setText(bike.getStationName());
        deposit.setText(bike.getDeposit() + " VND");
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        super.initialize(url, resourceBundle);
        setBController(RentBikeController.getInstance());
        setUserInfo();
        comboBox.getItems().addAll(RentingType.ALL);
        resetBtn.setOnMouseClicked(e -> {
            reset();
        });
        backBtn.setOnMouseClicked(e -> {
            goBack();
        });
        submitBtn.setOnMouseClicked(e -> {
            try {
                submitRentingInfo();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        });
    }

    public RentBikeController getBController() {
        return (RentBikeController) super.getBController();
    }

    private void setUserInfo() {
        AuthController authController = new AuthController();
        User user = authController.getAuthUser();
        nameTextField.setText(user.getName());
        phoneTextField.setText(user.getPhoneNumber());
    }

    private void reset() {
        comboBox.valueProperty().set(null);
    }

    private void submitRentingInfo() throws IOException {
        try {
            HashMap<String, String> rentingInfo = new HashMap<>();
            rentingInfo.put("name", nameTextField.getText());
            rentingInfo.put("phoneNumber", phoneTextField.getText());
            rentingInfo.put("rentingType", comboBox.getValue());
            Rental rental = getBController().processRentingInfo(bike, rentingInfo);
            Invoice invoice = getBController().createInvoice(rental);

            displayDepositInvoice(invoice);
        } catch (InvalidRentingInfoException e) {
            Toast.makeText(stage, e.getMessage(), "error");
        }
    }

    private void displayDepositInvoice(Invoice invoice) throws IOException {
        DepositInvoiceHandler depositInvoiceHandler = new DepositInvoiceHandler(stage, Configs.DEPOSIT_INVOICE_PATH, invoice);
        depositInvoiceHandler.setBController(getBController());
        depositInvoiceHandler.setPreviousScreen(this);
        depositInvoiceHandler.setScreenTitle("Deposit Invoice Screen");
        depositInvoiceHandler.show();
    }

    private void goBack() {
        getPreviousScreen().show();
    }
}
