package com.example.ecobikerental.handlers.user;

import com.example.ecobikerental.controllers.AuthController;
import com.example.ecobikerental.controllers.BaseController;
import com.example.ecobikerental.controllers.RentalController;
import com.example.ecobikerental.dao.rental.RentalDAO;
import com.example.ecobikerental.entities.Rental;
import com.example.ecobikerental.entities.User;
import com.example.ecobikerental.enums.rental.State;
import com.example.ecobikerental.handlers.BaseHandler;
import com.example.ecobikerental.utils.Configs;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class RentingHistoryHandler extends UserBaseHandler implements Initializable {
    @FXML
    public TableView<Rental> table;
    @FXML
    public TableColumn<Rental, Integer> IdColumn;
    @FXML
    public TableColumn<Rental, String> BikeNameColumn;
    @FXML
    public TableColumn<Rental, Timestamp> StartTimeColumn;
    @FXML
    public TableColumn<Rental, Timestamp> EndTimeColumn;
    @FXML
    public TableColumn<Rental, String> StatusColumn;

    private ObservableList<Rental> RentalList;

    public RentingHistoryHandler(Stage stage, String screenPath, BaseController baseController) throws IOException {
        super(stage, screenPath);
        setBController(baseController);
    }

    @Override
    public RentalController getBController() {
        return (RentalController)super.getBController();
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        super.initialize(url, resourceBundle);
        setBController(RentalController.getInstance());
        getRentingHistoryView();
        getRentingStatusView();
    }

    private void getRentingHistoryView() {
        List<Rental> list = getBController().getCurrentUserRental();
        RentalList = FXCollections.observableArrayList();
        for (Rental s : list) {
            RentalList.add(s);
        }
        IdColumn.setCellValueFactory(new PropertyValueFactory<Rental, Integer>("id"));
        BikeNameColumn.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getBike().getName()));
        StartTimeColumn.setCellValueFactory(new PropertyValueFactory<Rental, Timestamp>("startTime"));
        EndTimeColumn.setCellValueFactory(new PropertyValueFactory<Rental, Timestamp>("endTime"));
        StatusColumn.setCellValueFactory(cellData -> new SimpleStringProperty(State.state[cellData.getValue().getState()]));
        table.setItems(RentalList);
    }

    private void getRentingStatusView() {
        table.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
            if (newSelection != null) {
                try {
                    ViewRentingStatusHandler viewRentingStatusHandler = new ViewRentingStatusHandler(this.stage, Configs.VIEW_RENTING_STATUS_PATH, newSelection);
                    viewRentingStatusHandler.setScreenTitle("Renting Status");
                    viewRentingStatusHandler.setBController(getBController());
                    viewRentingStatusHandler.displayRentingStatus(table.getSelectionModel().getSelectedItem());
                    viewRentingStatusHandler.show();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        });
    }
}
