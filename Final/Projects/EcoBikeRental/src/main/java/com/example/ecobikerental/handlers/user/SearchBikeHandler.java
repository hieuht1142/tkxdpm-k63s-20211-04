package com.example.ecobikerental.handlers.user;

import com.example.ecobikerental.controllers.BikeController;
import com.example.ecobikerental.controllers.CategoryController;
import com.example.ecobikerental.entities.Bike;
import com.example.ecobikerental.entities.Paginator;
import com.example.ecobikerental.handlers.BaseHandler;
import com.example.ecobikerental.utils.Configs;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Pagination;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.util.Callback;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class SearchBikeHandler extends UserBaseHandler implements Initializable {
    @FXML
    private TextField searchTextField;
    @FXML
    private JFXComboBox<String> comboBox;
    @FXML
    private JFXButton searchBtn;
    @FXML
    private JFXButton clearBtn;
    @FXML
    private Pagination pagination;
    @FXML
    private HBox hBox;

    private List<Bike> bikes;
    private List bikeComponents;
    private int total;
    private String category;

    public SearchBikeHandler(Stage stage, String screenPath) throws IOException {
        super(stage, screenPath);
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        super.initialize(url, resourceBundle);
        setBController(new BikeController());
        setPageFactory();

        CategoryController categoryController = new CategoryController();
        comboBox.getItems().addAll(categoryController.getAllCategoryNames());
        comboBox.setOnAction(this::getCategory);

        searchBtn.setOnMouseClicked(event -> {
            setPageFactory();
        });

        clearBtn.setOnMouseClicked(event -> {
            clearFilter();
        });
    }

    public BikeController getBController() {
        return (BikeController) super.getBController();
    }

    private void getPageBikes(int pageIndex) {
        Paginator paginator = requestToGetBikes(pageIndex * Configs.PER_PAGE);
        this.bikes = paginator.getData();
        bikeComponents = new ArrayList();
        try {
            for (Bike bike : this.bikes) {
                BikeHandler bikeHandler = new BikeHandler(Configs.BIKE_PATH, bike, this.stage, this);
                bikeComponents.add(bikeHandler);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        addBikeItem(bikeComponents);
        total = paginator.getTotal();
        int totalPage = this.total % Configs.PER_PAGE == 0 ? this.total / Configs.PER_PAGE : (this.total / Configs.PER_PAGE + 1);
        pagination.setPageCount(totalPage);
        pagination.setCurrentPageIndex(pageIndex);
    }

    private Paginator requestToGetBikes(int skip) {
        Paginator paginator;
        String keyword = searchTextField.getText();

        if (keyword != null && category != null) {
            paginator = getBController().searchBike(keyword, category, skip);
        } else if (keyword != null) {
            paginator = getBController().searchByKeyword(keyword, skip);
        } else if (category != null) {
            paginator = getBController().searchByCategory(category, skip);
        } else {
            paginator = getBController().getAllBikes(skip);
        }

        return paginator;
    }

    private void addBikeItem(List<BikeHandler> items) {
        ArrayList bikeItems = (ArrayList) ((ArrayList) items).clone();
        hBox.getChildren().forEach(node -> {
            VBox vBox = (VBox) node;
            vBox.getChildren().clear();
        });
        while (!bikeItems.isEmpty()) {
            hBox.getChildren().forEach(node -> {
                VBox vBox = (VBox) node;
                while (vBox.getChildren().size() < 2 && !bikeItems.isEmpty()) {
                    BikeHandler bikeHandler = (BikeHandler) bikeItems.get(0);
                    vBox.getChildren().add(bikeHandler.getContent());
                    bikeItems.remove(bikeHandler);
                }
            });

            return;
        }
    }

    private void getCategory(ActionEvent event) {
        category = comboBox.getValue();
    }

    private void clearFilter() {
        searchTextField.setText(null);
        comboBox.valueProperty().set(null);
        setPageFactory();
    }

    private void setPageFactory() {
        pagination.setPageFactory(new Callback<Integer, Node>() {
            @Override
            public Node call(Integer param) {
                getPageBikes(param);
                return hBox;
            }
        });
    }
}
