package com.example.ecobikerental.handlers.user;

import com.example.ecobikerental.entities.Station;
import com.example.ecobikerental.handlers.BaseHandler;
import com.example.ecobikerental.utils.Configs;
import com.jfoenix.controls.JFXButton;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class SearchStationResultHandler extends UserBaseHandler implements Initializable {

    private List<Station> stations;
    private int page;
    private List<Pane> stationPanes;

    @FXML
    private Pane stationOne;

    @FXML
    private Pane stationTwo;

    @FXML
    private Pane stationThree;

    @FXML
    private Pane stationFour;

    @FXML
    private JFXButton btnNext;

    @FXML
    private JFXButton btnPrevious;

    @FXML
    private JFXButton btnBack;

    @FXML
    private Label noStations;

    @FXML
    private Label searchingResult;

    public SearchStationResultHandler(Stage stage, String screenPath, List<Station> stations) throws IOException {
        super(stage, screenPath);
        this.stations = stations;
        this.page = 0;
        this.stationPanes = new ArrayList<>();
        this.stationPanes.add(this.stationOne);
        this.stationPanes.add(this.stationTwo);
        this.stationPanes.add(this.stationThree);
        this.stationPanes.add(this.stationFour);
        displayStations(this.stations);
        this.btnPrevious.setDisable(true);
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        super.initialize(url, resourceBundle);
        this.btnPrevious.setOnMouseClicked(e -> {
            this.showPreviousPage();
        });
        this.btnNext.setOnMouseClicked(e -> {
            this.showNextPage();
        });
        this.btnBack.setOnMouseClicked(e -> {
            try {
                goBackToPreviousScreen();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        });
        this.noStations.setVisible(false);
    }

    /**
     * Display station in the station list
     * @param stationList: the station list
     */
    public void displayStations(List<Station> stationList) {
        int stationNumber = stationList.size();
        if (stationNumber == 0) {
            notifyNotFoundStations();
            return;
        }
        searchingResult.setVisible(true);
        searchingResult.setText("Searching returns " + String.valueOf(stationNumber) + " station");
        if (stationNumber > 1) {
            searchingResult.setText((searchingResult.getText() + "s"));
        }
        for (int i = 0; i < 4; i++) {
            if (this.page * 4 + i < stationNumber) {
                displayStation(stationPanes.get(i), stationList.get(this.page * 4 + i));
            }
            else {
                stationPanes.get(i).setVisible(false);
                btnNext.setDisable(true);
            }
        }
        if ((this.page + 1) * 4 == stationNumber) {
            btnNext.setDisable(true);
        }
    }

    /**
     * Display station on the station pane
     * @param stationPane: the station pane
     * @param station: the station needed to display
     */
    public void displayStation(Pane stationPane, Station station) {
        ImageView stationImage = (ImageView) stationPane.getChildren().get(0);
        Label stationName = (Label) stationPane.getChildren().get(1);
        Label stationAddress = (Label) stationPane.getChildren().get(2);
        Label bikes = (Label) stationPane.getChildren().get(3);
        Label eBikes = (Label) stationPane.getChildren().get(4);
        Label twinBikes = (Label) stationPane.getChildren().get(5);
        JFXButton buttonView = (JFXButton) stationPane.getChildren().get(6);
        stationName.setText(station.getName());
        stationAddress.setText(station.getAddress());
        bikes.setText(String.valueOf(station.getNumOfBikes()));
        eBikes.setText(String.valueOf(station.getNumOfEBikes()));
        twinBikes.setText(String.valueOf(station.getNumOfTwinBike()));
        stationPane.setVisible(true);
        setImage(stationImage, station.getImage());

        buttonView.setOnMouseClicked(e -> {
            try {
                this.viewStation(station);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        });
    }

    /**
     * Show station detail
     * @param station: the station
     * @throws IOException
     */
    public void viewStation(Station station) throws IOException {
        ViewStationHandler viewStationHandler = new ViewStationHandler(this.stage, Configs.VIEW_STATION_PATH, station);
        viewStationHandler.setPreviousScreen(this);
        viewStationHandler.setScreenTitle("Station Detail");
        viewStationHandler.setHomeHandler(homeHandler);
        viewStationHandler.show();
    }

    /**
     * Hide station panes and display no stations found
     */
    public void notifyNotFoundStations() {
        this.noStations.setVisible(true);
        this.stationOne.setVisible(false);
        this.stationTwo.setVisible(false);
        this.stationThree.setVisible(false);
        this.stationFour.setVisible(false);
        this.btnNext.setVisible(false);
        this.btnPrevious.setVisible(false);
        this.searchingResult.setVisible(false);
    }

    /**
     * Show stations in the next page
     */
    public void showNextPage() {
        this.page += 1;
        this.btnPrevious.setDisable(false);
        displayStations(this.stations);
    }

    /**
     * Show stations in the previous page
     */
    public void showPreviousPage() {
        this.page -= 1;
        displayStations(this.stations);
        this.btnNext.setDisable(false);
        if (this.page == 0) {
            this.btnPrevious.setDisable(true);
        }
    }
}
