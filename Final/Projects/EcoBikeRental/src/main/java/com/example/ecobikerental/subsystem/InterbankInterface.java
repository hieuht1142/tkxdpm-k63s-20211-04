package com.example.ecobikerental.subsystem;

import com.example.ecobikerental.dto.CreditCardDto;
import com.example.ecobikerental.entities.Payment;
import com.example.ecobikerental.exceptions.PaymentException;
import com.example.ecobikerental.exceptions.UnrecognizedException;

public interface InterbankInterface {

	/**
	 * Pay order, and then return the payment transaction
	 *
	 * @param card     - the credit card used for payment
	 * @param amount   - the amount to pay
	 * @param contents - the transaction contents
	 * @return {@link Payment} - if the
	 *         payment is successful
	 * @throws PaymentException      if responded with a pre-defined error code
	 * @throws UnrecognizedException if responded with an unknown error code or
	 *                               something goes wrong
	 */
	public abstract Payment payOrder(CreditCardDto card, int amount, String contents)
			throws PaymentException, UnrecognizedException;

	/**
	 * Refund, and then return the payment transaction
	 *
	 * @param card     - the credit card which would be refunded to
	 * @param amount   - the amount to refund
	 * @param contents - the transaction contents
	 * @return {@link Payment} - if the
	 *         payment is successful
	 * @throws PaymentException      if responded with a pre-defined error code
	 * @throws UnrecognizedException if responded with an unknown error code or
	 *                               something goes wrong
	 */
	public abstract Payment refund(CreditCardDto card, int amount, String contents)
			throws PaymentException, UnrecognizedException;

}
