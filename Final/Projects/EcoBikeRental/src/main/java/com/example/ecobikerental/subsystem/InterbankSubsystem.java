package com.example.ecobikerental.subsystem;


import com.example.ecobikerental.dto.CreditCardDto;
import com.example.ecobikerental.entities.Payment;
import com.example.ecobikerental.subsystem.interbank.InterbankSubsystemController;

/***
 * The {@code InterbankSubsystem} class is used to communicate with the
 * Interbank to make transaction.
 *
 * @author hieud
 *
 */
public class InterbankSubsystem implements InterbankInterface {

	/**
	 * Represent the controller of the subsystem
	 */
	private InterbankSubsystemController ctrl;

	/**
	 * Initializes a newly created {@code InterbankSubsystem} object so that it
	 * represents an Interbank subsystem.
	 */
	public InterbankSubsystem() {
		String str = new String();
		this.ctrl = new InterbankSubsystemController();
	}

	/**
	 * @see InterbankInterface#payOrder(CreditCardDto, int,
	 *      java.lang.String)
	 */
	public Payment payOrder(CreditCardDto card, int amount, String contents) {
		Payment transaction = ctrl.payOrder(card, amount, contents);
		return transaction;
	}

	/**
	 * @see InterbankInterface#refund(CreditCardDto, int,
	 *      java.lang.String)
	 */
	public Payment refund(CreditCardDto card, int amount, String contents) {
		Payment transaction = ctrl.refund(card, amount, contents);
		return transaction;
	}
}
