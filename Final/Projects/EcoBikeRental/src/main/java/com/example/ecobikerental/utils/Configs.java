package com.example.ecobikerental.utils;

public class Configs {
    // Database configuration
    public static final String DB_NAME = "ecobike_rental";
    public static final String DB_USERNAME = "root";
    public static final String DB_PASSWORD = "anhduy789";


    //Group's Card info
    public static final String cardCode = "kscq2_group4_2021";
    public static final String owner = "Group 4";
    public static final String cvvCode = "797";
    public static final String dateExpired = "11/25";

    // api constants
    public static final String PROCESS_TRANSACTION_URL = "https://ecopark-system-api.herokuapp.com/api/card/processTransaction";
    public static final String REGEX_NAME = "^[a-zA-Z ]*$";
    public static final String REGEX_PHONE = "^0\\d{9}$";
    public static final String REGEX_ADDRESS = "[a-zA-Z0-9 \\\\u0080-\\\\u9fff]*+";
    public static int PER_PAGE = 4;

    public static final String BIKE_IMAGES_DIR = "/src/main/resources/com/example/ecobikerental/assets/images/bikes";

    // fxml resource path
    public static final String SPLASH_PATH = "splash.fxml";
    public static final String LOGIN_PATH = "auth/login.fxml";
    public static final String HOME_USER_PATH = "user/home.fxml";
    public static final String SEARCH_BIKE_PATH = "user/search-bike.fxml";
    public static final String BIKE_PATH = "user/bike.fxml";
    public static final String BIKE_DETAIL_PATH = "user/bike-detail.fxml";
    public static final String RENTING_FORM_PATH = "user/renting-form.fxml";
    public static final String DEPOSIT_INVOICE_PATH = "user/deposit-invoice.fxml";
    public static final String PAYMENT_PATH = "user/payment.fxml";

    public static final String SEARCH_STATION_PATH = "user/search-station.fxml";
    public static final String SEARCH_STATION_RESULT_PATH = "user/search-station-result.fxml";
    public static final String VIEW_STATION_PATH = "user/view-station.fxml";

    public static final String VIEW_RENTING_HISTORY_PATH = "user/renting-history.fxml";
    public static final String VIEW_RENTING_STATUS_PATH = "user/view-renting-status.fxml";
    public static final String CHOOSE_STATION_PATH = "user/choose_station.fxml";
    public static final String STATION_PATH = "user/station.fxml";
    public static final String CHOOSE_POSITION_PATH = "user/choose-position.fxml";
    public static final String CHOOSE_BIKE_PATH = "user/choose-bike.fxml";
    public static final String INVOICE_PATH = "user/invoice.fxml";

    public static final String HOME_ADMIN_PATH = "admin/home.fxml";
    public static final String BIKE_MANAGEMENT_PATH = "admin/bike-management.fxml";
    public static final String STATION_MANAGEMENT_PATH = "admin/station-management.fxml";
    public static final String ADD_BIKE_PATH = "admin/add-bike.fxml";
    public static final String ADD_STATION_PATH = "admin/add-station.fxml";
}
