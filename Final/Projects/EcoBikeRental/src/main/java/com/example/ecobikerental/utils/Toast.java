package com.example.ecobikerental.utils;

import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.scene.Scene;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;

import java.util.Objects;

public final class Toast {
    public static void makeText(Stage ownerStage, String toastMsg, String type) {
        Stage toastStage = new Stage();
        toastStage.initOwner(ownerStage);
        toastStage.setResizable(false);
        toastStage.initStyle(StageStyle.TRANSPARENT);

        Text text = new Text(toastMsg);
        text.setFont(Font.font("Verdana", 15));
        text.setFill(Color.WHITE);

        StackPane root = new StackPane(text);

        switch (type) {
            case "error":
                root.setStyle("-fx-background-radius: 5; -fx-background-color: #e11d48; -fx-padding: 10 20; -fx-border-radius: 5;");
                break;
            case "success":
                root.setStyle("-fx-background-radius: 5; -fx-background-color: #047857; -fx-padding: 10 20; -fx-border-radius: 5;");
                break;
            case "warning":
                root.setStyle("-fx-background-radius: 5; -fx-background-color: #FFBD35; -fx-padding: 10 20; -fx-border-radius: 5;");
                break;
            default:
                // Làm gì đó tại đây ...
        }

        root.setLayoutY(500);
        root.setLayoutX(170);
        root.setOpacity(0);

        Scene scene = new Scene(root);
        scene.setFill(Color.TRANSPARENT);
        toastStage.setScene(scene);
        toastStage.show();

        Timeline fadeInTimeline = new Timeline();
        KeyFrame fadeInKey1 = new KeyFrame(Duration.millis(500), new KeyValue(toastStage.getScene().getRoot().opacityProperty(), 1));
        fadeInTimeline.getKeyFrames().add(fadeInKey1);
        fadeInTimeline.setOnFinished((ae) ->
        {
            new Thread(() -> {
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                Timeline fadeOutTimeline = new Timeline();
                KeyFrame fadeOutKey1 = new KeyFrame(Duration.millis(500), new KeyValue(toastStage.getScene().getRoot().opacityProperty(), 0));
                fadeOutTimeline.getKeyFrames().add(fadeOutKey1);
                fadeOutTimeline.setOnFinished((aeb) -> toastStage.close());
                fadeOutTimeline.play();
            }).start();
        });
        fadeInTimeline.play();
    }
}
