package com.example.ecobikerental.controllers;

import com.example.ecobikerental.exceptions.InvalidBikeInfoException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.ValueSource;
import static org.junit.jupiter.api.Assertions.*;

public class ValidateLicensePlateTest {

    private AddBikeController addBikeController;

    @BeforeEach
    public void setUp() {
        addBikeController = new AddBikeController();
    }

    @ParameterizedTest
    @ValueSource(strings = {"37L-20383"})
    public void testValidLicensePlate(String licensePlate) {
        assertTrue(addBikeController.validateLicensePlate(licensePlate));
    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {"37L-2038", "37L-203830", "A7L-20383", "37h-20383", "37L@20383", "37L-20a83"})
    public void testInvalidLicensePlate(String licensePlate) {
        assertThrows(InvalidBikeInfoException.class, () -> {
            addBikeController.validateLicensePlate(licensePlate);
        });
    }
}