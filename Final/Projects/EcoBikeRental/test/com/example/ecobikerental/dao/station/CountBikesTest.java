package com.example.ecobikerental.dao.station;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class CountBikesTest {

    private StationDAO stationDAO;

    @BeforeEach
    public void setUp() {
        stationDAO = new StationDAO();
    }

    @Test
    public void countBikes() {
        assertEquals(10, stationDAO.countBikes(1, 1));
        assertEquals(5, stationDAO.countBikes(1, 3));
        assertEquals(3, stationDAO.countBikes(5, 1));
        assertEquals(1, stationDAO.countBikes(5, 3));
        assertEquals(0, stationDAO.countBikes(0, 1));
        assertEquals(0, stationDAO.countBikes(6, 1));
        assertEquals(0, stationDAO.countBikes(1, 0));
        assertEquals(0, stationDAO.countBikes(1, 4));
    }
}