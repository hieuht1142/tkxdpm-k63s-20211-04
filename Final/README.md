# ĐÁNH GIÁ THÀNH VIÊN #


### Vũ Minh Hiếu: use case "Rent Bike" ###

* Đánh giá: Tham gia họp đầy đủ, chủ động trong công việc, nhiệt tình hỗ trợ các bạn trong nhóm
* Phần trăm đóng góp: 22%

### Nguyễn Lan Anh: use case "Return Bike" ###

* Đánh giá: Làm bài đầy đủ đúng hạn, chủ động
* Phần trăm đóng góp: 21%

### Hoàng Trung Hiếu: use case "Search Station" ###

* Đánh giá: Làm bài đầy đủ, đúng hạn, chủ động chia sẻ, đóng góp
* Phần trăm đóng góp: 18%

### Ngô Văn Giang: usecase "Add Bike" ###

* Đánh giá: Làm bài đầy đủ, đúng hạn, chủ động trao đổi
* Phần trăm đóng góp: 15%

### Trần Xuân Khu: usecase "Add Station" ###

* Đánh giá: Làm bài đầy đủ, đúng hạn, chủ động trao đổi
* Phần trăm đóng góp: 15%

### Nguyễn Khánh Duy: usecase "View Renting History" ###

* Đánh giá: Làm bài đầy đủ, còn chưa đúng hạn, chưa chủ động
* Phần trăm đóng góp: 11%