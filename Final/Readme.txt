Tổng kết: 
Vũ Minh Hiếu: use case "Rent bike"
	- Đánh giá: Tham gia họp đầy đủ, chủ động trong công việc, nhiệt tình hỗ trợ các bạn trong nhóm
	- Phần trăm đóng góp: 22%
Nguyễn Lan Anh: use case "Return bike"
	- Đánh giá: Làm bài đầy đủ đúng hạn, chủ động
	- Phần trăm đóng góp: 21%
Hoàng Trung Hiếu: use case "Search Station"
	- Đánh giá: Làm bài đầy đủ, đúng hạn, chủ động chia sẻ, đóng góp
	- Phần trăm đóng góp: 18%
Ngô Văn Giang:
	- Đánh giá: Làm bài đầy đủ, đúng hạn, chủ động trao đổi
	- Phần trăm đóng góp: 15%
Trần Xuân Khu:
	- Đánh giá: Làm bài đầy đủ, đúng hạn, chủ động trao đổi
	- Phần trăm đóng góp: 15%
Nguyễn Khánh Duy
	- Đánh giá: Làm bài đầy đủ, còn chưa đúng hạn, chưa chủ động
	- Phần trăm đóng góp: 11%