B1: Người dùng sử dụng tài khoản của mình, login thành công vào hệ thống.
B2: Sau khi login thành công, người dùng sẽ được chuyển đến trang chủ home screen.
B3_1: Tại màn home, người dùng có thể nhập mã bike code để thuê xe.
Nếu không tìm thấy xe có mã code mà người dùng nhập, hệ thống sẽ trả về 1 toast message : "No bike has been found!".
Ngược lại, nếu tìm thấy, người dùng sẽ được chuyển đến màn bike detail.
B3_2: Người dùng cũng có thể click vào button "Rent Bike" trên thành side bar.
Khi đó, màn search bike sẽ hiện ra.
Tại đây, ta có thể tìm kiếm xe theo category (phân loại), bike name (tên xe), bike code (mã xe).
Nút clear để xóa các search filter.
Khi click nút View, người dùng cũng sẽ được chuyển đến màn bike detail.
B4: Tại màn bike detail, người dùng có thể xem thông tin chi tiết của xe, có thể quay lại màn hình trước,
hoặc có thể nhấn nút "Rent Bike" để yêu cầu thuê xe.
Nếu xe đã có người khác thuê, trên màn hình sẽ hiện một toast message thông báo xe đã được thuê.
Nếu xe đang ở trạng thái available, màn hình Renting Form sẽ hiện ra để người dùng nhập các thông tin thuê xe.
Trường name và phone đã được nhập sẵn dựa theo thông tin của user đang đăng nhập hệ thống.
Người dùng có thể chọn 1 trong 2 hình thức thuê xe: thuê theo phút hoặc thuê theo ngày.
B5: Khi user nhấn submit, nếu các thông tin được validate thành công, user sẽ được chuyển đến màn "Deposit Invoice"
Tại đây sẽ hiện thị thông tin của hóa đơn tiền đặt cọc.
Nếu người dùng đồng ý, nhấn nút confirm, hệ thống sẽ chuyển người dùng đến màn thanh toán 'Payment form'.
B6: Người dùng nhập các thông tin thẻ tin dùng và nhấn nút Submit. Thông thông tin thẻ hợp lệ, hệ thống sẽ gọi API để thanh toán tiền đặt cọc.
Nếu thanh toán thành công, người dùng sẽ được chuyển về màn "View renting history" kèm theo 1 toast message: "Pay invoice successfully!"