package hoangtrunghieu;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;

public class HelloWorld extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField txtName;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					HelloWorld frame = new HelloWorld();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public HelloWorld() {
		setTitle("Hello World");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 223);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Enter your name:");
		lblNewLabel.setBounds(46, 23, 175, 34);
		contentPane.add(lblNewLabel);
		
		txtName = new JTextField();
		txtName.setBounds(46, 66, 205, 20);
		contentPane.add(txtName);
		txtName.setColumns(10);
		
		JLabel lblHello = new JLabel("");
		lblHello.setForeground(Color.GREEN);
		lblHello.setBounds(46, 97, 317, 42);
		contentPane.add(lblHello);
		
		JButton btnSubmit = new JButton("Submit");
		btnSubmit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String name = txtName.getText();
				
				if (!name.equals("")) {
					lblHello.setText("Hello " + name);
				} else {
					lblHello.setText("");
				}
			}
		});
		btnSubmit.setBounds(274, 65, 89, 23);
		contentPane.add(btnSubmit);
	}
}
