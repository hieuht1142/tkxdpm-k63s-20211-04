package calculation;

import com.sun.org.apache.xml.internal.security.Init;
import com.sun.xml.internal.ws.util.StringUtils;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.stage.Window;

import java.net.URL;
import java.util.Objects;
import java.util.ResourceBundle;

public class Calculation extends Application implements Initializable {
    @FXML
    private Button add;

    @FXML
    private Button sub;

    @FXML
    private Button mul;

    @FXML
    private Button div;

    @FXML
    private TextField a;

    @FXML
    private TextField b;

    @FXML
    private TextField res;

    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(Objects.requireNonNull(getClass().getResource("calculation.fxml")));
        primaryStage.setTitle("Calculation");
        primaryStage.setScene(new Scene(root, 600, 400));
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        add.setOnAction(event -> cal(add));
        sub.setOnAction(event -> cal(sub));
        mul.setOnAction(event -> cal(mul));
        div.setOnAction(event -> cal(div));
    }

    public void alert(Alert alertType, String title, String content, Window owner) {
        alertType.setTitle(title);
        alertType.setContentText(content);
        alertType.initOwner(owner);
        alertType.setHeaderText(null);
        alertType.show();
    }

    public void cal(Button button) {
        if (a.getText().isEmpty() || b.getText().isEmpty()) {
            alert(new Alert(Alert.AlertType.ERROR), "Calculation",
                    "Didn't enter enough two numbers", button.getScene().getWindow());
        } else {
            if (isNumeric(a.getText()) && isNumeric(b.getText())) {
                Double x = Double.parseDouble(a.getText());
                Double y = Double.parseDouble(b.getText());
                Double result;
                String btn_name = button.getId();
                switch (btn_name) {
                    case "add": result = x + y; break;
                    case "sub": result = x - y; break;
                    case "mul": result = x * y; break;
                    case "div": result = x / y; break;
                    default: result = 0d;
                }
                res.setText(String.valueOf(result));
            } else {
                alert(new Alert(Alert.AlertType.ERROR), "Error!",
                        "Please enter text is numeric!", button.getScene().getWindow());
            }
        }
    }

    public boolean isNumeric(String strNum) {
        try {
            Double d = Double.parseDouble(strNum);
        } catch (NumberFormatException e) {
            return false;
        }
        return true;
    }
}
