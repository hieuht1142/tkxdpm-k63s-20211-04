package helloworld;

import javafx.application.Application;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.stage.Window;

import java.util.Objects;

public class HelloWorld extends Application {
    @FXML
    private Label lb_name;

    @FXML
    private TextField txt_name;

    @FXML
    private Button btn_submit;

    @FXML
    public void submit() {
        String name = txt_name.getText();
        Window owner = btn_submit.getScene().getWindow();
        Alert alert;
        if (name.isEmpty()) {
            alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Hello World!");
            alert.setHeaderText(null);
            alert.setContentText("Please enter your name");
        } else {
            alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Hello World!");
            alert.setHeaderText(null);
            alert.setContentText("Hello " + name);
        }
        alert.initOwner(owner);
        alert.show();
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(Objects.requireNonNull(getClass().getResource("hello.fxml")));
        primaryStage.setTitle("Hello World!");
        primaryStage.setScene(new Scene(root, 600, 400));
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}