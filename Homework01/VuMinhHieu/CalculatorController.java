package com.company;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class CalculatorController {

    private CalculatorView calculatorView;
    private CalculatorModel calculatorModel;

    public CalculatorController(CalculatorView calculatorView, CalculatorModel calculatorModel) {
        this.calculatorView = calculatorView;
        this.calculatorModel = calculatorModel;

        // Tell the View that whenever the calculate button
        // is clicked to execute the actionPerformed method
        // in the CalculateListener inner class
        this.calculatorView.addCalculateListener(new CalculateListener());
    }

    class CalculateListener implements ActionListener{

        public void actionPerformed(ActionEvent e) {
            int firstNumber, secondNumber = 0;

            try {
                firstNumber = calculatorView.getFirstNumber();
                secondNumber = calculatorView.getSecondNumber();

                calculatorModel.addTwoNumbers(firstNumber, secondNumber);
                calculatorView.setCalcSolution(calculatorModel.getCalculationValue());
            } catch (NumberFormatException ex) {
                System.out.println(ex);
                calculatorView.displayErrorMessage("You Need to Enter 2 Integers");
            }
        }
    }
}