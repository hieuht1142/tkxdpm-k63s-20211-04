﻿CREATE TABLE users (
  id int NOT NULL AUTO_INCREMENT,
  username varchar(50) NOT NULL,
  name varchar(50),
  phone_number varchar(10),
  status int, 
  role varchar(20),
  password varchar(20),
  created_date datetime,
  created_by varchar(50),
  modified_date datetime,
  modified_by varchar(50),
  CONSTRAINT pk_users PRIMARY KEY (id)
);

CREATE TABLE stations (
  id int NOT NULL AUTO_INCREMENT,
  name varchar(100) NOT NULL,
  total_positions int,
  status int,
  created_date datetime,
  created_by varchar(50),
  modified_date datetime,
  modified_by varchar(50),
  CONSTRAINT pk_stations PRIMARY KEY (id)
);

CREATE TABLE categories (
  id int NOT NULL AUTO_INCREMENT,
  name varchar(100) NOT NULL,
  deposit long,
  price_rate float,
  status int,
  created_date datetime,
  created_by varchar(50),
  modified_date datetime,
  modified_by varchar(50),
  CONSTRAINT pk_categories PRIMARY KEY (id)
);

CREATE TABLE bikes (
  id int NOT NULL AUTO_INCREMENT,
  name varchar(100) NOT NULL,
  bike_code varchar(20),
  weight float,
  licence_plate varchar(100),
  manufacturing_date date,
  producer varchar(100),
  price long,
  category_id int,
  station_id int,
  `position` int,
  state int,
  battery_percentage int,
  load_cycles int,
  time_remaining int,
  status int,
  created_date datetime,
  created_by varchar(50),
  modified_date datetime,
  modified_by varchar(50),
  CONSTRAINT pk_bikes PRIMARY KEY (id),
  CONSTRAINT fk_bike_category FOREIGN KEY (category_id) REFERENCES categories(id),
  CONSTRAINT fk_bike_station FOREIGN KEY (station_id) REFERENCES stations(id)
);

CREATE TABLE rentals (
  id int NOT NULL AUTO_INCREMENT,
  renting_type int,
  start_time datetime,
  end_time datetime,
  renting_fee long,
  bike_id int,
  user_id int,
  state int,
  status int,
  created_date datetime,
  created_by varchar(50),
  modified_date datetime,
  modified_by varchar(50),
  CONSTRAINT pk_rentals PRIMARY KEY (id),
  CONSTRAINT fk_rental_bike FOREIGN KEY (bike_id) REFERENCES bikes(id),
  CONSTRAINT fk_rental_user FOREIGN KEY (user_id) REFERENCES users(id)
);

CREATE TABLE credit_cards (
  id int NOT NULL AUTO_INCREMENT,
  number varchar(20) NOT NULL,
  holder_name varchar(50),
  security_code varchar(20),
  expiration_month int,
  expiration_year int,
  status int,
  created_date datetime,
  created_by varchar(50),
  modified_date datetime,
  modified_by varchar(50),
  CONSTRAINT pk_cards PRIMARY KEY (id)
);

CREATE TABLE invoices (
  id int NOT NULL AUTO_INCREMENT,
  invoice_type int,
  total_amount long,
  rental_id int,
  status int,
  created_date datetime,
  created_by varchar(50),
  modified_date datetime,
  modified_by varchar(50),
  CONSTRAINT pk_invoices PRIMARY KEY (id),
  CONSTRAINT fk_invoice_rental FOREIGN KEY (rental_id) REFERENCES rentals(id)
);

CREATE TABLE payments (
  id int NOT NULL AUTO_INCREMENT,
  content varchar(200),
  errorCode int,
  credit_card_id int,
  invoice_id int,
  status int,
  created_date datetime,
  created_by varchar(50),
  modified_date datetime,
  modified_by varchar(50),
  CONSTRAINT pk_payments PRIMARY KEY (id),
  CONSTRAINT fk_payment_card FOREIGN KEY (credit_card_id) REFERENCES credit_cards(id),
  CONSTRAINT fk_payment_invoice FOREIGN KEY (invoice_id) REFERENCES invoices(id)
)