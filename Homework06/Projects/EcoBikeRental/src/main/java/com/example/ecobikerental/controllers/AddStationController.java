package com.example.ecobikerental.controllers;

import com.example.ecobikerental.dao.station.IStationDAO;
import com.example.ecobikerental.dao.station.StationDAO;
import com.example.ecobikerental.entities.Station;
import com.example.ecobikerental.exceptions.InvalidStationInfoException;

import java.sql.Timestamp;
import java.util.Hashtable;

public class AddStationController extends BaseController {

    private final IStationDAO stationDAO;

    public AddStationController() {
        stationDAO = new StationDAO();
    }

    public void addStation(Hashtable<String, String> stationInfo) {
        validateStationInfo(stationInfo);
        checkNameExist(stationInfo.get("name"));

        Station station = new Station();
        station.setName(stationInfo.get("name"));
        station.setAddress(stationInfo.get("address"));
        station.setTotalPositions(30);
        station.setImage(stationInfo.get("image"));
        station.setStatus(1);
        Timestamp createdDate = new Timestamp(System.currentTimeMillis());
        station.setCreatedDate(createdDate);
        stationDAO.createStation(station);
    }

    public void validateStationInfo(Hashtable<String, String> stationInfo) {
        if (!validateStationName(stationInfo.get("name")))
            throw new InvalidStationInfoException("Invalid Station Name!");
        if (!validateStationAddress(stationInfo.get("address")))
            throw new InvalidStationInfoException("Invalid Station Address!");
        if (!validateStationTotalPositions(stationInfo.get("total_positions")))
            throw new InvalidStationInfoException("Invalid Station Total Positions!");
    }

    public boolean validateStationTotalPositions(String totalPositions) {
        if (totalPositions == null) return false;
        try {
            Integer.parseInt(totalPositions);
        } catch (NumberFormatException e) {
            return false;
        }
        return true;
    }

    public boolean validateStationName(String name) {
        if (name == null || name.equals("")) return false;
        String normalName = convert(name);
        for (int i = 0; i < normalName.length(); i++) {
            char c = normalName.charAt(i);
            if (!((c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z') || c == ' '))
                return false;
        }
        return true;
    }

    public boolean validateStationAddress(String address) {
        if (address == null || address.equals("")) return false;
        String normalAddress = convert(address);
        for (int i = 0; i < normalAddress.length(); i++) {
            char c = normalAddress.charAt(i);
            if (!((c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z') || (c >= '0' && c <= '9')
                    || c == ' ' || c == ','))
                return false;
        }
        return true;
    }

    public void checkNameExist(String name) {
        if (stationDAO.findByName(name) != null)
            throw new InvalidStationInfoException("Station Name Not Available!");
    }

    public static String convert(String str) {
        str = str.replaceAll("à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ", "a");
        str = str.replaceAll("è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ", "e");
        str = str.replaceAll("ì|í|ị|ỉ|ĩ", "i");
        str = str.replaceAll("ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ", "o");
        str = str.replaceAll("ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ", "u");
        str = str.replaceAll("ỳ|ý|ỵ|ỷ|ỹ", "y");
        str = str.replaceAll("đ", "d");

        str = str.replaceAll("À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ", "A");
        str = str.replaceAll("È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ", "E");
        str = str.replaceAll("Ì|Í|Ị|Ỉ|Ĩ", "I");
        str = str.replaceAll("Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ", "O");
        str = str.replaceAll("Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ", "U");
        str = str.replaceAll("Ỳ|Ý|Ỵ|Ỷ|Ỹ", "Y");
        str = str.replaceAll("Đ", "D");
        return str;
    }
}
