package com.example.ecobikerental.controllers;

import com.example.ecobikerental.dao.bike.BikeDAO;
import com.example.ecobikerental.dao.station.StationDAO;
import com.example.ecobikerental.enums.bike.State;

public class AdminHomeController extends BaseController {

    private BikeDAO bikeDAO;

    private StationDAO stationDAO;

    public AdminHomeController() {
        bikeDAO = new BikeDAO();
        stationDAO = new StationDAO();
    }

    public AdminHomeController(BikeDAO bikeDAO, StationDAO stationDAO) {
        this.bikeDAO = bikeDAO;
        this.stationDAO = stationDAO;
    }

    public int getTotalBikes() {
        return bikeDAO.countAll();
    }

    public int getTotalStations() {
        return stationDAO.countAll();
    }

    public int getTotalPositions() {
        return stationDAO.countAllPositions();
    }

    public int getTotalAvaiableBikes() {
        return bikeDAO.countByState(State.AVAILABLE);
    }

    public int getTotalRentedBikes() {
        return bikeDAO.countByState(State.RENTED);
    }
}
