package com.example.ecobikerental.controllers;

import com.example.ecobikerental.dao.user.IUserDAO;
import com.example.ecobikerental.exceptions.InvalidCredentialsException;
import com.example.ecobikerental.dao.user.UserDAO;
import com.example.ecobikerental.entities.User;
import com.example.ecobikerental.enums.entity.Status;
import com.example.ecobikerental.utils.AuthPreference;

import java.util.List;

public class AuthController extends BaseController {
    private IUserDAO userDAO;
    private AuthPreference prefs;

    public AuthController() {
        userDAO = new UserDAO();
        prefs = new AuthPreference();
    }

    public User login(String username, String password) {
        List<User> users = userDAO.find(username, password, Status.ACTIVE);

        if (users.size() < 1) {
            throw new InvalidCredentialsException("Invalid Credentials!");
        }

        User user = users.get(0);
        prefs.setAuthUser(user.getId());

        return user;
    }

    public void logout() {
        prefs.removeAuthUser();
    }

    public User getAuthUser() {
        int id = prefs.getAuthUser();

        return userDAO.find(id);
    }
}
