package com.example.ecobikerental.controllers;

import com.example.ecobikerental.dao.card.CreditCardDAO;
import com.example.ecobikerental.dao.card.ICreditCardDAO;
import com.example.ecobikerental.dao.invoice.IInvoiceDAO;
import com.example.ecobikerental.dao.invoice.InvoiceDAO;
import com.example.ecobikerental.dao.payment.IPaymentDAO;
import com.example.ecobikerental.dao.payment.PaymentDAO;
import com.example.ecobikerental.dto.CreditCardDto;
import com.example.ecobikerental.entities.paymentcard.CreditCard;
import com.example.ecobikerental.entities.Payment;
import com.example.ecobikerental.entities.User;
import com.example.ecobikerental.exceptions.InvalidCardException;
import com.example.ecobikerental.exceptions.InvalidPaymentInfoException;
import com.example.ecobikerental.exceptions.PaymentException;
import com.example.ecobikerental.exceptions.UnrecognizedException;
import com.example.ecobikerental.subsystem.InterbankInterface;
import com.example.ecobikerental.subsystem.InterbankSubsystem;
import com.example.ecobikerental.utils.Configs;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PaymentController extends BaseController{
    /**
     * Represent the card used for payment
     */
    private CreditCardDto card;

    /**
     * Represent the Interbank subsystem
     */
    private InterbankInterface interbank;
    private IPaymentDAO paymentDAO;
    private IInvoiceDAO invoiceDAO;
    private ICreditCardDAO creditCardDAO;

    public PaymentController(){
        paymentDAO = new PaymentDAO();
        invoiceDAO = new InvoiceDAO();
        creditCardDAO = new CreditCardDAO();
    }

    /**
     * Validate the input date which should be in the format "mm/yy", and then
     * return a {@link java.lang.String String} representing the date in the
     * required format "mmyy" .
     *
     * @param date - the {@link java.lang.String String} represents the input date
     * @return {@link java.lang.String String} - date representation of the required
     *         format
     * @throws InvalidCardException - if the string does not represent a valid date
     *                              in the expected format
     */
    private String getExpirationDate(String date) throws InvalidCardException {
        String[] strs = date.split("/");
        if (strs.length != 2) {
            throw new InvalidCardException();
        }

        String expirationDate = null;
        int month = -1;
        int year = -1;

        try {
            month = Integer.parseInt(strs[0]);
            year = Integer.parseInt(strs[1]);
            if (month < 1 || month > 12 || year < Calendar.getInstance().get(Calendar.YEAR) % 100 || year > 100) {
                throw new InvalidCardException();
            }
            expirationDate = strs[0] + strs[1];

        } catch (Exception ex) {
            throw new InvalidCardException();
        }

        return expirationDate;
    }

    /**
     * Pay order, and then return the result with a message.
     *
     * @param amount         - the amount to pay
     * @param contents       - the transaction contents
     * @param cardNumber     - the card number
     * @param cardHolderName - the card holder name
     * @param expirationDate - the expiration date in the format "mm/yy"
     * @param securityCode   - the cvv/cvc code of the credit card
     * @return {@link java.util.Map Map} represent the payment result with a
     *         message.
     */
    public Map<String, Object> payOrder(int amount, String contents, String cardNumber, String cardHolderName,
                                        String expirationDate, String securityCode) {
        Map<String, Object> result = new Hashtable<>();
        result.put("RESULT", "Failed");
        try {
            this.card = createCreditCard(cardNumber, cardHolderName, expirationDate, securityCode);

            this.interbank = new InterbankSubsystem();
            try {
                Payment transaction = interbank.payOrder(card, amount, contents);
                result.put("RESULT", "Success");
                result.put("MESSAGE", "Pay invoice successfully!");
                if (transaction != null) {
                    result.put("Payment", transaction);
                }
            } catch (PaymentException ep){
                result.put("RESULT", "Failed");
                result.put("MESSAGE", ep.getMessage());
            }
        } catch (PaymentException | UnrecognizedException ex) {
            result.put("MESSAGE", ex.getMessage());
        }
        return result;
    }

    public Map<String, Object> refund(int amount, String contents, String cardNumber, String cardHolderName,
                                      String expirationDate, String securityCode) {
        Map<String, Object> result = new Hashtable<>();
        result.put("RESULT", "Failed");
        try {
            this.card = createCreditCard(cardNumber, cardHolderName, expirationDate, securityCode);

            this.interbank = new InterbankSubsystem();
            try {
                Payment transaction = interbank.refund(card, amount, contents);
                if(transaction != null){
                    result.put("Payment", transaction);
                }
                result.put("RESULT", "Success");
                result.put("MESSAGE", "You have made a successful transaction!");
            } catch (PaymentException ep){
                result.put("RESULT", "Failed");
                result.put("MESSAGE", ep.getMessage());
            }
        } catch (PaymentException | UnrecognizedException ex) {
            result.put("MESSAGE", ex.getMessage());
        }
        return result;
    }

    public boolean validatePaymentInfo(HashMap<String, String> inputs){
        String number = inputs.get("number");
        String date = inputs.get("date");
        String holder = inputs.get("holder");
        String security = inputs.get("security");

        if(number.equals("") || date.equals("") || holder.equals("") || security.equals("")){
            throw new InvalidPaymentInfoException("Please fill in all required fields!");
        } else {
            try{
                String expiration = getExpirationDate(date);
            } catch (InvalidCardException ex){
                throw new InvalidPaymentInfoException("Invalid expiration date!");
            }
        }
        return true;
    }

    /**
     * Kiem tra so dien thoai co hop le hay khong
     * @param number: so dien thoai
     * @return true (hop le)/ false (khong hop le)
     */
    public boolean validateNumber(String number) {
        // TODO: your work
        if(number == null) {
            return false;
        }
        try {
            Long.parseLong(number);
        } catch (NumberFormatException e) {
            // TODO: handle exception
            return false;
        }
        return true;
    }

    /**
     * Kiem tra ten co hop le hay khong
     * @param name: ten
     * @return true (hop le)/ false (khong hop le)
     */
    public boolean validateName(String name) {
        // TODO: your work
        if(name == null || name.length() == 0)
            return false;
        Pattern p = Pattern.compile(Configs.REGEX_NAME);
        Matcher m = p.matcher(name);
        if(m.matches()) {
            return true;
        } else {
            return false;
        }
    }

    public void savePayment(Payment payment) {
        int cardId = saveCreditCard((CreditCard) payment.getPaymentCard());
        int invoiceId = invoiceDAO.create(payment.getInvoice());
        payment.getInvoice().setId(invoiceId);
        payment.getPaymentCard().setId(cardId);
        paymentDAO.create(payment);
    }

    public CreditCard getUserCard() {
        AuthController authController = new AuthController();
        User authUser = authController.getAuthUser();

        return creditCardDAO.find(authUser.getId());
    }

    public CreditCardDto createCreditCard(String cardNumber, String cardHolderName, String expirationDate, String securityCode) {
        CreditCard card = getCreditCard(cardNumber);

        if (card == null) {
            return new CreditCardDto(cardNumber, cardHolderName, securityCode, getExpirationDate(expirationDate));
        }

        return new CreditCardDto(card);
    }

    private CreditCard getCreditCard(String cardNumber) {
        return creditCardDAO.find(cardNumber);
    }

    private int saveCreditCard(CreditCard card) {
        CreditCard creditCard = getCreditCard(card.getCardCode());

        if (creditCard == null) {
            AuthController authController = new AuthController();
            User authUser = authController.getAuthUser();
            card.setUser(authUser);

            return creditCardDAO.create(card);
        }

        return creditCard.getId();
    }
}
