package com.example.ecobikerental.controllers;

import com.example.ecobikerental.controllers.rentingtype.ByDay;
import com.example.ecobikerental.controllers.rentingtype.ByMinute;
import com.example.ecobikerental.controllers.rentingtype.RentingTypeFactory;
import com.example.ecobikerental.dao.rental.RentalDAO;
import com.example.ecobikerental.entities.Rental;
import com.example.ecobikerental.entities.User;
import com.example.ecobikerental.enums.rental.RentingType;

import java.util.ArrayList;
import java.util.List;

public class RentalController extends BaseController{

    public static RentalController instance = new RentalController();

    public static RentalController getInstance(){
        return  instance;
    }

    private RentalDAO rentalDAO;

    public RentalController(){
        rentalDAO = new RentalDAO();
        RentingTypeFactory.getInstance().registerRentingType(RentingType.BY_DAY, new ByDay());
        RentingTypeFactory.getInstance().registerRentingType(RentingType.BY_MINUTES, new ByMinute());
    }

    public List<Rental> getCurrentUserRental(){
        AuthController authController = new AuthController();
        User currentUser = authController.getAuthUser();
        List<Rental> list = new ArrayList<>();
        return rentalDAO.findByUserId(currentUser.getId());
    }

    public long calculateRentingFee(Rental rental){
        if(rental.getRentingType() == RentingType.BY_MINUTES){
            return RentingTypeFactory.getInstance().calculateRentingFee(RentingType.BY_MINUTES ,rental);
        } else if(rental.getRentingType() == RentingType.BY_DAY){
            return RentingTypeFactory.getInstance().calculateRentingFee(RentingType.BY_DAY ,rental);
        }
        return 0;
    }
}
