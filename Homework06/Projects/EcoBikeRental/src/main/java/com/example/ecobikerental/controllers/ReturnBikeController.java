package com.example.ecobikerental.controllers;

import com.example.ecobikerental.controllers.rentingtype.ByDay;
import com.example.ecobikerental.controllers.rentingtype.ByMinute;
import com.example.ecobikerental.controllers.rentingtype.RentingTypeFactory;
import com.example.ecobikerental.dao.bike.BikeDAO;
import com.example.ecobikerental.dao.bike.IBikeDAO;
import com.example.ecobikerental.dao.rental.IRentalDAO;
import com.example.ecobikerental.dao.rental.RentalDAO;
import com.example.ecobikerental.dao.station.IStationDAO;
import com.example.ecobikerental.dao.station.StationDAO;
import com.example.ecobikerental.entities.Bike;
import com.example.ecobikerental.entities.Invoice;
import com.example.ecobikerental.entities.Paginator;
import com.example.ecobikerental.entities.Rental;
import com.example.ecobikerental.enums.rental.RentingType;

import java.util.List;

public class ReturnBikeController extends BaseController {
    private IStationDAO stationDAO;
    private IBikeDAO bikeDAO;
    private IRentalDAO rentalDAO;

    private static ReturnBikeController instance = new ReturnBikeController();

    public static ReturnBikeController getInstance(){
        return instance;
    }

    public ReturnBikeController(){
        stationDAO = new StationDAO();
        bikeDAO = new BikeDAO();
        rentalDAO = new RentalDAO();

        //register renting type
        RentingTypeFactory.getInstance().registerRentingType(RentingType.BY_DAY, new ByDay());
        RentingTypeFactory.getInstance().registerRentingType(RentingType.BY_MINUTES, new ByMinute());
    }

    public Paginator getStations(int skip, int maxResult){
        return stationDAO.findAll(skip, maxResult);
    }

    public Paginator getStations(String name, int skip, int maxResult){
        return stationDAO.find(name, skip, maxResult);
    }

    public List<Bike> getBikes(int id){
        return bikeDAO.getBikesInStation(id);
    }

    public long calculateRentingFee(Rental rental){
        if(rental.getRentingType() == RentingType.BY_MINUTES){
            return RentingTypeFactory.getInstance().calculateRentingFee(RentingType.BY_MINUTES ,rental);
        } else if(rental.getRentingType() == RentingType.BY_DAY){
            return RentingTypeFactory.getInstance().calculateRentingFee(RentingType.BY_DAY ,rental);
        }
        return 0;
    }

    public Rental getRental(int id){
        return rentalDAO.find(id);
    }

    public void paymentSuccess(Invoice invoice) {
        Rental rental = invoice.getRental();
        rentalDAO.update(rental);

        bikeDAO.updateToStation(rental.getBike());
    }
}
