package com.example.ecobikerental.controllers;

import com.example.ecobikerental.dao.station.IStationDAO;
import com.example.ecobikerental.dao.station.StationDAO;
import com.example.ecobikerental.entities.Station;

import java.util.List;

public class StationManagementController extends BaseController {

    public final IStationDAO stationDAO;

    public StationManagementController() {
        this.stationDAO = new StationDAO();
    }

    public int countTotalPages(int pageSize) {
        int numberOfStation = stationDAO.countAll();
        return numberOfStation % pageSize == 0 ? numberOfStation/pageSize : numberOfStation/pageSize+1;
    }

    public List<Station> findStationPage(int offset, int pageSize) {
        return stationDAO.findByDetails(offset, pageSize);
    }
}
