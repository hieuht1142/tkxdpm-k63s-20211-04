package com.example.ecobikerental.dao;

import com.example.ecobikerental.dao.database.MySQLConnection;
import com.example.ecobikerental.dao.mapper.IBaseMapper;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class BaseDAO<T> implements IBaseDAO<T> {
    private Connection conn;

    @Override
    public Connection getConnection() {
        return MySQLConnection.getInstance().setUpConnection();
    }

    @Override
    public void closeConnection() {
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public List<T> query(String sql, IBaseMapper<T> mapper, Object... params) {
        List<T> results = new ArrayList<>();
        PreparedStatement statement = null;
        ResultSet rs = null;
        try {
            conn = getConnection();
            statement = conn.prepareStatement(sql);
            setParameters(statement, params);
            rs = statement.executeQuery();
            while (rs.next()) {
                results.add(mapper.mapRow(rs));
            }
            return results;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            try {
                closeConnection();
                if (statement != null) {
                    statement.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e1) {
                e1.printStackTrace();
                return null;
            }
        }
    }

    @Override
    public long insert(String sql, Object... params) {
        ResultSet resultSet = null;
        long id = 0;
        PreparedStatement statement = null;
        try {
            conn = getConnection();
            conn.setAutoCommit(false);
            statement = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            setParameters(statement, params);
            statement.executeUpdate();
            resultSet = statement.getGeneratedKeys();
            if (resultSet.next()) {
                id = resultSet.getLong(1);
            }
            conn.commit();
            return id;
        } catch (SQLException e) {
            e.printStackTrace();
            if (conn != null) {
                try {
                    conn.rollback();
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
            }
            return id;
        } finally {
            try {
                closeConnection();
                if (statement != null) {
                    statement.close();
                }
                if (resultSet != null) {
                    resultSet.close();
                }
            } catch (SQLException e2) {
                e2.printStackTrace();
                return 0;
            }
        }
    }

    @Override
    public Long insert(String sql, Parameter... params) {
        ResultSet resultSet = null;
        Long id = null;
        PreparedStatement statement = null;
        try {
            conn = getConnection();
            conn.setAutoCommit(false);
            statement = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            setParameters(statement, params);
            statement.executeUpdate();
            resultSet = statement.getGeneratedKeys();
            if (resultSet.next()) {
                id = resultSet.getLong(1);
            }
            conn.commit();
            return id;
        } catch (SQLException e) {
            e.printStackTrace();
            if (conn != null) {
                try {
                    conn.rollback();
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
            }
            return id;
        } finally {
            try {
                closeConnection();
                if (statement != null) {
                    statement.close();
                }
                if (resultSet != null) {
                    resultSet.close();
                }
            } catch (SQLException e2) {
                e2.printStackTrace();
                return null;
            }
        }
    }

    @Override
    public void updateOrDelete(String sql, Object... params) {
        PreparedStatement statement = null;
        try {
            conn = getConnection();
            conn.setAutoCommit(false);
            statement = conn.prepareStatement(sql);
            setParameters(statement, params);
            statement.executeUpdate();
            conn.commit();
        } catch (SQLException e) {
            e.printStackTrace();
            if (conn != null) {
                try {
                    conn.rollback();
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
            }
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                }
                if (statement != null) {
                    statement.close();
                }
            } catch (SQLException e2) {
                e2.printStackTrace();
            }
        }
    }

    private void setParameters(PreparedStatement statement, Object... params) {
        try {
            for (int i = 0; i < params.length; i++) {
                Object param = params[i];
                if (param instanceof Long) {
                    statement.setLong(i + 1, (Long) param);
                } else if (param instanceof String) {
                    statement.setString(i + 1, (String) param);
                } else if (param instanceof Integer) {
                    statement.setInt(i + 1, (Integer) param);
                } else if (param instanceof Timestamp) {
                    statement.setTimestamp(i + 1, (Timestamp) param);
                } else if (param instanceof Date) {
                    statement.setDate(i + 1, (Date) param);
                } else if (param instanceof Float) {
                    statement.setFloat(i + 1, (Float) param);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    
    private void setParameters(PreparedStatement statement, Parameter... params) {
        try {
            for (int i = 0; i < params.length; i++) {
                Parameter param = params[i];
                if (param.getValue() == null) {
                    statement.setNull(i + 1, param.getSqlType());
                } else {
                    Object value = param.getValue();
                    if (value instanceof Long) {
                        statement.setLong(i + 1, (Long) value);
                    } else if (value instanceof String) {
                        statement.setString(i + 1, (String) value);
                    } else if (value instanceof Integer) {
                        statement.setInt(i + 1, (Integer) value);
                    }  else if (value instanceof Date) {
                        statement.setDate(i + 1, (Date) value);
                    } else if (value instanceof Timestamp) {
                        statement.setTimestamp(i + 1, (Timestamp) value);
                    } else if (value instanceof Float) {
                        statement.setFloat(i + 1, (Float) value);
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int count(String sql, Object... params) {
        PreparedStatement statement = null;
        ResultSet rs = null;
        int cnt = 0;
        try {
            conn = getConnection();
            statement = conn.prepareStatement(sql);
            setParameters(statement, params);
            rs = statement.executeQuery();
            while (rs.next()) {
                cnt = rs.getInt(1);
            }
            return cnt;
        } catch (SQLException e) {
            e.printStackTrace();
            return 0;
        } finally {
            try {
                closeConnection();
                if (statement != null) {
                    statement.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e1) {
                e1.printStackTrace();
                return 0;
            }
        }
    }

    @Override
    public int queryCount(String sql, String name, Object... params) {
        PreparedStatement statement = null;
        ResultSet rs = null;
        try {
            conn = getConnection();
            statement = conn.prepareStatement(sql);
            setParameters(statement, params);
            rs = statement.executeQuery();
            while (rs.next()) {
                return rs.getInt(name);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                closeConnection();
                if (statement != null) {
                    statement.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        }
        return 0;
    }
}
