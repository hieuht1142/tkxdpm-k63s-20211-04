package com.example.ecobikerental.dao;

import com.example.ecobikerental.dao.mapper.IBaseMapper;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.List;

public interface IBaseDAO<T> {
    public Connection getConnection();
    public void closeConnection();
    public List<T> query(String sql, IBaseMapper<T> mapper, Object... params);
    public long insert(String sql, Object... params);
    public Long insert(String sql, Parameter... params);
    public void updateOrDelete(String sql, Object... params);
    public int queryCount(String sql, String name, Object... params);

    /**
     * Execute count query
     *
     * @param sql:    the count query
     * @param params: the parameters passed in the query
     * @return the result of the count query
     */
    public int count(String sql, Object... params);
}
