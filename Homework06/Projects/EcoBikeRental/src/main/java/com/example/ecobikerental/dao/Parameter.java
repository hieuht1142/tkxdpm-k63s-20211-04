package com.example.ecobikerental.dao;

public class Parameter {

    private Object value;

    private Integer sqlType;

    public Parameter(Object value, Integer sqlType) {
        this.value = value;
        this.sqlType = sqlType;
    }

    public Object getValue() {
        return value;
    }

    public Integer getSqlType() {
        return sqlType;
    }
}
