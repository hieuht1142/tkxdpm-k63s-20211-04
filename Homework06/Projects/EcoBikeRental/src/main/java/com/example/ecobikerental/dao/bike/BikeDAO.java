package com.example.ecobikerental.dao.bike;

import com.example.ecobikerental.dao.BaseDAO;
import com.example.ecobikerental.dao.IBaseDAO;
import com.example.ecobikerental.dao.Parameter;
import com.example.ecobikerental.dao.mapper.BikeMapper;
import com.example.ecobikerental.entities.Bike;
import com.example.ecobikerental.enums.bike.State;
import com.example.ecobikerental.entities.Paginator;

import java.sql.Types;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

public class BikeDAO extends BaseDAO<Bike> implements IBikeDAO {

    @Override
    public Long save(Bike bike) {
        final String command = "INSERT INTO bikes (name, bike_code, image, weight, licence_plate, manufacturing_date, producer, price, " +
                "position, state, battery_percentage, load_cycles, time_remaining, category_id, station_id) " +
                "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

        java.sql.Date sqlManufacturingDate = null;
        if (bike.getManufacturingDate() != null) {
            sqlManufacturingDate = new java.sql.Date(bike.getManufacturingDate().getTime());
        }
        return insert(command,
                new Parameter(bike.getName(), Types.NVARCHAR),
                new Parameter(bike.getBikeCode(), Types.NVARCHAR),
                new Parameter(bike.getImage(), Types.NVARCHAR),
                new Parameter(bike.getWeight(), Types.FLOAT),
                new Parameter(bike.getLicencePlate(), Types.VARCHAR),
                new Parameter(sqlManufacturingDate, Types.DATE),
                new Parameter(bike.getProducer(), Types.NVARCHAR),
                new Parameter(bike.getPrice(), Types.BIGINT),
                new Parameter(bike.getPosition(), Types.INTEGER),
                new Parameter(bike.getState(), Types.INTEGER),
                new Parameter(bike.getBatteryPercentage(), Types.INTEGER),
                new Parameter(bike.getLoadCycles(), Types.INTEGER),
                new Parameter(bike.getTimeRemaining(), Types.INTEGER),
                new Parameter(bike.getCategory().getId(), Types.INTEGER),
                new Parameter(bike.getStation().getId(), Types.INTEGER)
        );
    }

    @Override
    public int countAll() {
        String command = "SELECT COUNT(*) FROM bikes";
        return count(command);
    }

    public int countByState(Integer bikeState) {
        String command = "SELECT COUNT(*) FROM bikes WHERE state = ?";
        return count(command, bikeState);
    }

    public int checkExistLicencePlate(String licencePlate) {
        String command = "SELECT COUNT(*) FROM bikes WHERE licence_plate = ?";
        return count(command, licencePlate);
    }

    @Override
    public int checkUsedPosition(Integer stationId, Integer position) {
        String command = "SELECT COUNT(*) FROM bikes WHERE station_id = ? AND position = ?";
        return count(command, stationId, position);
    }

    @Override
    public List<Bike> findAll() {
        String sqlCommand = "SELECT * FROM bikes";

        return super.query(sqlCommand, new BikeMapper());
    }

    @Override
    public Paginator findAll(int skip, int perPage) {
        String sqlCommand = "SELECT * FROM bikes LIMIT ?, ?";
        List<Bike> bikes = super.query(sqlCommand, new BikeMapper(), skip, perPage);

        Paginator paginator = new Paginator();
        String sql = "SELECT count(b.id) as total FROM bikes b";
        paginator.setTotal(super.queryCount(sql, "total"));
        paginator.setData(bikes);

        return paginator;
    }

    @Override
    public List<Bike> getBikesInStation(int id) {
        String sql = "SELECT * FROM bikes b WHERE b.station_id = ? AND state = ?";
        return super.query(sql, new BikeMapper(), id, State.AVAILABLE);
    }

    @Override
    public Bike find(int id) {
        String sqlCommand = "SELECT * FROM bikes WHERE id = ?";
        List<Bike> bikes = super.query(sqlCommand, new BikeMapper(), id);

        return bikes.size() > 0 ? bikes.get(0) : null;
    }

    @Override
    public void updateToStation(Bike bike) {
        String sql = "UPDATE bikes set station_id = ?, position = ?, state = ?, modified_date = ? WHERE id = ?";
        Timestamp now = new Timestamp(new Date().getTime());
        super.updateOrDelete(sql, bike.getStation().getId(), bike.getPosition(), State.AVAILABLE, now, bike.getId());
    }

    @Override
    public Bike find(String bikeCode) {
        String sqlCommand = "SELECT * FROM bikes WHERE bike_code LIKE ?";
        List<Bike> bikes = super.query(sqlCommand, new BikeMapper(), bikeCode);

        return bikes.size() > 0 ? bikes.get(0) : null;
    }

    @Override
    public Paginator find(String keyword, String category, int skip, int perPage) {
        String sqlCommand = "SELECT * FROM bikes b JOIN categories c " +
                "WHERE b.category_id = c.id AND c.name LIKE ? AND (b.name LIKE ? OR b.bike_code LIKE ?) LIMIT ?, ?";
        List<Bike> bikes = super.query(sqlCommand, new BikeMapper(), category, '%' + keyword + '%', '%' + keyword + '%', skip, perPage);

        Paginator paginator = new Paginator();
        String sql = "SELECT count(b.id) as total FROM bikes b JOIN categories c " +
                "WHERE b.category_id = c.id AND c.name LIKE ? AND (b.name LIKE ? OR b.bike_code LIKE ?) LIMIT ?, ?";
        paginator.setTotal(super.queryCount(sql, "total", category, '%' + keyword + '%', '%' + keyword + '%', skip, perPage));
        paginator.setData(bikes);

        return paginator;
    }

    @Override
    public Paginator findByKeyword(String keyword, int skip, int perPage) {
        String sqlCommand = "SELECT * FROM bikes WHERE name LIKE ? OR bike_code LIKE ? LIMIT ?, ?";
        List<Bike> bikes = super.query(sqlCommand, new BikeMapper(), '%' + keyword + '%', '%' + keyword + '%', skip, perPage);

        Paginator paginator = new Paginator();
        String sql = "SELECT count(b.id) as total FROM bikes b WHERE b.name LIKE ? OR b.bike_code LIKE ? LIMIT ?, ?";
        paginator.setTotal(super.queryCount(sql, "total", '%' + keyword + '%', '%' + keyword + '%', skip, perPage));
        paginator.setData(bikes);

        return paginator;
    }

    @Override
    public Paginator findByCategory(String category, int skip, int perPage) {
        String sqlCommand = "SELECT * FROM bikes b JOIN categories c WHERE b.category_id = c.id AND c.name LIKE ? LIMIT ?, ?";
        List<Bike> bikes = super.query(sqlCommand, new BikeMapper(), category, skip, perPage);

        Paginator paginator = new Paginator();
        String sql = "SELECT count(b.id) as total FROM bikes b JOIN categories c WHERE b.category_id = c.id AND c.name LIKE ? LIMIT ?, ?";
        paginator.setTotal(super.queryCount(sql, "total", category, skip, perPage));
        paginator.setData(bikes);

        return paginator;
    }

    @Override
    public void update(int id, int state) {
        String sql = "UPDATE bikes SET state = ? WHERE id = ?";
        super.updateOrDelete(sql, state, id);
    }
}
