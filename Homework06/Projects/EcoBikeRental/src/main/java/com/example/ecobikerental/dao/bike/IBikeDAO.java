package com.example.ecobikerental.dao.bike;

import com.example.ecobikerental.dao.IBaseDAO;
import com.example.ecobikerental.entities.Bike;
import com.example.ecobikerental.entities.Paginator;

import java.util.List;

public interface IBikeDAO extends IBaseDAO<Bike> {
    public Long save(Bike bike);
    public int countAll();
    public int checkExistLicencePlate(String licencePlate);
    public int checkUsedPosition(Integer stationId, Integer position);
    public List<Bike> findAll();
    public Paginator findAll(int skip, int perPage);
    public List<Bike> getBikesInStation(int id);
    public Bike find(int id);
    public void updateToStation(Bike bike);
    public Bike find(String bikeCode);
    public Paginator find(String keyword, String category, int skip, int perPage);
    public Paginator findByKeyword(String keyword, int skip, int perPage);
    public Paginator findByCategory(String category, int skip, int perPage);
    public void update(int id, int state);
}
