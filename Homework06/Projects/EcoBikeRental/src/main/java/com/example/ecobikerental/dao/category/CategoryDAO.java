package com.example.ecobikerental.dao.category;

import com.example.ecobikerental.dao.BaseDAO;
import com.example.ecobikerental.dao.mapper.BikeMapper;
import com.example.ecobikerental.dao.mapper.CategoryMapper;
import com.example.ecobikerental.entities.Bike;
import com.example.ecobikerental.entities.Category;

import java.util.List;

public class CategoryDAO extends BaseDAO<Category> implements ICategoryDAO {

    @Override
    public List<Category> findAll() {
        String sqlCommand = "SELECT * FROM categories";

        return super.query(sqlCommand, new CategoryMapper());
    }

    @Override
    public Category find(int id) {
        String sqlCommand = "SELECT * FROM categories WHERE id = ?";
        List<Category> categories = super.query(sqlCommand, new CategoryMapper(), id);

        return categories.size() > 0 ? categories.get(0) : null;
    }
}
