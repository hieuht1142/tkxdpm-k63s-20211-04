package com.example.ecobikerental.dao.category;

import com.example.ecobikerental.dao.IBaseDAO;
import com.example.ecobikerental.entities.Category;

import java.util.List;

public interface ICategoryDAO extends IBaseDAO<Category> {
    public List<Category> findAll();
    public Category find(int id);
}
