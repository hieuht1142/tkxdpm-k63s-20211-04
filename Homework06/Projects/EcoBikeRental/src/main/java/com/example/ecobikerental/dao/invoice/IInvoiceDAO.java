package com.example.ecobikerental.dao.invoice;

import com.example.ecobikerental.dao.IBaseDAO;
import com.example.ecobikerental.entities.Invoice;

public interface IInvoiceDAO extends IBaseDAO<Invoice> {
    public int create(Invoice invoice);
}
