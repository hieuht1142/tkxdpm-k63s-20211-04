package com.example.ecobikerental.dao.mapper;

import com.example.ecobikerental.dto.BikeDetailsDto;

import java.sql.ResultSet;
import java.sql.SQLException;

public class BikeDetailsMapper implements IBaseMapper<BikeDetailsDto> {

    @Override
    public BikeDetailsDto mapRow(ResultSet rs) {
        BikeDetailsDto bikeDto = new BikeDetailsDto();
        try {
            bikeDto.setName(rs.getString("name"));
            bikeDto.setLicensePlate(rs.getString("licence_plate"));
            bikeDto.setPrice(rs.getLong("price"));
            bikeDto.setCategoryName(rs.getString("category_name"));
            bikeDto.setStationName(rs.getString("station_name"));
            bikeDto.setPosition(rs.getInt("position"));
            bikeDto.setState(rs.getInt("state"));
            return bikeDto;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }
}
