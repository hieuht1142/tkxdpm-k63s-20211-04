package com.example.ecobikerental.dao.mapper;

import com.example.ecobikerental.dao.category.CategoryDAO;
import com.example.ecobikerental.dao.station.StationDAO;
import com.example.ecobikerental.entities.Bike;
import com.example.ecobikerental.entities.Category;
import com.example.ecobikerental.entities.Station;

import java.sql.ResultSet;
import java.sql.SQLException;

public class BikeMapper implements IBaseMapper<Bike> {
    @Override
    public Bike mapRow(ResultSet rs) {
        try {
            Bike bike = new Bike();
            bike.setId(rs.getInt("id"));
            bike.setName(rs.getString("name"));
            bike.setBikeCode(rs.getString("bike_code"));
            bike.setWeight(rs.getFloat("weight"));
            bike.setLicencePlate(rs.getString("licence_plate"));
            bike.setManufacturingDate(rs.getDate("manufacturing_date"));
            bike.setProducer(rs.getString("producer"));
            bike.setPrice(rs.getLong("price"));
            bike.setPosition(rs.getInt("position"));
            bike.setState(rs.getInt("state"));
            bike.setBatteryPercentage(rs.getInt("battery_percentage"));
            bike.setLoadCycles(rs.getInt("load_cycles"));
            bike.setTimeRemaining(rs.getInt("time_remaining"));
            bike.setCreatedDate(rs.getTimestamp("created_date"));
            bike.setCreatedBy(rs.getString("created_by"));
            bike.setModifiedDate(rs.getTimestamp("modified_date"));
            bike.setModifiedBy(rs.getString("modified_by"));
            bike.setImage(rs.getString("image"));

            CategoryDAO categoryDAO = new CategoryDAO();
            Category category = categoryDAO.find(rs.getInt("category_id"));
            bike.setCategory(category);
            bike.setDeposit(category.getDeposit());

            StationDAO stationDAO = new StationDAO();
            Station station = stationDAO.find(rs.getInt("station_id"));
            bike.setStation(station);

            return bike;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return null;
        }
    }
}
