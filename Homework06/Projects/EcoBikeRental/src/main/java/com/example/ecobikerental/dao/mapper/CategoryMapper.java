package com.example.ecobikerental.dao.mapper;

import com.example.ecobikerental.entities.Category;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CategoryMapper implements IBaseMapper<Category> {
    @Override
    public Category mapRow(ResultSet rs) {
        Category category = new Category();
        
        try {
            category.setId(rs.getInt("id"));
            category.setName(rs.getString("name"));
            category.setDeposit(rs.getLong("deposit"));
            category.setPriceRate(rs.getFloat("price_rate"));
            category.setStatus(rs.getInt("status"));
            category.setCreatedDate(rs.getTimestamp("created_date"));
            category.setCreatedBy(rs.getString("created_by"));
            category.setModifiedDate(rs.getTimestamp("modified_date"));
            category.setModifiedBy(rs.getString("modified_by"));
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return category;
    }
}
