package com.example.ecobikerental.dao.mapper;

import com.example.ecobikerental.dao.user.UserDAO;
import com.example.ecobikerental.entities.*;
import com.example.ecobikerental.entities.paymentcard.CreditCard;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CreditCardMapper implements IBaseMapper<CreditCard> {
    private UserDAO userDAO;

    public CreditCardMapper(){
        userDAO = new UserDAO();
    }

    @Override
    public CreditCard mapRow(ResultSet rs) {
        try {
            CreditCard creditCard = new CreditCard();
            creditCard.setId(rs.getInt("id"));
            creditCard.setCardCode(rs.getString("number"));
            creditCard.setOwner(rs.getString("holder_name"));
            creditCard.setCvvCode(rs.getString("security_code"));
            creditCard.setDateExpired(rs.getString("expiration_date"));

            User user = userDAO.find(rs.getInt("user_id"));
            creditCard.setUser(user);

            return creditCard;
        } catch (SQLException ex) {
            ex.printStackTrace();
            return null;
        }
    }
}
