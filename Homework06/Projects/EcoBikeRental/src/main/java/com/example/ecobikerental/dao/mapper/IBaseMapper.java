package com.example.ecobikerental.dao.mapper;

import java.sql.ResultSet;

public interface IBaseMapper<T>{
    public T mapRow(ResultSet rs);
}
