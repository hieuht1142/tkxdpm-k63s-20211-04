package com.example.ecobikerental.dao.mapper;

import com.example.ecobikerental.entities.Station;

import java.sql.ResultSet;
import java.sql.SQLException;

public class StationMapper implements IBaseMapper<Station>{
    @Override
    public Station mapRow(ResultSet rs) {
        try {
            Station station = new Station();
            station.setId(rs.getInt("id"));
            station.setName(rs.getString("name"));
            station.setTotalPositions(rs.getInt("total_positions"));
            station.setAddress(rs.getString("address"));
            station.setStatus(rs.getInt("status"));
            station.setImage(rs.getString("image"));
            station.setCreatedDate(rs.getTimestamp("created_date"));
            station.setCreatedBy(rs.getString("created_by"));
            station.setModifiedDate(rs.getTimestamp("modified_date"));
            station.setModifiedBy(rs.getString("modified_by"));

            return station;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return null;
        }
    }
}
