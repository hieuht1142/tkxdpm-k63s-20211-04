package com.example.ecobikerental.dao.mapper;

import com.example.ecobikerental.entities.User;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UserMapper implements IBaseMapper{

    @Override
    public Object mapRow(ResultSet rs) {
        User user = new User();
        try {
            user.setId(rs.getInt("id"));
            user.setName(rs.getString("name"));
            user.setUsername(rs.getString("username"));
            user.setPhoneNumber(rs.getString("phone_number"));
            user.setRole(rs.getString("role"));
            user.setPassword(rs.getString("password"));
            user.setStatus(rs.getInt("status"));
            user.setCreatedDate(rs.getTimestamp("created_date"));
            user.setCreatedBy(rs.getString("created_by"));
            user.setModifiedDate(rs.getTimestamp("modified_date"));
            user.setModifiedBy(rs.getString("modified_by"));
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return user;
    }
}
