package com.example.ecobikerental.dao.payment;

import com.example.ecobikerental.dao.IBaseDAO;
import com.example.ecobikerental.entities.Payment;

public interface IPaymentDAO extends IBaseDAO<Payment> {
    public void create(Payment payment);
}
