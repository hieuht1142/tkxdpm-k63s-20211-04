package com.example.ecobikerental.dao.payment;

import com.example.ecobikerental.dao.BaseDAO;
import com.example.ecobikerental.entities.Payment;

import java.sql.Timestamp;
import java.util.Date;

public class PaymentDAO extends BaseDAO<Payment> implements IPaymentDAO {

    @Override
    public void create(Payment payment) {
        String sql = "INSERT INTO payments (content, errorCode, credit_card_id, invoice_id, created_date, modified_date)" +
                "VALUES (?, ?, ?, ?, ?, ?)";
        Timestamp now = new Timestamp(new Date().getTime());
        super.insert(sql, payment.getContent(), payment.getErrorCode(), payment.getPaymentCard().getId(), payment.getInvoice().getId(), now, now);
    }
}
