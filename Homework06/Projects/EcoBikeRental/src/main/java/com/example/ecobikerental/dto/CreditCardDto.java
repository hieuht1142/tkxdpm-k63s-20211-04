package com.example.ecobikerental.dto;

import com.example.ecobikerental.entities.paymentcard.CreditCard;

public class CreditCardDto {
    private String cardCode;
    private String owner;
    private String cvvCode;
    private String dateExpired;

    public CreditCardDto(CreditCard creditCard) {
        this.cardCode = creditCard.getCardCode();
        this.owner = creditCard.getOwner();
        this.cvvCode = creditCard.getCvvCode();
        this.dateExpired = creditCard.getDateExpired();
    }

    public CreditCardDto(String cardCode, String owner, String cvvCode, String dateExpired) {
        this.cardCode = cardCode;
        this.owner = owner;
        this.cvvCode = cvvCode;
        this.dateExpired = dateExpired;
    }

    public String getCardCode() {
        return cardCode;
    }

    public void setCardCode(String cardCode) {
        this.cardCode = cardCode;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getCvvCode() {
        return cvvCode;
    }

    public void setCvvCode(String cvvCode) {
        this.cvvCode = cvvCode;
    }

    public String getDateExpired() {
        return dateExpired;
    }

    public void setDateExpired(String dateExpired) {
        this.dateExpired = dateExpired;
    }

    @Override
    public String toString() {
        return "{" +
                super.toString() +
                " cardCode='" + cardCode + "'" +
                ", owner='" + owner + "'" +
                ", cvvCode='" + cvvCode + "'" +
                ", dateExpired='" + dateExpired + "'" +
                "}";
    }
}
