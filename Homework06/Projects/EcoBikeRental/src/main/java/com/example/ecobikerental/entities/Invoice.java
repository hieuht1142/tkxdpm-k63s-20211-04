package com.example.ecobikerental.entities;

public class Invoice extends BaseEntity{
    private Rental rental;
    private int type;
    private long totalAmount;
    private Bike bike;

    public Invoice() {
    }

    public Invoice(Rental rental) {
        this.rental = rental;
    }

    public Rental getRental() {
        return rental;
    }

    public void setRental(Rental rental) {
        this.rental = rental;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public long getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(long totalAmount) {
        this.totalAmount = totalAmount;
    }

    public Bike getBike() {
        return bike;
    }

    public void setBike(Bike bike) {
        this.bike = bike;
    }

    @Override
    public String toString() {
        return "{" +
                super.toString() +
                " rental='" + rental + "'" +
                ", type='" + type + "'" +
                ", totalAmount='" + totalAmount + "'" +
                "}";
    }
}
