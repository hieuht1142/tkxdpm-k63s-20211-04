package com.example.ecobikerental.entities;

import com.example.ecobikerental.entities.paymentcard.PaymentCard;

import java.sql.Timestamp;
import java.util.Date;

public class Payment extends BaseEntity{
    private PaymentCard paymentCard;
    private Invoice invoice;
    private String errorCode;
    private String content;
    private String transId;

    public Payment(String errorCode, String content, String transId) {
        this.errorCode = errorCode;
        this.content = content;
        this.transId = transId;
        this.createdDate = new Timestamp(new Date().getTime());
    }

    public String getTransId() {
        return transId;
    }

    public void setTransId(String transId) {
        this.transId = transId;
    }

    public PaymentCard getPaymentCard() {
        return paymentCard;
    }

    public void setPaymentCard(PaymentCard paymentCard) {
        this.paymentCard = paymentCard;
    }

    public Invoice getInvoice() {
        return invoice;
    }

    public void setInvoice(Invoice invoice) {
        this.invoice = invoice;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return "{" +
                super.toString() +
                " creditCard='" + paymentCard + "'" +
                ", invoice='" + invoice + "'" +
                ", errorCode='" + errorCode + "'" +
                ", content='" + content + "'" +
                ", transId='" + transId + "'" +
                "}";
    }
}
