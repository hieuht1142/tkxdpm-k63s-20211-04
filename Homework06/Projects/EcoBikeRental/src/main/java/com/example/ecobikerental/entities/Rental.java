package com.example.ecobikerental.entities;

import java.sql.Timestamp;

public class Rental extends BaseEntity {
    private Bike bike;
    private User renter;
    private Timestamp startTime;
    private Timestamp endTime;
    private long rentingFee;
    private int rentingType;
    private long deposit;
    private int state;

    public Rental() {
    }

    public Rental(Bike bike) {
        this.bike = bike;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public long getDeposit() {
        return deposit;
    }

    public void setDeposit(long deposit) {
        this.deposit = deposit;
    }

    public int getRentingType() {
        return rentingType;
    }

    public void setRentingType(int rentingType) {
        this.rentingType = rentingType;
    }

    public Timestamp getStartTime() {
        return startTime;
    }

    public void setStartTime(Timestamp startTime) {
        this.startTime = startTime;
    }

    public Timestamp getEndTime() {
        return endTime;
    }

    public void setEndTime(Timestamp endTime) {
        this.endTime = endTime;
    }

    public long getRentingFee() {
        return rentingFee;
    }

    public void setRentingFee(long rentingFee) {
        this.rentingFee = rentingFee;
    }

    public Bike getBike() {
        return bike;
    }

    public void setBike(Bike bike) {
        this.bike = bike;
    }

    public User getRenter() {
        return renter;
    }

    public void setRenter(User renter) {
        this.renter = renter;
    }

    @Override
    public String toString() {
        return "{" +
                super.toString() +
                " rentingType='" + rentingType + "'" +
                ", deposit='" + deposit + "'" +
                ", startTime='" + startTime + "'" +
                ", endTime='" + endTime + "'" +
                ", rentingFee='" + rentingFee + "'" +
                ", bike='" + bike + "'" +
                ", renter='" + renter + "'" +
                ", state='" + state + "'" +
                "}";
    }
}
