package com.example.ecobikerental.entities;

public class User extends BaseEntity {
    private String username;
    private String name;
    private String phoneNumber;
    private String role;
    private String password;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return "{" +
                super.toString() +
                " username='" + username + "'" +
                ", name='" + name + "'" +
                ", phoneNumber='" + phoneNumber + "'" +
                ", role='" + role + "'" +
                ", password='" + password + "'" +
                "}";
    }
}
