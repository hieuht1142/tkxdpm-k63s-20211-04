package com.example.ecobikerental.enums.category;

public class Category {
    public static final int BIKE = 1;
    public static final int E_BIKE = 2;
    public static final int TWIN_BIKE = 3;
    public static final String BIKE_TEXT = "Bike";
    public static final String E_BIKE_TEXT = "Electric Bike";
    public static final String TWIN_BIKE_TEXT = "Twin Bike";
}
