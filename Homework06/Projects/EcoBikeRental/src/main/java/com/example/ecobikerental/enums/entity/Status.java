package com.example.ecobikerental.enums.entity;

public final class Status {
    public static final int ACTIVE = 1;
    public static final int INACTIVE = 0;
}
