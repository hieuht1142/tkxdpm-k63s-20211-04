package com.example.ecobikerental.enums.rental;

import java.util.Arrays;
import java.util.List;

public final class RentingType {
    public static final int BY_MINUTES = 0;
    public static final int BY_DAY = 1;
    public static final String BY_MINUTES_TEXT = "By Minutes";
    public static final String BY_DAY_TEXT = "By Day";
    public static final List<String> ALL = Arrays.asList(BY_MINUTES_TEXT, BY_DAY_TEXT);
}
