package com.example.ecobikerental.enums.user;

public final class Role {
    public static final String RENTER = "renter";
    public static final String ADMIN = "admin";
}
