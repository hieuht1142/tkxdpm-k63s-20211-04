package com.example.ecobikerental.exceptions;

public class AddBikeException extends RuntimeException {

    public AddBikeException() { }

    public AddBikeException(String message) {
        super(message);
    }
}
