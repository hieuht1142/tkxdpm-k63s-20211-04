package com.example.ecobikerental.exceptions;

public class ExistedLicencePlateException extends AddBikeException {

    public ExistedLicencePlateException() {
        super("Licence plate existed!");
    }
}
