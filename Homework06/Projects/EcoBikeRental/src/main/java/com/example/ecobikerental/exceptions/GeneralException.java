package com.example.ecobikerental.exceptions;

;

/**
 * The GeneralException wraps all unchecked exceptions.
 *
 * @author Vu Minh Hieu
 */
public class GeneralException extends RuntimeException {

    public GeneralException() {

    }

    public GeneralException(String message) {
        super(message);
    }
}