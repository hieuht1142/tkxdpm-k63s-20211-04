package com.example.ecobikerental.exceptions;

/**
 * InvalidCredentialsException is thrown when the credentials using for login is not correct
 *
 * @author Vu Minh Hieu
 */
public class InvalidCredentialsException extends GeneralException {

    public InvalidCredentialsException() {
    }

    public InvalidCredentialsException(String message) {
        super(message);
    }

}
