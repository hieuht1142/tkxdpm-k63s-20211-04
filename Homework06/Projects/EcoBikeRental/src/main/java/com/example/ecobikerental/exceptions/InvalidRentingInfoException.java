package com.example.ecobikerental.exceptions;;

/**
 * InvalidRentingInfoException is thrown when the renting information is invalid.
 * 
 * @author Vu Minh Hieu
 */
public class InvalidRentingInfoException extends GeneralException {

	public InvalidRentingInfoException() {
	}

	public InvalidRentingInfoException(String message) {
		super(message);
	}

}