package com.example.ecobikerental.exceptions;

public class InvalidStationInfoException extends GeneralException {

    public InvalidStationInfoException() {

    }

    public InvalidStationInfoException(String message) {
        super(message);
    }
}
