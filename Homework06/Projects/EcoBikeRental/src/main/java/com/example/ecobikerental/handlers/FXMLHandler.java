package com.example.ecobikerental.handlers;

import com.example.ecobikerental.App;
import javafx.fxml.FXMLLoader;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;

import java.io.IOException;

public class FXMLHandler {
    protected FXMLLoader loader;
    protected HBox content;

    public FXMLHandler(String screenPath) throws IOException {
        this.loader = new FXMLLoader(App.class.getResource(screenPath));
        // Set this class as the controller
        this.loader.setController(this);
        this.content = (HBox) loader.load();
    }

    public HBox getContent() {
        return this.content;
    }

    public FXMLLoader getLoader() {
        return this.loader;
    }

    public void setImage(ImageView imv, String path) {
        Image image = new Image(String.valueOf(App.class.getResource(path)));
        imv.setImage(image);
    }
}
