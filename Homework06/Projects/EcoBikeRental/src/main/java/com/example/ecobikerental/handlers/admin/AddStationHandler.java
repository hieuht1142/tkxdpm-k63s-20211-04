package com.example.ecobikerental.handlers.admin;

import com.example.ecobikerental.controllers.AddStationController;
import com.example.ecobikerental.exceptions.InvalidStationInfoException;
import com.example.ecobikerental.utils.Configs;
import com.example.ecobikerental.utils.Toast;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.Hashtable;
import java.util.ResourceBundle;

public class AddStationHandler extends AdminBaseHandler implements Initializable {

    @FXML
    private TextField name;

    @FXML
    private TextField address;

    @FXML
    private Button imageBtn;

    @FXML
    private Button backBtn;

    @FXML
    private Button resetBtn;

    @FXML
    private Button submitBtn;

    @FXML
    private ImageView image;

    @FXML
    private ComboBox<String> comboBox;

    private File chosenImage;

    public AddStationHandler(Stage stage, String screenPath) throws IOException {
        super(stage, screenPath);
    }

    /**
     * initialize button click, text content binding, ... events.
     * @param url
     * @param resourceBundle
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        super.initialize(url, resourceBundle);
        setBController(new AddStationController());
        submitBtn.setOnMouseClicked(mouseEvent -> handleAddStation());
        imageBtn.setOnMouseClicked(mouseEvent -> handleChooseImage());
        resetBtn.setOnMouseClicked(mouseEvent -> handleResetForm());
        backBtn.setOnMouseClicked(mouseEvent -> handleCancel());
        comboBox.getItems().add("30(Only Available)");
    }

    public void handleAddStation() {
        Hashtable<String, String> stationInfo = new Hashtable<>();
        String copyImagePath = createCopyImagePath();
        if (name.getText() != null)
            stationInfo.put("name", name.getText());
        if (address.getText() != null)
            stationInfo.put("address", address.getText());
        if (comboBox.getValue() != null)
            stationInfo.put("total_positions", "30");
        stationInfo.put("image", copyImagePath);

        try {
            getBController().addStation(stationInfo);
            if (this.chosenImage != null) {
                String realCopyImagePath = System.getProperty("user.dir") + "/src/main/resources/com/example/ecobikerental/"
                        + copyImagePath;
                copyFile(this.chosenImage, realCopyImagePath);
            }
            try {
                StationManagementHandler stationManagementHandler = new StationManagementHandler(this.stage, Configs.STATION_MANAGEMENT_PATH);
                stationManagementHandler.setScreenTitle("Station Management");
                stationManagementHandler.show();
                Toast.makeText(stage, "Successfully!", "success");
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (InvalidStationInfoException e) {
            Toast.makeText(stage, e.getMessage(), "error");
        }
    }

    public void handleChooseImage() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter(
                "Image files", "*.jpg", "*.png", "*.jpeg"));
        File chosenFile = fileChooser.showOpenDialog(this.stage);
        if (chosenFile != null)
            image.setImage(new Image(chosenFile.toURI().toString(), 242, 161, false, true));
        this.chosenImage = chosenFile;
    }

    public void handleResetForm() {
        name.setText(null);
        address.setText(null);
        comboBox.getSelectionModel().clearSelection();
        setImage(image, "assets/images/station.png");
    }

    public void handleCancel() {
        getPreviousScreen().show();
    }

    public String createCopyImagePath() {
        if (this.chosenImage == null) {
            return "assets/images/station.png";
        }
        String fileName = this.chosenImage.getName();
        int i = fileName.lastIndexOf('.');
        String fileExtension = null;
        if (i > 0) {
            fileExtension = fileName.substring(i);
        }
        String stationImageName = this.name.getText();

        return "assets/images/" + stationImageName + fileExtension;
    }

    public void copyFile(File originalFile, String copyFilePath) {
        File newFile = new File(copyFilePath);
        try {
            Files.copy(originalFile.toPath(),
                    newFile.toPath(),
                    StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public AddStationController getBController() {
        return (AddStationController) super.getBController();
    }

}