package com.example.ecobikerental.handlers.admin;

import com.example.ecobikerental.handlers.BaseHandler;
import com.example.ecobikerental.utils.Configs;
import com.jfoenix.controls.JFXButton;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class AdminBaseHandler extends BaseHandler implements Initializable {
    @FXML
    protected JFXButton homeBtn;
    @FXML
    protected JFXButton bikeBtn;
    @FXML
    protected JFXButton stationBtn;

    public AdminBaseHandler(Stage stage, String screenPath) throws IOException {
        super(stage, screenPath);
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        super.initialize(url, resourceBundle);
        setHomeBtnListener();
        setBikeBtnListener();
        setStationBtnListener();
    }

    private void setHomeBtnListener() {
        if (homeBtn != null) {
            homeBtn.setOnMouseClicked(e -> {
                try {
                    AdminHomeHandler homeHandler = new AdminHomeHandler(this.stage, Configs.HOME_ADMIN_PATH);
                    homeHandler.setScreenTitle("Home");
                    homeHandler.show();
                } catch (IOException exp) {
                    exp.printStackTrace();
                }
            });
        }
    }

    private void setBikeBtnListener() {
        if (bikeBtn != null) {
            bikeBtn.setOnMouseClicked(e -> {
                try {
                    BikeManagementHandler bikeManagementHandler = new BikeManagementHandler(this.stage, Configs.BIKE_MANAGEMENT_PATH);
                    bikeManagementHandler.setScreenTitle("Bike Management");
                    bikeManagementHandler.show();
                } catch (IOException exp) {
                    exp.printStackTrace();
                }
            });
        }
    }

    private void setStationBtnListener() {
        if (stationBtn != null) {
            stationBtn.setOnMouseClicked(e -> {
                try {
                    StationManagementHandler stationManagementHandler = new StationManagementHandler(this.stage, Configs.STATION_MANAGEMENT_PATH);
                    stationManagementHandler.setScreenTitle("Station Management");
                    stationManagementHandler.show();
                } catch (IOException exp) {
                    exp.printStackTrace();
                }
            });
        }
    }
}
