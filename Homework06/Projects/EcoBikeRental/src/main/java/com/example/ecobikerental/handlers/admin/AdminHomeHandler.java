package com.example.ecobikerental.handlers.admin;

import com.example.ecobikerental.controllers.AdminHomeController;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class AdminHomeHandler extends AdminBaseHandler {

    @FXML
    private Label totalBikeLabel;

    @FXML
    private Label totalStationLabel;

    @FXML
    private Label totalPositionLabel;

    @FXML
    private Label avaiBikeLabel;

    @FXML
    private Label rentedBikeLabel;

    private AdminHomeController adminHomeController;

    public AdminHomeHandler(Stage stage, String screenPath) throws IOException {
        super(stage, screenPath);
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        super.initialize(url, resourceBundle);
        adminHomeController = new AdminHomeController();

        int totalBikes = adminHomeController.getTotalBikes();
        totalBikeLabel.setText(String.valueOf(totalBikes));

        int totalStations = adminHomeController.getTotalStations();
        totalStationLabel.setText(String.valueOf(totalStations));

        int totalPositions = adminHomeController.getTotalPositions();
        totalPositionLabel.setText(String.valueOf(totalPositions));

        int avaiBikes = adminHomeController.getTotalAvaiableBikes();
        avaiBikeLabel.setText(String.valueOf(avaiBikes));

        int rentedBikes = adminHomeController.getTotalRentedBikes();
        rentedBikeLabel.setText(String.valueOf(rentedBikes));
    }
}
