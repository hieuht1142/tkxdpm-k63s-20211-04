package com.example.ecobikerental.handlers.admin;

import com.example.ecobikerental.controllers.BikeManagementController;
import com.example.ecobikerental.dto.BikeDetailsDto;
import com.example.ecobikerental.utils.Configs;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

public class BikeManagementHandler extends AdminBaseHandler {

    private static final int PAGE_SIZE = 10;

    @FXML
    private Button addButton;

    @FXML
    private Button editButton;

    @FXML
    private Button deleteButton;

    @FXML
    private Button searchButton;

    @FXML
    private TextField searchInput;

    @FXML
    private TableView<BikeDetailsDto> bikeTable;

    @FXML
    private TableColumn<BikeDetailsDto, String> bikeNameCol;

    @FXML
    private TableColumn<BikeDetailsDto, String> licensePlateCol;

    @FXML
    private TableColumn<BikeDetailsDto, Long> priceCol;

    @FXML
    private TableColumn<BikeDetailsDto, String> categoryCol;

    @FXML
    private TableColumn<BikeDetailsDto, String> stationCol;

    @FXML
    private TableColumn<BikeDetailsDto, Integer> positionCol;

    @FXML
    private TableColumn<BikeDetailsDto, Integer> stateCol;

    @FXML
    private Pagination pagination;

    private BikeManagementController bikeManagementController;

    public BikeManagementHandler(Stage stage, String screenPath) throws IOException {
        super(stage, screenPath);
    }

    public void requestToAddBike() {
        try {
            AddBikeHandler addBikeHandler = new AddBikeHandler(this.stage, Configs.ADD_BIKE_PATH);
            addBikeHandler.setPreviousScreen(this);
            addBikeHandler.setScreenTitle("Add bike");
            addBikeHandler.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        super.initialize(url, resourceBundle);
        addButton.setOnAction(event -> {
            requestToAddBike();
        });
        bikeManagementController = new BikeManagementController();
        initializeBikeTable();
        initializePagination();
    }

    private void initializeBikeTable() {
        bikeNameCol.setCellValueFactory( new PropertyValueFactory<>("name") );
        licensePlateCol.setCellValueFactory(new PropertyValueFactory<>("licensePlate"));
        priceCol.setCellValueFactory( new PropertyValueFactory<>("price") );
        categoryCol.setCellValueFactory( new PropertyValueFactory<>("categoryName") );
        stationCol.setCellValueFactory( new PropertyValueFactory<>("stationName") );
        positionCol.setCellValueFactory( new PropertyValueFactory<>("position") );
        stateCol.setCellValueFactory( new PropertyValueFactory<>("state") );
    }

    public void initializePagination() {
        pagination.setPageCount(bikeManagementController.countPage(PAGE_SIZE));
        pagination.setMaxPageIndicatorCount(5);
        pagination.setPageFactory(this::createPage);
        pagination.setCurrentPageIndex(1);
    }

    private Node createPage(int page) {
        int offset = page * PAGE_SIZE;
        List<BikeDetailsDto> bikes = bikeManagementController.getBikeTablePageData(offset, PAGE_SIZE);
        ObservableList<BikeDetailsDto> bikeItems = FXCollections.observableList(bikes);
        bikeTable.setItems(bikeItems);
        return bikeTable;
    }
}
