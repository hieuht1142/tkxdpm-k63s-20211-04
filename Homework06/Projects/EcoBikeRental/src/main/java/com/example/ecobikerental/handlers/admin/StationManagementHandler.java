package com.example.ecobikerental.handlers.admin;

import com.example.ecobikerental.App;
import com.example.ecobikerental.controllers.StationManagementController;
import com.example.ecobikerental.entities.Station;
import com.example.ecobikerental.utils.Configs;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

public class StationManagementHandler extends AdminBaseHandler implements Initializable {

    private static final int PAGE_SIZE = 8;

    @FXML
    private Button addButton;
    @FXML
    private Button editButton;
    @FXML
    private Button deleteButton;
    @FXML
    private Button searchButton;
    @FXML
    private TextField searchInput;
    @FXML
    private TableView<Station> stationTable;
    @FXML
    private TableColumn<Station, String> idCol;
    @FXML
    private TableColumn<Station, String> nameCol;
    @FXML
    private TableColumn<Station, Integer> totalPositionsCol;
    @FXML
    private TableColumn<Station, String> addressCol;
    @FXML
    private TableColumn<Station, ImageView> imageCol;
    @FXML
    private TableColumn<Station, Integer> statusCol;
    @FXML
    private Pagination pagination;

    public StationManagementHandler(Stage stage, String screenPath) throws IOException {
        super(stage, screenPath);
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        super.initialize(url, resourceBundle);
        setBController(new StationManagementController());
        addButton.setOnMouseClicked(mouseEvent -> requestToAddStation());
        initializeStationTable();
        initializePagination();
    }

    public void initializeStationTable() {
        idCol.setCellValueFactory(new PropertyValueFactory<>("id"));
        nameCol.setCellValueFactory(new PropertyValueFactory<>("name"));
        totalPositionsCol.setCellValueFactory(new PropertyValueFactory<>("totalPositions"));
        addressCol.setCellValueFactory(new PropertyValueFactory<>("address"));
        imageCol.setCellValueFactory(new PropertyValueFactory<>("imageView"));
        statusCol.setCellValueFactory(new PropertyValueFactory<>("status"));
    }

    public void requestToAddStation() {
        try {
            AddStationHandler addStationHandler = new AddStationHandler(stage, Configs.ADD_STATION_PATH);
            addStationHandler.setPreviousScreen(this);
            addStationHandler.setScreenTitle("Add Station");
            addStationHandler.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void initializePagination() {
        pagination.setPageCount(getBController().countTotalPages(PAGE_SIZE));
        pagination.setMaxPageIndicatorCount(5);
        pagination.setPageFactory(this::createPage);
        pagination.setCurrentPageIndex(0);
    }

    private Node createPage(int page) {
        int offset = page * PAGE_SIZE;
        List<Station> stations =  getBController().findStationPage(offset, PAGE_SIZE);
        for (Station station : stations) {
            if (App.class.getResource(station.getImage()) != null)
                station.setImageView(new ImageView(new Image(String.valueOf(App.class.getResource(station.getImage())), 55, 35, false, true)));
            else
                station.setImageView(new ImageView(new Image(String.valueOf(App.class.getResource("assets/images/station-image-not-found.png")), 55, 35, false, true)));
        }
        ObservableList<Station> stationItems = FXCollections.observableList(stations);
        stationTable.setItems(stationItems);
        return stationTable;
    }

    public StationManagementController getBController() {
        return (StationManagementController) super.getBController();
    }
}
