package com.example.ecobikerental.handlers.auth;

import com.example.ecobikerental.exceptions.InvalidCredentialsException;
import com.example.ecobikerental.controllers.AuthController;
import com.example.ecobikerental.entities.User;
import com.example.ecobikerental.enums.user.Role;
import com.example.ecobikerental.handlers.BaseHandler;
import com.example.ecobikerental.handlers.admin.AdminHomeHandler;
import com.example.ecobikerental.handlers.user.HomeHandler;
import com.example.ecobikerental.utils.Configs;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.Objects;
import java.util.ResourceBundle;

public class LoginHandler extends BaseHandler implements Initializable {

    @FXML
    private Button loginBtn;
    @FXML
    private TextField username;
    @FXML
    private TextField password;
    @FXML
    private Text error;

    public LoginHandler(Stage stage, String screenPath) throws IOException {
        super(stage, screenPath);
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        setBController(new AuthController());
        loginBtn.setOnMouseClicked(e -> {
            try {
                requestToLogin();
            } catch (SQLException | IOException exp) {
                exp.printStackTrace();
            }
        });
    }

    public AuthController getBController() {
        return (AuthController) super.getBController();
    }

    public void requestToLogin() throws SQLException, IOException {
        try {
            String name = username.getText();
            String pass = password.getText();

            User user = getBController().login(name, pass);
            if (Objects.equals(user.getRole(), Role.RENTER)) {
                HomeHandler homeHandler = new HomeHandler(this.stage, Configs.HOME_USER_PATH);
                homeHandler.setScreenTitle("Home");
                homeHandler.show();
            } else {
                AdminHomeHandler adminHomeHandler = new AdminHomeHandler(this.stage, Configs.HOME_ADMIN_PATH);
                adminHomeHandler.setScreenTitle("Admin Home");
                adminHomeHandler.show();
            }
        } catch (InvalidCredentialsException e) {
            error.setText(e.getMessage());
        }
    }
}
