package com.example.ecobikerental.handlers.user;

import com.example.ecobikerental.enums.category.Category;
import com.example.ecobikerental.exceptions.BikeNotAvailableException;
import com.example.ecobikerental.controllers.BikeController;
import com.example.ecobikerental.controllers.RentBikeController;
import com.example.ecobikerental.entities.Bike;
import com.example.ecobikerental.handlers.BaseHandler;
import com.example.ecobikerental.utils.Configs;
import com.example.ecobikerental.utils.Toast;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;

import static com.example.ecobikerental.enums.category.Category.*;

public class BikeDetailHandler extends UserBaseHandler implements Initializable {
    @FXML
    private Button rentBikeBtn;
    @FXML
    private Button backBtn;
    @FXML
    private ImageView image;
    @FXML
    private Text name;
    @FXML
    private Text bikeCode;
    @FXML
    private Text type;
    @FXML
    private Text weight;
    @FXML
    private Text licencePlate;
    @FXML
    private Text manufacturingDate;
    @FXML
    private Text producer;
    @FXML
    private Text batteryPercentage;
    @FXML
    private Text deposit;
    @FXML
    private Text timeRemaining;
    @FXML
    private Text batteryLabel;
    @FXML
    private Text licencePlateLabel;
    @FXML
    private Text timeRemainingLabel;

    private Bike bike;

    public BikeDetailHandler(Stage stage, String screenPath, Bike bike) throws IOException {
        super(stage, screenPath);
        this.bike = bike;
        setBikeInfo(bike);
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        super.initialize(url, resourceBundle);
        setBController(new BikeController());
        rentBikeBtn.setOnMouseClicked(e -> {
            try {
                requestToRentBike();
            } catch (SQLException | IOException exp) {
                exp.printStackTrace();
            }
        });
        setBackBtnListener();
    }

    public void requestToRentBike() throws SQLException, IOException {
        try {
            RentBikeController rentBikeController = new RentBikeController();
            rentBikeController.rentBike(bike);
            displayRentingForm();
        } catch (BikeNotAvailableException e) {
            Toast.makeText(stage, e.getMessage(), "error");
        }
    }

    public void displayRentingForm() throws IOException {
        RentingFormHandler rentingFormHandler = new RentingFormHandler(this.stage, Configs.RENTING_FORM_PATH, bike);
        rentingFormHandler.setPreviousScreen(this);
        rentingFormHandler.setScreenTitle("Renting Information");
        rentingFormHandler.show();
    }

    private void setBikeInfo(Bike bike) {
        setImage(image, bike.getImage());
        name.setText(bike.getName());
        bikeCode.setText(bike.getBikeCode());
        type.setText(bike.getCategoryName());
        weight.setText(bike.getWeight() + " kg");
        licencePlate.setText(bike.getLicencePlate());
        manufacturingDate.setText(bike.getManufacturingDate().toString());
        producer.setText(bike.getProducer());
        batteryPercentage.setText(bike.getBatteryPercentage() + "%");
        deposit.setText(bike.getDeposit() + " VND");
        timeRemaining.setText(bike.getTimeRemaining() + " minutes");

        if (bike.getCategoryName().equals(BIKE_TEXT)) {
            batteryLabel.setText("Station");
            batteryPercentage.setText(bike.getStationName());
            licencePlateLabel.setText("Category");
            licencePlate.setText(bike.getCategoryName());
            timeRemainingLabel.setVisible(false);
            timeRemaining.setVisible(false);
        }
    }

    private void setBackBtnListener() {
        backBtn.setOnMouseClicked(e -> {
            try {
                goBackToPreviousScreen();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        });
    }
}
