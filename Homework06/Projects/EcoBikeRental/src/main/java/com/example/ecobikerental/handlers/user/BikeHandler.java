package com.example.ecobikerental.handlers.user;

import com.example.ecobikerental.entities.Bike;
import com.example.ecobikerental.handlers.BaseHandler;
import com.example.ecobikerental.utils.Configs;
import com.jfoenix.controls.JFXButton;
import javafx.fxml.FXML;
import javafx.scene.image.ImageView;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.io.IOException;

public class BikeHandler extends UserBaseHandler {
    @FXML
    private Text bikeName;
    @FXML
    private Text station;
    @FXML
    private Text bikeCode;
    @FXML
    private ImageView bikeImage;
    @FXML
    private JFXButton viewBtn;

    private Bike bike;
    private Stage stage;
    private SearchBikeHandler searchBikeHandler;

    public BikeHandler(String screenPath, Bike bike, Stage stage, SearchBikeHandler searchBikeHandler) throws IOException {
        super(stage, screenPath);
        this.bike = bike;
        this.stage = stage;
        this.searchBikeHandler = searchBikeHandler;

        setImage(bikeImage, bike.getImage());
        bikeName.setText(bike.getName());
        station.setText(bike.getStationName());
        bikeCode.setText(bike.getBikeCode());

        viewBtn.setOnMouseClicked(event -> {
            try {
                BikeDetailHandler bikeDetailHandler = new BikeDetailHandler(stage, Configs.BIKE_DETAIL_PATH, bike);
                bikeDetailHandler.setPreviousScreen(searchBikeHandler);
                bikeDetailHandler.setScreenTitle(bike.getName());
                bikeDetailHandler.show();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }
}
