package com.example.ecobikerental.handlers.user;

import com.example.ecobikerental.controllers.ReturnBikeController;
import com.example.ecobikerental.entities.Bike;
import com.example.ecobikerental.entities.Invoice;
import com.example.ecobikerental.entities.Rental;
import com.example.ecobikerental.entities.Station;
import com.example.ecobikerental.enums.bike.State;
import com.example.ecobikerental.enums.category.Category;
import com.example.ecobikerental.handlers.BaseHandler;
import com.example.ecobikerental.utils.Configs;
import com.example.ecobikerental.utils.Toast;
import com.jfoenix.controls.JFXButton;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.effect.DropShadow;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

public class ChooseBikeHandler extends UserBaseHandler {
    @FXML
    private JFXButton btnBack;
    @FXML
    private GridPane positions;
    @FXML
    private Text stationName;

    private Station station;

    private List<Bike> bikes;

    public ChooseBikeHandler(Stage stage, String screenPath) throws IOException {
        super(stage, screenPath);
        positions.getChildren().forEach(node -> {
            Rectangle slot = (Rectangle) node;
            slot.setOnMouseClicked(event -> {
                if(slot.getFill().equals(Color.web("0xbdbdbd"))) {
                    Toast.makeText(this.stage, "Không có xe nào ở vị trí này!", "error");
                }
            });
        });


        btnBack.setOnMouseClicked(event -> {
            getPreviousScreen().show();
        });
    }

    public void setStation(Station station) {
        this.station = station;
        stationName.setText(station.getName());
        ReturnBikeController controller = (ReturnBikeController) getBController();
        this.bikes = controller.getBikes(station.getId());
        for(Bike bike: this.bikes){
            setBikePosition(bike.getCategory().getId(), bike);
        }
    }

    private void setBikePosition(int type, Bike bike){
        Rectangle slot = (Rectangle) positions.getChildren().get(bike.getPosition());
        switch (type){
            case Category.BIKE:
                slot.setFill(Color.web("0xff834f"));
                break;
            case Category.E_BIKE:
                slot.setFill(Color.web("0xffeb3b"));
                break;
            case Category.TWIN_BIKE:
                slot.setFill(Color.web("0x8bc34a"));
                break;
        }
        slot.setOnMouseClicked(event -> {
            try {
                BikeDetailHandler bikeDetailHandler = new BikeDetailHandler(stage, Configs.BIKE_DETAIL_PATH, bike);
                bikeDetailHandler.setPreviousScreen(this);
                bikeDetailHandler.setScreenTitle(bike.getName());
                bikeDetailHandler.show();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }
}
