package com.example.ecobikerental.handlers.user;

import com.example.ecobikerental.controllers.ReturnBikeController;
import com.example.ecobikerental.entities.Bike;
import com.example.ecobikerental.entities.Invoice;
import com.example.ecobikerental.entities.Rental;
import com.example.ecobikerental.entities.Station;
import com.example.ecobikerental.enums.bike.State;
import com.example.ecobikerental.enums.category.Category;
import com.example.ecobikerental.handlers.BaseHandler;
import com.example.ecobikerental.utils.Configs;
import com.example.ecobikerental.utils.Toast;
import com.jfoenix.controls.JFXButton;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.effect.DropShadow;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

public class ChoosePositionHandler extends UserBaseHandler implements Initializable {
    @FXML
    private JFXButton btnBack;
    @FXML
    private GridPane positions;
    @FXML
    private Text stationName;
    @FXML
    private JFXButton btnReturnBike;

    private Station station;
    private Rental rental;

    private List<Bike> bikes;
    private List<Rectangle> selected = new ArrayList<>();

    public ChoosePositionHandler(Stage stage, String screenPath) throws IOException {
        super(stage, screenPath);
        positions.getChildren().forEach(node -> {
            Rectangle slot = (Rectangle) node;
            slot.setOnMouseClicked(event -> {
                if(slot.getFill().equals(Color.web("0xbdbdbd"))) {
                    btnReturnBike.setDisable(false);
                    if (selected.size() == 0) {
                        //click lan dau
                        selected.add(slot);
                        slot.setEffect(new DropShadow(15, Color.RED));
                    } else if (selected.get(0) == slot) {
                        //click lai lan 2
                        slot.setEffect(null);
                        selected.remove(0);
                    } else {
                        //click o khac
                        selected.get(0).setEffect(null);
                        selected.remove(0);
                        slot.setEffect(new DropShadow(15, Color.RED));
                        selected.add(slot);
                    }
                } else {
                    Toast.makeText(this.stage, "Đã có xe ở vị trí này!", "error");
                }
            });
        });

        btnReturnBike.setDisable(true);

        btnReturnBike.setOnMouseClicked(event -> {
            if(selected.size() > 0){
                //Create Invoice and go to Invoice Screen
                Integer X = GridPane.getColumnIndex(selected.get(0));
                Integer Y = GridPane.getRowIndex(selected.get(0));
                int tmpX = X==null?0:X;
                int tmpY = Y==null?0:Y;
                int index = tmpX + tmpY*6;
                Bike bike = rental.getBike();
                bike.setPosition(index);
                bike.setStation(station);
                bike.setState(State.AVAILABLE);
                rental.setEndTime(new Timestamp(new Date().getTime()));
                rental.setState(com.example.ecobikerental.enums.rental.State.FINISHED);
                Invoice invoice = new Invoice();
                invoice.setRental(rental);
                long fee = ((ReturnBikeController)getBController()).calculateRentingFee(rental);
                rental.setRentingFee(fee);

                try {
                    InvoiceHandler invoiceHandler = new InvoiceHandler(this.stage, Configs.INVOICE_PATH, invoice);
                    invoiceHandler.setBController(getBController());
                    invoiceHandler.setHomeHandler(homeHandler);
                    invoiceHandler.setPreviousScreen(this);
                    invoiceHandler.setScreenTitle("Invoice");
                    invoiceHandler.show();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        });

        btnBack.setOnMouseClicked(event -> {
            getPreviousScreen().show();
        });
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        super.initialize(url, resourceBundle);
    }

    public void setStation(Station station) {
        this.station = station;
        stationName.setText(station.getName());
        ReturnBikeController controller = (ReturnBikeController) getBController();
        this.bikes = controller.getBikes(station.getId());
        for(Bike bike: this.bikes){
            setBikePosition(bike.getCategory().getId(), bike.getPosition());
        }
    }

    public void setRental(Rental rental){
        this.rental = rental;
    }

    private void setBikePosition(int type, int position){
        Rectangle slot = (Rectangle) positions.getChildren().get(position);
        switch (type){
            case Category.BIKE:
                slot.setFill(Color.web("0xff834f"));
                break;
            case Category.E_BIKE:
                slot.setFill(Color.web("0xffeb3b"));
                break;
            case Category.TWIN_BIKE:
                slot.setFill(Color.web("0x8bc34a"));
                break;
        }
    }
}
