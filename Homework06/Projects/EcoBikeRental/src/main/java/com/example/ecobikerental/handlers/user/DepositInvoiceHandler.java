package com.example.ecobikerental.handlers.user;

import com.example.ecobikerental.controllers.AuthController;
import com.example.ecobikerental.entities.Rental;
import com.example.ecobikerental.controllers.RentBikeController;
import com.example.ecobikerental.entities.Bike;
import com.example.ecobikerental.entities.Invoice;
import com.example.ecobikerental.entities.User;
import com.example.ecobikerental.utils.Configs;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ResourceBundle;

public class DepositInvoiceHandler extends UserBaseHandler implements Initializable, PaymentResultHandler {
    @FXML
    private Button confirmBtn;
    @FXML
    private Button cancelBtn;
    @FXML
    private ImageView image;
    @FXML
    private Text renterName;
    @FXML
    private Text phoneNumber;
    @FXML
    private Text bikeCode;
    @FXML
    private Text bikeName;
    @FXML
    private Text station;
    @FXML
    private Text deposit;
    @FXML
    private Text startTime;

    private Invoice invoice;

    public DepositInvoiceHandler(Stage stage, String screenPath, Invoice invoice) throws IOException {
        super(stage, screenPath);
        this.invoice = invoice;
        Bike bike = invoice.getBike();
        setImage(image, bike.getImage());
        bikeCode.setText(bike.getBikeCode());
        bikeName.setText(bike.getName());
        station.setText(bike.getStationName());
        deposit.setText(invoice.getTotalAmount() + " VND");
        String timeFormatted = new SimpleDateFormat("dd/MM/yyyy HH:mm").format(invoice.getRental().getStartTime());
        startTime.setText(timeFormatted);
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        super.initialize(url, resourceBundle);
        setConfirmBtnListener();
        setCancelBtnListener();
        setUserInfo();
    }

    public RentBikeController getBController() {
        return (RentBikeController) super.getBController();
    }

    private void requestToConfirmInvoice() throws IOException {
        PaymentHandler paymentHandler = new PaymentHandler(this.stage, Configs.PAYMENT_PATH, invoice, "Pay Deposit Invoice");
        paymentHandler.setPreviousScreen(this);
        paymentHandler.setScreenTitle("Payment Screen");
        paymentHandler.init();
        paymentHandler.show();
    }

    private void setConfirmBtnListener() {
        confirmBtn.setOnMouseClicked(e -> {
            try {
                requestToConfirmInvoice();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        });
    }

    private void setCancelBtnListener() {
        cancelBtn.setOnMouseClicked(e -> {
            try {
                goBackToPreviousScreen();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        });
    }

    @Override
    public void paymentSuccess(Invoice invoice) {
        Rental rental = invoice.getRental();
        int rentalId = getBController().saveRental(rental);
        getBController().updateBikeRented(rental.getBike());
        invoice.getRental().setId(rentalId);
    }

    private void setUserInfo() {
        AuthController authController = new AuthController();
        User user = authController.getAuthUser();
        renterName.setText(user.getName());
        phoneNumber.setText(user.getPhoneNumber());
    }
}
