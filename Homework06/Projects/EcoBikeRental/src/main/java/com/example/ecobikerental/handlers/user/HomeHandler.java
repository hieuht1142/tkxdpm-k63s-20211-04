package com.example.ecobikerental.handlers.user;

import com.example.ecobikerental.controllers.BikeController;
import com.example.ecobikerental.controllers.RentalController;
import com.example.ecobikerental.entities.Bike;
import com.example.ecobikerental.handlers.BaseHandler;
import com.example.ecobikerental.utils.Configs;
import com.example.ecobikerental.utils.Toast;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;

public class HomeHandler extends UserBaseHandler implements Initializable {
    @FXML
    private TextField bikeCodeTextField;
    @FXML
    private Button chooseStationBtn;
    @FXML
    private Button rentBikeBtn;
    @FXML
    private Button viewRentalBtn;

    public HomeHandler(Stage stage, String screenPath) throws IOException {
        super(stage, screenPath);
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        super.initialize(url, resourceBundle);

        setRentBikeBtnListener();
        setChooseStationBtnListener();
        setViewRentalBtnListener();
    }

    public void requestToSearchBike() throws SQLException, IOException {
        BikeController bikeController = new BikeController();
        String bikeCode = bikeCodeTextField.getText();
        Bike bike = bikeController.searchBike(bikeCode);

        if (bike == null) {
            Toast.makeText(stage, "No bike has been found!", "warning");
        } else {
            BikeDetailHandler bikeDetailHandler = new BikeDetailHandler(stage, Configs.BIKE_DETAIL_PATH, bike);
            bikeDetailHandler.setScreenTitle(bike.getName());
            bikeDetailHandler.show();
        }
    }

    private void setChooseStationBtnListener() {
        chooseStationBtn.setOnMouseClicked(e -> {
            try {
                SearchStationHandler searchStationHandler = new SearchStationHandler(this.stage, Configs.SEARCH_STATION_PATH);
                searchStationHandler.setScreenTitle("Search Station");
                searchStationHandler.show();
            } catch (IOException exp) {
                exp.printStackTrace();
            }
        });
    }

    private void setViewRentalBtnListener() {
        viewRentalBtn.setOnMouseClicked(e -> {
            try {
                RentingHistoryHandler rentingHistoryHandler = new RentingHistoryHandler(this.stage, Configs.VIEW_RENTING_HISTORY_PATH, new RentalController());
                rentingHistoryHandler.setScreenTitle("Renting History");
                rentingHistoryHandler.show();
            } catch (IOException exp) {
                exp.printStackTrace();
            }
        });
    }

    private void setRentBikeBtnListener() {
        rentBikeBtn.setOnMouseClicked(e -> {
            try {
                requestToSearchBike();
            } catch (SQLException | IOException exp) {
                exp.printStackTrace();
            }
        });
    }
}
