package com.example.ecobikerental.handlers.user;

import com.example.ecobikerental.controllers.PaymentController;
import com.example.ecobikerental.controllers.RentalController;
import com.example.ecobikerental.entities.paymentcard.CreditCard;
import com.example.ecobikerental.entities.Invoice;
import com.example.ecobikerental.entities.Payment;
import com.example.ecobikerental.enums.invoice.Type;
import com.example.ecobikerental.exceptions.InvalidPaymentInfoException;
import com.example.ecobikerental.utils.Configs;
import com.example.ecobikerental.utils.Toast;
import com.jfoenix.controls.JFXButton;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

public class PaymentHandler extends UserBaseHandler implements Initializable {
    private Invoice invoice;

    @FXML
    private TextField number;
    @FXML
    private TextField expirationDate;
    @FXML
    private TextField holderName;
    @FXML
    private TextField securityCode;
    @FXML
    private JFXButton submitBtn;
    @FXML
    private JFXButton resetBtn;
    @FXML
    private JFXButton backBtn;

    private String content;
    private CreditCard creditCard;

    public PaymentHandler(Stage stage, String screenPath, Invoice invoice, String content) throws IOException {
        super(stage, screenPath);
        this.invoice = invoice;
        this.content = content;
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        super.initialize(url, resourceBundle);
        setBController(new PaymentController());
        setBackBtnListener();

        CreditCard card = getBController().getUserCard();
        creditCard = card;
        number.setText(card.getCardCode());
        holderName.setText(card.getOwner());
        securityCode.setText(card.getCvvCode());
        expirationDate.setText(insertString(card.getDateExpired(), "/", 1));
        setResetBtnListener();
    }

    public PaymentController getBController() {
        return (PaymentController) super.getBController();
    }

    public void init() {
        submitBtn.setOnMouseClicked(event -> {
            HashMap<String, String> inputs = new HashMap<String, String>();
            inputs.put("number", number.getText());
            inputs.put("date", expirationDate.getText());
            inputs.put("holder", holderName.getText());
            inputs.put("security", securityCode.getText());

            try {
                boolean isValid = getBController().validatePaymentInfo(inputs);
            } catch (InvalidPaymentInfoException ex) {
                Toast.makeText(this.stage, ex.getMessage(), "error");
                return;
            }
            Map<String, Object> response;
            if (invoice.getType() == Type.REFUND_INVOICE) {
                response = getBController().refund((int) Math.abs(invoice.getTotalAmount()), content, number.getText(), holderName.getText(), expirationDate.getText(), securityCode.getText());
            } else {
                response = getBController().payOrder((int) Math.abs(invoice.getTotalAmount()), content, number.getText(), holderName.getText(),
                        expirationDate.getText(), securityCode.getText());
            }
            String result = (String) response.get("RESULT");
            String message = (String) response.get("MESSAGE");
            if (result.equals("Success")) {
                Payment payment = (Payment) response.get("Payment");
                payment.setInvoice(invoice);

                ((PaymentResultHandler) getPreviousScreen()).paymentSuccess(this.invoice);

//                if (invoice.getType() == Type.DEPOSIT_INVOICE) {
//                    RentBikeController renBikeController = new RentBikeController();
//                    int rentalId = renBikeController.saveRental(invoice.getRental());
//                    renBikeController.saveInvoice(invoice, rentalId);
//                }

                getBController().savePayment(payment);

                try {
                    Toast.makeText(this.stage, message, "success");
                    RentingHistoryHandler rentingHistoryHandler = new RentingHistoryHandler(this.stage, Configs.VIEW_RENTING_HISTORY_PATH, new RentalController());
                    rentingHistoryHandler.setScreenTitle("Renting History");
                    rentingHistoryHandler.show();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                Toast.makeText(this.stage, message, "error");
            }
        });
    }

    private void setResetBtnListener(){
        resetBtn.setOnMouseClicked(event -> {
            number.setText(creditCard.getCardCode());
            holderName.setText(creditCard.getOwner());
            securityCode.setText(creditCard.getCvvCode());
            expirationDate.setText(insertString(creditCard.getDateExpired(), "/", 1));
        });
    }

    private void setBackBtnListener() {
        backBtn.setOnMouseClicked(e -> {
            try {
                goBackToPreviousScreen();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        });
    }

    private String insertString(String originalString, String stringToBeInserted, int index) {
        return originalString.substring(0, index + 1)
                + stringToBeInserted
                + originalString.substring(index + 1);
    }
}
