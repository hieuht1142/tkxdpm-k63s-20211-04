package com.example.ecobikerental.handlers.user;

import com.example.ecobikerental.entities.Invoice;

public interface PaymentResultHandler {
    public void paymentSuccess(Invoice invoice);
}
