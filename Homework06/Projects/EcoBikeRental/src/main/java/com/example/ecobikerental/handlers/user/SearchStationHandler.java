package com.example.ecobikerental.handlers.user;

import com.example.ecobikerental.controllers.SearchStationController;
import com.example.ecobikerental.entities.Station;
import com.example.ecobikerental.handlers.BaseHandler;
import com.example.ecobikerental.utils.Configs;
import javafx.fxml.FXML;
import com.jfoenix.controls.JFXButton;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.ResourceBundle;

public class SearchStationHandler extends UserBaseHandler implements Initializable {

    @FXML
    private TextField stationName;

    @FXML
    private TextField stationAddress;

    @FXML
    private ComboBox<String> bikes;

    @FXML
    private ComboBox<String> eBikes;

    @FXML
    private ComboBox<String> twinBikes;

    @FXML
    private ComboBox<String> positions;

    @FXML
    private JFXButton btnSearch;

    @FXML
    private JFXButton btnReset;

    public SearchStationHandler(Stage stage, String screenPath) throws IOException {
        super(stage, screenPath);
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        super.initialize(url, resourceBundle);
        this.bikes.getItems().addAll("0", "5", "10", "15", "20", "25");
        this.eBikes.getItems().addAll("0", "5", "10", "15", "20", "25");
        this.twinBikes.getItems().addAll("0", "5", "10", "15", "20", "25");
        this.positions.getItems().addAll("0", "5", "10", "15", "20", "25");

        this.bikes.getSelectionModel().selectFirst();
        this.eBikes.getSelectionModel().selectFirst();
        this.twinBikes.getSelectionModel().selectFirst();
        this.positions.getSelectionModel().selectFirst();

        this.btnSearch.setOnMouseClicked(e -> {
            try {
                this.submitSearchInfo();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        });

        this.btnReset.setOnMouseClicked(e -> {
            this.resetSearchInfo();
        });
    }

    /**
     * Reset search information
     */
    public void resetSearchInfo() {
        stationName.setText("");
        stationAddress.setText("");
        bikes.getSelectionModel().selectFirst();
        eBikes.getSelectionModel().selectFirst();
        twinBikes.getSelectionModel().selectFirst();
        positions.getSelectionModel().selectFirst();
    }

    /**
     * Get searching information from screen and submit it
     * @throws IOException
     */
    public void submitSearchInfo() throws IOException {
        HashMap<String, String> info = new HashMap<String, String>();
        info.put("name", stationName.getText());
        info.put("address", stationAddress.getText());
        info.put("numBikes", bikes.getValue());
        info.put("numEBikes", eBikes.getValue());
        info.put("numTwinBikes", twinBikes.getValue());
        info.put("numEmptyPositions", positions.getValue());

        List<Station> stations = getBController().searchStations(info);
        BaseHandler searchStationResultHandler = new SearchStationResultHandler(this.stage, Configs.SEARCH_STATION_RESULT_PATH, stations);
        searchStationResultHandler.setPreviousScreen(this);
        searchStationResultHandler.setHomeHandler(homeHandler);
        searchStationResultHandler.setScreenTitle("Search Stations Result");
        searchStationResultHandler.setBController(getBController());
        searchStationResultHandler.show();
    }

    /**
     * Get base controller
     * @return base controller
     */
    public SearchStationController getBController(){
        return new SearchStationController();
    }

}
