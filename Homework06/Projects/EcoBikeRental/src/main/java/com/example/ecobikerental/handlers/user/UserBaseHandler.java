package com.example.ecobikerental.handlers.user;

import com.example.ecobikerental.controllers.RentalController;
import com.example.ecobikerental.handlers.BaseHandler;
import com.example.ecobikerental.utils.Configs;
import com.jfoenix.controls.JFXButton;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class UserBaseHandler extends BaseHandler implements Initializable {
    @FXML
    protected JFXButton homeBtn;
    @FXML
    protected JFXButton bikeBtn;
    @FXML
    protected JFXButton stationBtn;
    @FXML
    protected JFXButton rentalBtn;

    public UserBaseHandler(Stage stage, String screenPath) throws IOException {
        super(stage, screenPath);
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        super.initialize(url, resourceBundle);

        setHomeBtnListener();
        setBikeBtnListener();
        setStationBtnListener();
        setRentalBtnListener();
    }

    private void setHomeBtnListener() {
        if (homeBtn != null) {
            homeBtn.setOnMouseClicked(e -> {
                try {
                    HomeHandler homeHandler = new HomeHandler(this.stage, Configs.HOME_USER_PATH);
                    homeHandler.setScreenTitle("Home");
                    homeHandler.show();
                } catch (IOException exp) {
                    exp.printStackTrace();
                }
            });
        }
    }

    private void setBikeBtnListener() {
        if (bikeBtn != null) {
            bikeBtn.setOnMouseClicked(e -> {
                try {
                    SearchBikeHandler searchBikeHandler = new SearchBikeHandler(this.stage, Configs.SEARCH_BIKE_PATH);
                    searchBikeHandler.setScreenTitle("Search Bike");
                    searchBikeHandler.show();
                } catch (IOException exp) {
                    exp.printStackTrace();
                }
            });
        }
    }

    private void setStationBtnListener() {
        if (stationBtn != null) {
            stationBtn.setOnMouseClicked(e -> {
                try {
                    SearchStationHandler searchStationHandler = new SearchStationHandler(this.stage, Configs.SEARCH_STATION_PATH);
                    searchStationHandler.setScreenTitle("Search Station");
                    searchStationHandler.show();
                } catch (IOException exp) {
                    exp.printStackTrace();
                }
            });
        }
    }

    private void setRentalBtnListener() {
        if (rentalBtn != null) {
            rentalBtn.setOnMouseClicked(e -> {
                try {
                    RentingHistoryHandler rentingHistoryHandler = new RentingHistoryHandler(this.stage, Configs.VIEW_RENTING_HISTORY_PATH, RentalController.getInstance());
                    rentingHistoryHandler.setScreenTitle("Renting History");
                    rentingHistoryHandler.show();
                } catch (IOException exp) {
                    exp.printStackTrace();
                }
            });
        }
    }
}
