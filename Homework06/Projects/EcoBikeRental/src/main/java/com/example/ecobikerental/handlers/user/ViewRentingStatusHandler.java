package com.example.ecobikerental.handlers.user;

import com.example.ecobikerental.controllers.RentalController;
import com.example.ecobikerental.controllers.ReturnBikeController;
import com.example.ecobikerental.entities.Rental;
import com.example.ecobikerental.enums.rental.RentingType;
import com.example.ecobikerental.enums.rental.State;
import com.example.ecobikerental.handlers.BaseHandler;
import com.example.ecobikerental.utils.Configs;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.Date;

public class ViewRentingStatusHandler extends UserBaseHandler {
    @FXML
    private ImageView image;
    @FXML
    private Text bikeName;
    @FXML
    private Text bikeCode;
    @FXML
    private Text name;
    @FXML
    private Text id;
    @FXML
    private Text startTime;
    @FXML
    private Text endTime;
    @FXML
    private Text rentingType;
    @FXML
    private Text rentingFee;
    @FXML
    private Text status;
    @FXML
    private Button btn_2;

    private Rental rental;

    public ViewRentingStatusHandler(Stage stage, String screenPath,Rental rental) throws IOException {
        super(stage, screenPath);
        this.rental = rental;
    }

    public void displayRentingStatus(Rental rental){
        setImage(image, rental.getBike().getImage());
        bikeName.setText(rental.getBike().getName());
        bikeCode.setText(rental.getBike().getBikeCode());
        name.setText(rental.getRenter().getName()==null?rental.getRenter().getUsername():rental.getRenter().getName());
        id.setText(Integer.toString(rental.getRenter().getId()));
        startTime.setText(String.valueOf(rental.getStartTime()));
        endTime.setText(rental.getEndTime()==null?"":String.valueOf(rental.getEndTime()));
        rentingType.setText(RentingType.ALL.get(rental.getRentingType()));
        rentingFee.setText(Long.toString(rental.getRentingFee()));
        status.setText(State.state[rental.getState()]);
        if(rental.getState() == State.FINISHED){
            btn_2.setDisable(true);
        } else{
            rental.setEndTime(new Timestamp(new Date().getTime()));
            long fee = ((RentalController)getBController()).calculateRentingFee(rental);
            rentingFee.setText(String.valueOf(fee));
        }

        btn_2.setOnMouseClicked(e->{
            if(rental.getState() == State.IN_PROGRESS) {
                try {
                    ChooseStationHandler chooseStationHandler = new ChooseStationHandler(stage, Configs.CHOOSE_STATION_PATH);
                    chooseStationHandler.setBController(ReturnBikeController.getInstance());
                    chooseStationHandler.setRental(rental);
                    chooseStationHandler.init();
                    chooseStationHandler.setScreenTitle("Choose station");
                    chooseStationHandler.show();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        });
    }
}
