package com.example.ecobikerental.subsystem.interbank;

import com.example.ecobikerental.exceptions.UnrecognizedException;
import com.example.ecobikerental.utils.API;

public class InterbankBoundary {

	String query(String url, String data) {
		String response = null;
		try {
			response = API.patch(url, data);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			throw new UnrecognizedException();
		}
		return response;
	}

}
