package com.example.ecobikerental.subsystem.interbank;

import java.util.Map;
import java.util.logging.Logger;

import com.example.ecobikerental.dto.CreditCardDto;
import com.example.ecobikerental.entities.paymentcard.CreditCard;
import com.example.ecobikerental.entities.Payment;
import com.example.ecobikerental.exceptions.*;
import com.example.ecobikerental.utils.Configs;
import com.example.ecobikerental.utils.MyMap;
import com.example.ecobikerental.utils.Utils;

public class InterbankSubsystemController {

    private static final String SECRET_KEY = "BEPk+NBsTTw=";
    private static final String PAY_COMMAND = "pay";
    private static final String REFUND_COMMAND = "refund";
    private static final String VERSION = "1.0.0";

    private static Logger LOGGER = Utils.getLogger(Utils.class.getName());

    private static InterbankBoundary interbankBoundary = new InterbankBoundary();

    public Payment refund(CreditCardDto card, int amount, String contents) throws PaymentException{
        Map<String, Object> transaction = new MyMap();

        try {
            transaction.putAll(MyMap.toMyMap(card));
        } catch (IllegalArgumentException | IllegalAccessException e) {
            // TODO Auto-generated catch block
            throw new InvalidCardException();
        }
        transaction.put("command", REFUND_COMMAND);
        transaction.put("transactionContent", contents);
        transaction.put("amount", amount);
        transaction.put("createdAt", Utils.getToday());

        Map<String, Object> requestMap = new MyMap();
        requestMap.put("version", VERSION);
        requestMap.put("transaction", transaction);

        String responseText = interbankBoundary.query(Configs.PROCESS_TRANSACTION_URL, generateData(requestMap));
        MyMap response = null;
        try {
            response = MyMap.toMyMap(responseText, 0);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            throw new UnrecognizedException();
        }
        LOGGER.info(responseText);
        String errorCode = (String) response.get("errorCode");
        if(errorCode.equals("00")){
            return makePaymentTransaction(response);
        } else if (errorCode.equals("01")){
            throw new PaymentException("Thẻ không hợp lệ!");
        } else if (errorCode.equals("02")){
            throw new PaymentException("Thẻ không đủ số dư!");
        } else if(errorCode.equals("03")){
            throw new PaymentException("Đã có lỗi xảy ra!");
        } else if(errorCode.equals("04")){
            throw new PaymentException("Giao dịch bị nghi ngờ gian lận!");
        } else if(errorCode.equals("05")){
            throw new PaymentException("Không đủ thông tin giao dịch");
        } else if(errorCode.equals("06")){
            throw new PaymentException("Thiếu thông tin version!");
        } else if(errorCode.equals("07")){
            throw new PaymentException("Amount không hợp lệ!");
        }
        return null;
    }

    private String generateData(Map<String, Object> data) {
        return ((MyMap) data).toJSON();
    }

    public Payment payOrder(CreditCardDto card, int amount, String contents) throws PaymentException{
        Map<String, Object> transaction = new MyMap();

        try {
            transaction.putAll(MyMap.toMyMap(card));
        } catch (IllegalArgumentException | IllegalAccessException e) {
            // TODO Auto-generated catch block
            throw new InvalidCardException();
        }
        transaction.put("command", PAY_COMMAND);
        transaction.put("transactionContent", contents);
        transaction.put("amount", amount);
        transaction.put("createdAt", Utils.getToday());

        Map<String, Object> requestMap = new MyMap();
        requestMap.put("version", VERSION);
        requestMap.put("transaction", transaction);

        String responseText = interbankBoundary.query(Configs.PROCESS_TRANSACTION_URL, generateData(requestMap));
        MyMap response = null;
        try {
            response = MyMap.toMyMap(responseText, 0);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            throw new UnrecognizedException();
        }
        LOGGER.info(responseText);
        String errorCode = (String) response.get("errorCode");
        if(errorCode.equals("00")){
            return makePaymentTransaction(response);
        } else if (errorCode.equals("01")){
            throw new InvalidCardException();
        } else if (errorCode.equals("02")){
            throw new NotEnoughBalanceException();
        } else if(errorCode.equals("03")){
            throw new PaymentException("Đã có lỗi xảy ra!");
        } else if(errorCode.equals("04")){
            throw new SuspiciousTransactionException();
        } else if(errorCode.equals("05")){
            throw new NotEnoughTransactionInfoException();
        } else if(errorCode.equals("06")){
            throw new InvalidVersionException();
        } else if(errorCode.equals("07")){
            throw new InvalidTransactionAmountException();
        }
        return null;

    }

    private Payment makePaymentTransaction(MyMap response) {
        if (response == null)
            return null;
        MyMap transcation = (MyMap) response.get("transaction");
        CreditCard card = new CreditCard((String) transcation.get("cardCode"), (String) transcation.get("owner"),
                (String) transcation.get("cvvCode"), (String) transcation.get("dateExpired"));
        Payment trans = new Payment((String) response.get("errorCode"), (String) transcation.get("transactionContent"), (String) transcation.get("transactionId"));
        trans.setPaymentCard(card);
        switch (trans.getErrorCode()) {
            case "00":
                break;
            case "01":
                throw new InvalidCardException();
            case "02":
                throw new NotEnoughBalanceException();
            case "03":
                throw new InternalServerErrorException();
            case "04":
                throw new SuspiciousTransactionException();
            case "05":
                throw new NotEnoughTransactionInfoException();
            case "06":
                throw new InvalidVersionException();
            case "07":
                throw new InvalidTransactionAmountException();
            default:
                throw new UnrecognizedException();
        }

        return trans;
    }
}
