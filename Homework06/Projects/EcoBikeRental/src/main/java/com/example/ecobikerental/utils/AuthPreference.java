package com.example.ecobikerental.utils;

import java.util.prefs.Preferences;

public class AuthPreference {
    private Preferences prefs;
    private static final String PREFERENCES_USER = "Authenticated User";

    public AuthPreference() {
        this.prefs = Preferences.userRoot().node(this.getClass().getName());
    }

    public void setAuthUser(int userId) {
        prefs.putInt(PREFERENCES_USER, userId);
    }

    public void removeAuthUser() {
        prefs.remove(PREFERENCES_USER);
    }

    public int getAuthUser() {
        return prefs.getInt(PREFERENCES_USER, 0);
    }
}
