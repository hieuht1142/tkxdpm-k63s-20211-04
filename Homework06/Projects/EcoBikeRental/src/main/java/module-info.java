module com.example.ecobikerental {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.logging;
    requires java.sql;
    requires mysql.connector.java;
    requires com.jfoenix;
    requires java.base;
    requires java.net.http;
    requires java.prefs;
    requires org.junit.jupiter.api;

    opens com.example.ecobikerental.handlers to javafx.fxml;
    opens com.example.ecobikerental.handlers.admin to javafx.fxml;
    opens com.example.ecobikerental.handlers.auth to javafx.fxml;
    opens com.example.ecobikerental.handlers.user to javafx.fxml;
    opens com.example.ecobikerental.entities to javafx.base;
    opens com.example.ecobikerental to javafx.base;
    opens com.example.ecobikerental.dto to javafx.base;
    opens com.example.ecobikerental.entities.paymentcard to javafx.base;
    opens com.example.ecobikerental.controllers to org.junit.platform.commons;

    exports com.example.ecobikerental;
    opens com.example.ecobikerental.controllers.rentingtype to org.junit.platform.commons;
}
