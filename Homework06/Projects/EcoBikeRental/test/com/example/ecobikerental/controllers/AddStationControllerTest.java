package com.example.ecobikerental.controllers;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.*;

class AddStationControllerTest {

    private AddStationController addStationController;

    @BeforeEach
    void setUp() {
        addStationController = new AddStationController();
    }

    // White-box Testing
    @Test
    void validateStationAddress1() {
        String address = "";
        boolean res = addStationController.validateStationAddress(address);
        assertFalse(res);
    }

    @Test
    void validateStationAddress2() {
        String address = null;
        boolean res = addStationController.validateStationAddress(address);
        assertFalse(res);
    }

    @Test
    void validateStationAddress3() {
        String address = "$#^ So 18, Ta Quang Buu";
        boolean res = addStationController.validateStationAddress(address);
        assertFalse(res);
    }

    @Test
    void validateStationAddress4() {
        String address = "So 18, Ta Quang Buu, Hai Ba Trung, Ha Noi";
        boolean res = addStationController.validateStationAddress(address);
        assertTrue(res);
    }


    // Black-box Testing
    @ParameterizedTest
    @CsvSource({
            "haibatrung, true", "18, true", "hai ba trung, true", "'ta quang buu, hbt', true", "'Hai Bà Trưng, Hà Nội', true",
            "## Hà Bà Trưng, false", ", false", "'', false"
    })
    void validateStationAddress(String name, boolean expected) {
        boolean isValid = addStationController.validateStationAddress(name);
        assertEquals(expected, isValid);
    }
}