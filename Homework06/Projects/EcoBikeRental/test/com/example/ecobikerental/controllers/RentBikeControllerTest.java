package com.example.ecobikerental.controllers;

import com.example.ecobikerental.entities.Bike;
import com.example.ecobikerental.enums.bike.State;
import com.example.ecobikerental.exceptions.BikeNotAvailableException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.sql.SQLException;

import static org.junit.jupiter.api.Assertions.*;

class RentBikeControllerTest {

    private RentBikeController rentBikeController;

    @BeforeEach
    public void setUp() throws Exception {
        rentBikeController = RentBikeController.getInstance();
    }

    @Test
    public void rentBike() throws SQLException {
        testBikeAvailable();
        testBikeRented();
    }

    @Test
    public void testBikeAvailable() throws SQLException {
        Bike bike = new Bike();
        bike.setState(State.AVAILABLE);

        assertTrue(rentBikeController.rentBike(bike));
    }

    @Test
    public void testBikeRented() throws SQLException {
        Bike bike = new Bike();
        bike.setState(State.RENTED);

        try {
            rentBikeController.rentBike(bike);
        } catch (BikeNotAvailableException e) {
            assertEquals(e.getClass(), BikeNotAvailableException.class);

            String expectedMessage = "The bike had already been rented. Please choose another one!";
            String actualMessage = e.getMessage();
            assertTrue(actualMessage.contains(expectedMessage));
        }
    }

    @ParameterizedTest
    @CsvSource({"Ha,true", "Ha Ha Ha Ha Ha Ha Ha Ha Ha Hai,true", "H,false", "'',false", ",false", "Vu_Minh%Hieu,false", "121Hieu,false"})
    public void validateName(String name, boolean expected) {
        boolean isValid = rentBikeController.validateName(name);
        assertEquals(expected, isValid);
    }

    @ParameterizedTest
    @CsvSource({"0366951603,true", "0232423,false", "abc%45,false", "1234567890,false", ",false", "'',false"})
    public void validatePhoneNumber(String phoneNumber, boolean expected) {
        boolean isValid = rentBikeController.validatePhoneNumber(phoneNumber);
        assertEquals(expected, isValid);
    }

    @ParameterizedTest
    @CsvSource({"By Minutes,true", "By Day,true", ",false", "'',false", "Another Value,false"})
    public void validateRentingType(String rentingType, boolean expected) {
        boolean isValid = rentBikeController.validateRentingType(rentingType);
        assertEquals(expected, isValid);
    }
}
