package com.example.ecobikerental.controllers;

import com.example.ecobikerental.entities.Rental;
import com.example.ecobikerental.enums.rental.RentingType;
import org.junit.jupiter.api.Test;

import java.sql.Timestamp;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

class ReturnBikeControllerTest {
    private Rental rental;
    private ReturnBikeController returnBikeController;

    public ReturnBikeControllerTest(){
        returnBikeController = ReturnBikeController.getInstance();
        rental = new Rental();
        rental.setStartTime(new Timestamp(new Date().getTime()));
    }

    @Test
    void calculateRentingFee() {
        //theo phut, <10p
        rental.setRentingType(RentingType.BY_MINUTES);
        rental.setEndTime(new Timestamp(rental.getStartTime().getTime() + 540000)); //9 minutes
        long fee = returnBikeController.calculateRentingFee(rental);
        assertEquals(fee, 0);
    }

    @Test
    void calculateRentingFee1() {
        //theo phut, >10p && <=30
        rental.setRentingType(RentingType.BY_MINUTES);
        rental.setEndTime(new Timestamp(rental.getStartTime().getTime() + 660000)); //11 minutes
        long fee = returnBikeController.calculateRentingFee(rental);
        assertEquals(fee, 10000);
    }

    @Test
    void calculateRentingFee2() {
        //theo phut, >30
        rental.setRentingType(RentingType.BY_MINUTES);
        rental.setEndTime(new Timestamp(rental.getStartTime().getTime() + 1860000)); //11 minutes
        long fee = returnBikeController.calculateRentingFee(rental);
        assertEquals(fee, 13000);
    }

    @Test
    void calculateRentingFee3() {
        //theo ngay, < 12h
        rental.setRentingType(RentingType.BY_DAY);
        rental.setEndTime(new Timestamp(rental.getStartTime().getTime() + 660000)); //11 minutes
        long fee = returnBikeController.calculateRentingFee(rental);
        assertEquals(fee, 80000);
    }

    @Test
    void calculateRentingFee4() {
        //theo ngay, > 12h && <= 24h
        rental.setRentingType(RentingType.BY_DAY);
        rental.setEndTime(new Timestamp(rental.getStartTime().getTime() + 54000000)); //15 hours
        long fee = returnBikeController.calculateRentingFee(rental);
        assertEquals(fee, 200000);
    }

    @Test
    void calculateRentingFee5() {
        //theo ngay, > 24h
        rental.setRentingType(RentingType.BY_DAY);
        rental.setEndTime(new Timestamp(rental.getStartTime().getTime() + 97200000)); //27 hours
        long fee = returnBikeController.calculateRentingFee(rental);
        assertEquals(fee, 224000);
    }
}