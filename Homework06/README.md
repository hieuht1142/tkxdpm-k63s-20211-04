# Phân công nhiệm vụ Homework 06 #


### Vũ Minh Hiếu ###

* Tham gia dựng code base
* Code các màn: user homescreen, search bike, bike detail, renting form, deposit invoice screen, payment screen
* Viết unti test cho class RentBikeController

### Nguyễn Lan Anh ###

* Tham gia dựng code base
* Code các màn: choose station, choose position, interbank subsystem
* Viết unit test cho

### Hoàng Trung Hiếu ###

* Tham gia dựng code base
* Code các màn: search station, search station result
* Viết unit test cho

### Ngô văn Giang ###

* Code các màn: admin homescreen, bike management, add bike
* Viết unit test cho

### Trần Xuân Khu ###

* Code các màn: station management, add station
* Viết unit test cho

### Nguyễn Khánh Duy ###

* Code các màn: Renting history, view renting status
* Viết unit test cho
